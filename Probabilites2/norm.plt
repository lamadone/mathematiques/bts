set format "% .5f"
set table "norm1.dat"
set samples 5
plot [x=-2:0] 4*(erf((x-2.5)/sqrt(2))+1)/2
set table "norm2.dat"
set samples 25
plot [x=0:2.5] 4*(erf((x-2.5)/sqrt(2))+1)/2
set table "norm3.dat"
plot [x=2.5:5] 4*(erf((x-2.5)/sqrt(2))+1)/2
set table "norm4.dat"
set samples 5
plot [x=5:7] 4*(erf((x-2.5)/sqrt(2))+1)/2

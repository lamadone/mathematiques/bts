\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=1.5cm,hmargin=1.5cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usetikzlibrary{calc,intersections}
\usepackage{pgfplots}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{setspace}
\usepackage{xstring}
\usepackage{framed}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
%\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\theoremstyle{plain}
\newtheorem{exercice}{Exercice}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{}
\date{}
%\date{7 octobre 2013}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
    \framebox{%
      \begin{minipage}{0.98\linewidth}%
        Nom: \hfill\Large \@title ~-- \@author \hfill ~\normalsize\\%
        Prénom: \hfill\Large \@date\hfill ~
    \end{minipage}}%
    \normalsize%
    %\vspace{1cm}%
  \end{center}
}
\makeatother

\setlength{\parindent}{0pt}
\everymath{\displaystyle\everymath{}}
\renewcommand{\labelenumii}{\theenumii)}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}

\newcommand{\Zc}{\underline{Z}}
\newcommand{\fem}{force électromagnétique}
\newcommand{\cplx}{\underline}

\begin{document}
\maketitle


On considère les trois composants essentiels :
\begin{tikzpicture}[circuit ee IEC]
  \draw (-1,0) to [resistor={info=$R$}] (1,0) ;
\end{tikzpicture}
résistor de résistance $R$ exprimée en Ohms ($\Omega$),
\begin{tikzpicture}[circuit ee IEC]
  \draw (-1,0) to [inductor={info=$L$}] (1,0) ;
\end{tikzpicture}
bobine d'inductance $L$ exprimée en Henrys ($H$) et
\begin{tikzpicture}[circuit ee IEC]
  \draw (-1,0) to [capacitor={info=$C$}] (1,0) ;
\end{tikzpicture}
condensateur de capacité $C$ exprimée en Farads ($F$).

Dans le plan complexe, on peut représenter ces grandeurs de la façon
suivante :
\begin{center}
  \begin{tikzpicture}
    \draw [->] (-3,0) -- (7,0) node[right] {$\mathscr{R}e$ };
    \draw [->] (0,-5) -- (0,5) node[above] {$\mathscr{I}m$ };
    \draw (5,-0.1) -- (5,0.1) node[above] {$R$ résistance positive };
    \draw (-0.1,-2.5) -- (0.1,-2.5) node[left] { $\Zc_C =
    \frac1{jC\omega} $ };
    \draw (-0.1,2.5) -- (0.1,2.5) node[left] { $\Zc_L = jL\omega$ } ;

    \draw (6,-4) node[text width=3cm] { pulsation (rad/$s$) $\omega =
    2\pi f$, $f$ la fréquence en $Hz$. };
  \end{tikzpicture}

  $\Zc_C$ : impédance complexe de $C$\\
  $\Zc_L$ : impédance complexe de $L$
\end{center}

\begin{exemple}[moteur à courant continu]

  \[\left\lbrace\begin{array}{l}
      u(t) = e(t) + Ri(t) \\
      e(t) = K\Omega(t) \\
      C(t) = Ki(t) \\
      J\frac{d\Omega(t)}{dt} = C(t) - C_{\text{charge}}
  \end{array}\right.\]

  $u(t)$ est la tension d'induit, $e$ la \fem, $R$ la résistance
  d'induit, $i$ le courant d'induit, $K$ la constante de \fem (ou du
  couple), $J$ le moment d'inertie en $kg\cdots m^2$, $C$ le couple
  électromagnétique du moteur en $Nm$ et $C_{\text{charge}}$, le couple
  de charge, en $Nm$.

  Dans le domaine fréquentiel (complexe) on a :
  \[\left\lbrace\begin{array}{l}
      \cplx{U} = \cplx{E} + R\cplx{I} \\
      \cplx{E} = K\cplx{\Omega} \\
      \cplx{C} = K\cplx{I} \\
      jJ\omega\cplx{\Omega} = \cplx{C} \ \ \  ;~
      C_{\text{charge}} = 0 \ \ \text{moteur à vide}
  \end{array}\right.\]

  Écrivons la relation qui lie $\cplx{\Omega}$ à $\cplx{U}$ :

  $\cplx{\Omega} = \frac{\cplx{C}}{jJ\omega} = \frac1{jJ\omega}K \cdot
  \left(\frac{\cplx{U} - k\cplx{\Omega}}{R}\right)$

  $\implies \Omega jJ\omega = K\frac{\cplx{U}}{R} - \frac{K^2}R
  \cplx{\Omega}$

  Soit $\cplx\Omega \left(jJ\omega + \frac{K^2}R\right) =
  K\frac{\cplx{U}}{R}$

  $\implies \cplx\Omega = \frac{\frac{K}{R}}{\frac{K^2}R + jJ\omega}
  \cplx{U}$

  $\implies \cplx\Omega = \frac1K \cdot
  \frac1{1+j\omega\cdot\frac{RJ}{K^2}}\cplx{U}$

  de la forme :
  \[ \frac{\cplx{\Omega}}{\cplx{U}} =
    \frac{G_0}{1+j\frac{\omega}{\omega_c}}
  \]
  fonction de transfert du premier ordre, où $G_0 = \frac1K$ est le
  gain statique et $\omega_c = \frac{K^2}{RJ}$ la pulsation de
  coupure.

  $\implies$ comportement d'un filtre passe-bas de premier ordre.

  $\iff$ $\cplx{I} = \frac{\cplx{\Omega}}{\cplx{U}} =
  \frac{G_0}{1+j\frac{\omega}{\omega_c}}$

  On peut donc tracer les courbes de Bode en module et en phase :

  $\left|\cplx{I}\right| =
  \frac{G_0}{\sqrt{1+\left(\frac{\omega}{\omega_c}\right)^2}} $ ou
  $20\log\left( \left|\cplx{I}\right| \right) = 20\log\left(
  \frac{G_0}{\sqrt{1+\left(\frac{\omega}{\omega_c}\right)^2}} \right)$

  et $\mathop{Arg}(\cplx{I}) =
  -\arctan\left(\frac{\omega}{\omega_c}\right)$

  \begin{tikzpicture}[xscale=2]
    \begin{loglogaxis}[
        domain=0.001:1000,
        %        /pgfplots/log basis x=10,
      ]
      \addplot+[
        y filter/.code={\pgfmathparse{20*\pgfmathresult}},
      ]
      {1/(sqrt(1+x^2))};
    \end{loglogaxis}
  \end{tikzpicture}

  \begin{tikzpicture}[xscale=2]
    \begin{semilogxaxis}[
        domain=0.001:1000,
        %        /pgfplots/log basis x=10,
      ]
      \addplot+[
      ]
      {-atan(x)};
    \end{semilogxaxis}
  \end{tikzpicture}

  Application numérique :

  $\begin{array}{l} J = \np[kg.m^2]{1.25} \\
  R = \np[\Omega]{2.2} \\ K = \np[Nm.A^{-1}]{1.3}\end{array}$

\end{exemple}

\begin{exemple}[cas d'une machine asynchrone]~
  \begin{center}
    \begin{tikzpicture}[circuit ee IEC,scale=1.4]
      \draw (-3,2) -- (-1,2) to [inductor={info=$X_2$}] (1,2) to
      [resistor={info=$\frac{R_2}{g}$}] (1,0) -- (-3,0) ;
      \draw (-2,2) to [inductor={info=$X_m$}] (-2,0) ;
      \draw [->] (-2.7,2) -- (-2.5,2) node[above] {$\cplx{I}_1$} ;
      \draw [->] (-1.7,2) -- (-1.5,2) node[above] {$\cplx{I}_2$} ;
      \draw [->] (-3,0.1) -- (-3,1.9) ; \draw (-3,1) node[left]
      {$\cplx{V}_1$} ;
    \end{tikzpicture}
  \end{center}
  \begin{description}
    \item[g] glissement
    \item[$X_m$] réactance de magnétisation (imaginaire)
    \item[$X_2$] réactance de fuite (imaginaire)
    \item[$R_2$] résistance rotorique (réel)
  \end{description}

  Expression de $I_2$ : \[ \cplx{I}_2 =
    \frac{\cplx{V}_1}{\frac{R_2}{g}+jX_2} \implies I_2 =
  \frac{V_1}{\sqrt{\left(\frac{R_2}{g}\right)^2+X_2^2}} \]
  Pertes Joules : \[ P_{J_R} = 3 R_2 I_2^2 = 3 R_2
  \frac{V_1^2}{\left(\frac{R_2}{g}\right)^2+X_2^2} \]
  Puissance transmise au rotor : \[ P_{\text{tr}} = \frac{P_{J_R}}g = 3
  \frac{R_2}g \frac{V_1^2}{\left(\frac{R_2}{g}\right)^2+X_2^2} \]
  Couple électromagnétique : \[ P_{\text{tr}} = C\cdot\Omega_s \implies
    C = \frac{P_{\text{tr}}}{\Omega_s} = \frac3{\Omega_s} \frac{R_2}g
  \frac{V_1^2}{\left(\frac{R_2}{g}\right)^2+X_2^2} \]
  On suppose que tous les paramètres sont constants.

  Calculer l'expression de $g=g_0$ pour lequel $C=C_{\max}$.

  On trouve $g_0 = \frac{R_2}{X_2}$, donc $C_{\max} = \frac3{\Omega_s}
  \frac{R_2}g \frac{V_1^2}{2\left(\frac{R_2}{g}\right)^2} =
  \frac3{2\Omega_s} \frac{V_1^2}{X_2}$
  \[ \boxed{C_{\max} = \frac{3V_1^2}{2\Omega_sX_2}} \]
\end{exemple}

\begin{exercice}[Représenter $C=f(g)$]~

  \begin{center}
    \scriptsize
    \begin{tikzpicture}[yscale=2.0,xscale=1.7]
      \draw [->] (-2,0) -- (3.5,0) ;
      \draw [->] (0,-1) -- (0,1) ;
      \draw plot[domain=-1:-0.01,smooth] (\x,{1/(\x*((1/\x)^2+1))} ) ;
      \draw [name path=f] plot[domain=0.01:3.01,smooth]
      (\x,{1/(\x*((1/\x)^2+1))} ) ;
      \draw [dashed] (-0.5,0.5) -- (3,0.5) ;
      \draw (1,0.5) node[fill, circle,inner sep=.7pt] { } ;
      \draw (1,0.5) node[above] {$C_{\max}$} ;
      \draw (0,0) node[fill, circle,inner sep=.7pt] { } ;
      \draw (0,0) node[below right] { synchronisation } ;
      \draw [name path = d,dashed] (3,1) -- (3,0) ;
      \path [name intersections ={of = d and f }] ;
      \coordinate (A) at (intersection-1) ;
      \draw (A) node[fill, circle,inner sep=.7pt] { } ;
      \draw (A) node[right] { arrêt } ;
    \end{tikzpicture}
    \normalsize
  \end{center}

\end{exercice}

\end{document}

% !TEX program = latexmk
% !TEX options = -pdf -synctex=1 -interaction=nonstopmode -file-line-error -shell-escape "%DOC%"
% !TEX encoding = UTF-8
% !TEX root = SIO_maths_approfondies.tex
\documentclass[class = scrartcl, crop = false, french]{standalone}

\usepackage{lycee}


\title{Dérivation}
\author{Vincent-Xavier Jumel}
\date{}

\begin{document}

\IfStandalone{%
  \maketitle
}{}

\section{Nombre dérivé}
\subsection{Une définition du nombre dérivé}

Soit $x_0 \in I$ et $h$ un petit nombre tel que $x_0+h\in I$. Le taux
d'accroissement de la fonction $f$ en $x_0$ est
$\dfrac{f(x_0+h)-f(x_0)}{x_0+h-x_0}=\frac{f(x_0+h)-f(x_0)}{h}$.

L'accroissement de la fonction se traduit graphiquement :

\begin{center}
  \begin{tikzpicture}[scale=0.4,domain=-2:4]
    \tkzInit[xmin=-2,xmax=4]

    \draw[very thin,color=lightgray] (-3,-3) grid (5,5);

    \draw[->] (-3,0) -- (5,0) node[right] {$x$};
    \draw[->] (0,-3) -- (0,5) node[above] {$f(x)$};

    \draw[color=red] (-2,-2) .. controls (0,-2) and (-0.7,-1.2) ..
    (0.5,2) .. controls (1.2,3.2) .. (1.2,3.2) .. controls (2,4) and
    (2,4) .. (2.5,4) ..  controls (3,4) .. (5,2) node[right]
    {$\mathcal{C}_f$};
    \draw[dashed] (0.5,0) node[below] {\tiny $a$} -- (0.5,2) ;
    \draw[dashed] (0,2) node [left] {\tiny $f(a)$} -- (0.5,2) ;
    \draw[dashed] (1.2,0) node [below] {\tiny $a+h$} -- (1.2,3.2) ;
    \draw[dashed] (0,3.2) node [left] {\tiny $f(a+h)$} -- (1.2,3.2) ;
    \draw[blue,thick] (0.5,2) -- (1.2,3.2) ;
  \end{tikzpicture}
\end{center}

La courbe est approchée par une portion de droite. Plus la quantité $h$
est «petite», plus on approche la courbe par des portions de droites. On
peut introduire le nombre dérivé
  \[\boxed{f'(a)=\lim_{h\rightarrow 0}{\frac{f(a+h)-f(a)}{h}}}\]

Ce nombre dérivé en $a$ coïncide avec la valeur de la fonction dérivée
en $a$. En pratique, on utilise plutôt la fonction dérivée.

\subsection{Tangente en un point de la courbe}

Par sa définition, le nombre dérivé donne la direction de la tangente à
la courbe en ce point ; $f'(a)$ est le coefficient directeur. L'équation
complète de la tangente en $a$ est \[
  \boxed{y=f'(a)(x-a)+f(a)}
\]

\begin{center}
  \begin{tikzpicture}[scale=0.4,domain=-2:4]
    \tkzInit[xmin=-2,xmax=4]

    \draw[very thin,color=lightgray] (-3,-3) grid (5,5);

    \draw[->] (-3,0) -- (5,0) node[right] {$x$};
    \draw[->] (0,-3) -- (0,5) node[above] {$f(x)$};

    \draw[color=red] (-2,-2) .. controls (0,-2) and (-0.7,-1.2) ..
    (0.5,2) .. controls (1.2,3.2) .. (1.2,3.2) .. controls (2,4) and
    (2,4) .. (2.5,4) ..  controls (3,4) .. (5,2) node[right]
    {$\mathcal{C}_f$};
    \draw[dashed] (0.5,0) node[below] {\tiny $a$} -- (0.5,2) ;
    \draw[dashed] (0,2) node [left] {\tiny $f(a)$} -- (0.5,2) ;
    \draw[blue,thin,<->] (-0.2,0.8) -- (1.2,3.2) ;
  \end{tikzpicture}
\end{center}

\section{Fonction dérivée, fonctions dérivées usuelles, opérations}

Comme on peut tracer des tangentes en tout point, on peut calculer le
nombre dérivé d'une fonction en tout point de son domaine de définition.
La fonction $x\mapsto f'(x)$ est appellé \textcolor{red}{fonction
dérivée de $f$, notée $f'$}.

On peut noter que si $f$ et $g$ sont deux fonctions et $k$ un nombre
réel, alors $(f+g)'=f'+g'$, $(f-g)'=f'-g'$ et $(k\times f)'=k\times f'$.
Pour les fonctions usuelles, on peut admettre le tableau suivant :
\begin{center}
  \begin{tabular}{|l||*{6}{c|}}\hline
    \(\displaystyle f\)  & 1 & \(\displaystyle k\) & \(\displaystyle ax+b\) & \(\displaystyle x^2\) & \(\displaystyle x^3\)  & \(\displaystyle \frac{1}{x}\) \\[6mm] \hline
    \(\displaystyle f'\) & 0 & 0     & \(\displaystyle a\)    & \(\displaystyle 2x\)  & \(\displaystyle 3x^2\) & \(\displaystyle -\frac{1}{x^2}\)  \\[6mm] \hline
  \end{tabular}
\end{center}

Soient $k$ un nombre réel et $u$ et $v$ deux fonctions, qu'on suppose
dérivable, on a les propriétés suivantes :

\begin{proposition}
  \begin{itemize}
    \item $(ku)'=ku'$ (multiplication par un réel)
    \item $(u+v)=u'+v'$ (addition de deux fonctions)
    \item $(u+kv)=u'+kv'$ (addition généralisée)
  \end{itemize}
\end{proposition}

Si $n$ est un nombre entier, on a :
\begin{proposition}
  La fonction $u^n$ est dérivable et $(u^n)'=nu'u^{n-1}$.
\end{proposition}

\begin{proposition}
  La fonction $uv$ est dérivable et $(uv)'=uv'+u'v$.
\end{proposition}

Si on suppose de plus que pour tout $x$ appartenant à l'intervalle de
définition de $v$, $v(x)\neq0$, alors on a également la propriété
suivante.
\begin{proposition}

  \begin{itemize}
    \item La fonction $\displaystyle\frac1u$ est dérivable et
      $\displaystyle{\left(\frac1u\right)'} =
      -\displaystyle\frac{u'}{u^2}$
    \item La fonction $\displaystyle\frac{u}v$ est dérivable et 
      $\displaystyle{\left(\frac{u}v\right)'} =
      \displaystyle\frac{u'v-v'u}{v^2}$
  \end{itemize}
\end{proposition}

\end{document}

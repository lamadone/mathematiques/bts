\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\newtheorem{exemple}{Exemple}
\newtheorem{theoreme}{Théorème}
\newtheorem{exercise}{Exercice}
\newtheorem{definition}{Définition}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\begin{document}
\maketitle

\begin{definition}
  La mesure d'un angle en radian est la longueur de l'arc de cercle unitaire
  intercepté par cet angle.
\end{definition}

\begin{definition}
  \begin{itemize}
    \item Le \em{cosinus} d'un angle $\widehat{xOM}$ est l'abscisse du point
      $M$. On note $\cos(x)$ ou $\cos\alpha$.
    \item Le \em{sinus} d'un angle $\widehat{xOM}$ est l'ordonnée du point
      $M$. On note $\sin(x)$ ou $\sin\alpha$.
    \item Pour les angles différents de l'angle droit, la \em{tangente} d'un
      angle est le rapport du \em{sinus} sur le \em{cosinus}. On note
      $\tan(x)$ ou $\tan\alpha$.
  \end{itemize}
\end{definition}

On admet que les angles ont un sens :
\begin{itemize}
  \item positif ou direct s'il est dans le sens inverse des aiguilles d'une
    montre ;
  \item négatif ou indirect s'il est dans le sens des aiguilles d'une montre.
\end{itemize}

\section{Propriétés et valeurs remarquables}

\begin{center}

  \begin{tabular}{|c|c|c|c|c|c|}\hline
    $\alpha$     & 0 & $\frac{\pi}{6}$      & $\frac{\pi}{4}$      & $\frac{\pi}{3}$      & $\frac{\pi}{2}$ \\ \hline
    $\sin\alpha$ & 0 & $\frac{1}{2}$        & $\frac{\sqrt{2}}{2}$ & $\frac{\sqrt{3}}{2}$ & 1 \\ \hline
    $\cos\alpha$ & 1 & $\frac{\sqrt{3}}{2}$ & $\frac{\sqrt{2}}{2}$ & $\frac{1}{2}$        & 0 \\ \hline
  \end{tabular}
\end{center}
  Les deux figures suivantes synthétisent les résultats du tableau et
  les égalités suivantes, vraies quelque soient $\alpha$ et $\beta$
  réels.
  \renewcommand{\labelitemi}{$\bullet$}
  \begin{multicols}{3}
    \begin{itemize}
      \item $\cos\frac{\pi}{2}-\alpha = \sin\alpha$
      \item $\sin\frac{\pi}{2}-\alpha = \cos\alpha$
      \item $\cos\pi-\alpha = -\cos\alpha$
      \item $\cos-\alpha = \cos\alpha$
      \item $\sin\pi-\alpha = \sin\alpha$
      \item $\sin-\alpha = -\sin\alpha$
    \end{itemize}
  \end{multicols}
  \begin{multicols}{2}
    \begin{itemize}
      \item $\cos\alpha+\beta = \cos\alpha\cos\beta -
        \sin\alpha\sin\beta$
      \item $\sin\alpha+\beta = \sin\alpha\cos\beta +
        \cos\alpha\sin\beta$
    \end{itemize}
  \end{multicols}
  \[\cos^2\alpha + \sin^2\alpha = 1\]
\begin{center}
  \begin{tikzpicture}[scale=2.7,cap=round]
  % Local definitions
  \def\cosforty{0.8090169943749475}
  \def\sinforty{0.5877852522924731}
  % Colors
  \colorlet{anglecolor}{green!50!black}
  \colorlet{sincolor}{red}
  \colorlet{tancolor}{orange!80!black}
  \colorlet{coscolor}{blue}

  % Styles
  \tikzstyle{axes}=[]
  \tikzstyle{important line}=[very thick]
  \tikzstyle{information text}=[rounded corners,fill=red!10,inner sep=1ex]

  % The graphic
  \draw[style=help lines,step=0.5cm] (-1.4,-1.4) grid (1.4,1.4);

  \draw (0,0) circle (1cm);

  \begin{scope}[style=axes]
    \draw[->] (-1.5,0) -- (1.5,0) node[right] {$x$};
    \draw[->] (0,-1.5) -- (0,1.5) node[above] {$y$};

    \foreach \x/\xtext in {-1, -.5/-\frac{1}{2}, 1}
      \draw[xshift=\x cm] (0pt,1pt) -- (0pt,-1pt) node[below,fill=white]
            {$\xtext$};

    \foreach \y/\ytext in {-1, -.5/-\frac{1}{2}, .5/\frac{1}{2}, 1}
      \draw[yshift=\y cm] (1pt,0pt) -- (-1pt,0pt) node[left,fill=white]
            {$\ytext$};
  \end{scope}

  \filldraw[fill=green!40,draw=anglecolor] (0,0) -- (3mm,0pt) arc(0:54:3mm);
  \filldraw[fill=green!20,draw=anglecolor] (0,0) -- (3mm,0pt) arc(0:36:3mm);
  \draw (18:2mm) node[anglecolor] {$\alpha$};

  \draw[style=important line,sincolor,dashed]
    (36:1cm) -- node[left=1pt,fill=white] {$\sin \alpha$} +(0,-\sinforty);

  \draw[style=important line,coscolor]
    (0,0) -- node[below=2pt,fill=white] {$\cos \alpha$} (\cosforty,0);

  \draw[style=important line,tancolor] (1,0) --
    node [right=1pt,fill=white]
    {
      $\displaystyle \tan \alpha \color{black}=
      \frac{{\color{sincolor}\sin \alpha}}{\color{coscolor}\cos \alpha}$
    } (intersection of 0,0--36:1cm and 1,0--1,1) coordinate (t);

  \draw (0,0) -- (t);

  \draw[style=important line,sincolor,dashed] (54:1cm) -- (0,\cosforty) ;
  \draw[style=important line,coscolor] (54:1cm) -- +(0,-\cosforty) ;
  \draw (0,0) -- (54:1cm) ;
  \draw (82:3mm) node[anglecolor] {$\frac{\pi}{2}-\alpha$};

\end{tikzpicture}

  \begin{tikzpicture}[scale=2.7]
  % draw the coordinates
        \draw[->] (-1.5cm,0cm) -- (1.5cm,0cm) node[right,fill=white] {$x$};
        \draw[->] (0cm,-1.5cm) -- (0cm,1.5cm) node[above,fill=white] {$y$};

   % draw each angle in radians
        \foreach \x/\xtext in {
            30/\frac{\pi}{6},
            45/\frac{\pi}{4},
            60/\frac{\pi}{3},
            90/\frac{\pi}{2},
            120/\frac{2\pi}{3},
            135/\frac{3\pi}{4},
            150/\frac{5\pi}{6},
            180/\pi,
            210/\frac{7\pi}{6},
            225/\frac{5\pi}{4},
            240/\frac{4\pi}{3},
            270/\frac{3\pi}{2},
            300/\frac{5\pi}{3},
            315/\frac{7\pi}{4},
            330/\frac{11\pi}{6},
            360/0|2\pi}
            {
                % dots at each point
                \filldraw[black] (\x:1cm) circle(0.4pt);
                \draw (\x:1.17cm) node[fill=white] {$\xtext$};
              }

        % draw the unit circle
        \draw[thick] (0cm,0cm) circle(1cm);

        % 30°
        \draw[thick,dashed] (30:1cm) -- (150:1cm) ;
        \draw[thick,dashed] (150:1cm) -- (210:1cm) ;
        \draw[thick,dashed] (210:1cm) -- (330:1cm) ;
        \draw[thick,dashed] (330:1cm) -- (30:1cm) ;
        % 45°
        \draw (45:1cm) -- (135:1cm) ;
        \draw (135:1cm) -- (225:1cm) ;
        \draw (225:1cm) -- (315:1cm) ;
        \draw (315:1cm) -- (45:1cm) ;
        % 60°
        \draw[thick,dash pattern = on 0.5pt off 3pt on 4pt off 4pt ]
        (60:1cm) -- (120:1cm) ;
        \draw[thick,dash pattern = on 0.5pt off 3pt on 4pt off 4pt ]
        (120:1cm) -- (240:1cm) ;
        \draw[thick,dash pattern = on 0.5pt off 3pt on 4pt off 4pt ]
        (240:1cm) -- (300:1cm) ;
        \draw[thick,dash pattern = on 0.5pt off 3pt on 4pt off 4pt ]
        (300:1cm) -- (60:1cm) ;

        \draw (0,0.5) node [below left] {$\frac{1}{2}$} ;
        \draw (0.5,0) node [below left] {$\frac{1}{2}$} ;

      \end{tikzpicture}

\end{center}


\section{Études des fonctions et courbes représentatives}

Les fonctions $\sin$ et $\cos$ sont $2\pi$-périodiques ($\forall
x\in\mathbf{R}\  f(x+2\pi)=f(x)$). De plus, $\cos$ est une fonction paire et
$\sin$ est une fonction impaire. $\tan$ est une fonction impaire et
$\pi$-périodique. On a donc les tableaux de variations suivants :
\begin{multicols}{3}
\scriptsize
 $\begin{tabvar}{|C|CCCCC|} \hline
          x                 & 0 &          & \frac{\pi}{2} &        & \pi \\ \hline
          \niveau{3}{3}\TVcenter{\cos x} & 1 & \decroit & 0 & \decroit & -1 \\ \hline
      \end{tabvar}$

 $\begin{tabvar}{|C|CCCCC|} \hline
     x                 & -\frac{\pi}{2}&          & 0 &        & \frac{\pi}{2} \\ \hline
          \niveau{1}{3}\TVcenter{\sin x} & -1 & \croit & 0 & \croit & 1 \\ \hline
      \end{tabvar}$

 $\begin{tabvar}{|C|CCCCC|} \hline
     x                              & 0       &        & \frac{\pi}{4} &        & \frac{\pi}{2} \\ \hline
     \niveau{1}{3}\TVcenter{\tan x} & -\infty & \croit & 0             & \croit & +\infty  \\ \hline
      \end{tabvar}$
\normalsize
\end{multicols}

\begin{center}
  \begin{tikzpicture}[scale=0.7]
    \draw [->] (-5,0) -- (5,0) ;
    \draw [->] (0,-4) -- (0,4) ;

    \draw [dashed] plot [domain=-5:5,smooth,samples=50] (\x,{sin(\x r)})
    node[below] {$\sin x$} ;
    \draw plot [domain=-5:5,smooth,samples=50] (\x,{cos(\x r)})
    node[above right] {$\cos x$} ;

    \draw plot [domain=-1.3:1.3,smooth,samples=50] (\x,{tan(\x r)}) ;
    \draw plot [domain=1.85:4.45,smooth,samples=50] (\x,{tan(\x r)})
    node[right] {$\tan x$} ;
    \draw plot [domain=-1.85:-4.45,smooth,samples=50] (\x,{tan(\x r)}) ;

  \end{tikzpicture}
\end{center}

\section{Dérivée et primitive}

\renewcommand{\labelitemi}{$\bullet$}

On a les dérivées et primitives suivantes :
\begin{multicols}{2}
  Dérivées :
  \begin{itemize}
    \item $\sin'(x) = \cos(x) $
    \item $\cos'(x) = -\sin(x)$
    \item $\tan'(x)=1+\tan^2(x)=\dfrac{1}{\cos^2(x)}$
  \end{itemize}
  \columnbreak
  Primitives :
  \begin{itemize}
    \item $\sin(x)$ a pour primitive $-\cos(x)$
    \item $\cos(x)$ a pour primtive $\sin(x)$
  \end{itemize}
\end{multicols}

On passe de la fonction à sa dérivée en ajoutant $\frac{\pi}{2}$ et on
passe de la fonction à sa primitive en retirant $\frac{\pi}{2}$.

Autrement dit, dériver et intégrer un courant ou une intensité reviennent
à déphaser le courant ou l'intensité.
\end{document}

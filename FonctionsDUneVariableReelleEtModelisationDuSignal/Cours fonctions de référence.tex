\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\begin{document}
\maketitle
\section{Fonction linéaire et affine}
\subsection{Fonction linéaire}

\begin{multicols}{2}
Une fonction linéaire est une fonction $f:x\mapsto ax$, où $a$ est un nombre réel non nul.

La représentation graphique d'une telle fonction est une droite passant par l'origine. Le nombre $a$ est le coefficient directeur de cette droite et est aussi appelé « pente ».

\columnbreak

\begin{center}
\begin{tikzpicture}[scale=0.4,domain=-5:5]
  \tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]

  %\draw[very thin,color=gray] (-5,-5) grid (5,5);

  \draw[->] (-5,0) -- (5,0) node[right] {$x$};
  \draw[->] (0,-5) -- (0,5) node[above] {$f(x)$};

  \draw[color=red]    plot (\x,\x)             node[right] {$f(x) =x$};
\end{tikzpicture}
\end{center}
\end{multicols}

\subsection{Fonction affine}
\begin{multicols}{2}
Une fonction affine est une fonction linéaire « décalée » (verticalement de $b$ ou horizontalement de $-\frac{b}{a}$. Si $a$ et $b$ sont deux nombres réels, on note $f:x\mapsto ax+b$.

La représentation graphique d'une telle fonction est une droite.

\columnbreak

\begin{center}
\begin{tikzpicture}[scale=0.4,domain=-4:2]
  \tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]

  %\draw[very thin,color=gray] (-5,-5) grid (5,5);

  \draw[->] (-5,0) -- (5,0) node[right] {$x$};
  \draw[->] (0,-5) -- (0,5) node[above] {$f(x)$};

  \draw[color=red]    plot (\x,2*\x+3)             node[left] {$f(x) =2x+3$};
\end{tikzpicture}
\end{center}
\end{multicols}

\section{Fonction carrée}

\begin{multicols}{2}
La fonction carrée de référence est la fonction $f:x\mapsto x^2$. Cette fonction est dite paire, c'est à dire que pour tout $x$ réel, $f(x)=f(-x)$.

La représentation graphique est une parabole symétrique par rapport à l'axe $Oy$ des ordonnées.

\columnbreak

\begin{center}
\begin{tikzpicture}[scale=0.4,domain=-2.2:2.2]
  \tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]

  %\draw[very thin,color=gray] (-5,-5) grid (5,5);

  \draw[->] (-5,0) -- (5,0) node[right] {$x$};
  \draw[->] (0,-5) -- (0,5) node[above] {$f(x)$};

  \draw[color=red]    plot (\x,\x*\x)             node[left] {$f(x) =x^2$};
\end{tikzpicture}
\end{center}
\end{multicols}

\section{Fonction cube}

\begin{multicols}{2}
La fonction cube de référence est la fonction $f:x\mapsto x^3$. Cette fonction est dite impaire, c'est à dire que pour tout $x$ réel, $f(x)=-f(-x)$.

La représentation graphique est symétrique par rapport à l'origine $O$ du repère.

\columnbreak

\begin{center}
\begin{tikzpicture}[scale=0.4,domain=-1.5:1.5,smooth]
  \tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]

  %\draw[very thin,color=gray] (-5,-5) grid (5,5);

  \draw[->] (-5,0) -- (5,0) node[right] {$x$};
  \draw[->] (0,-5) -- (0,5) node[above] {$f(x)$};

  \draw[color=red]    plot (\x,\x^3)             node[left] {$f(x) =x^3$};
\end{tikzpicture}
\end{center}
\end{multicols}

\section{Fonction inverse}

\begin{multicols}{2}
La fonction inverse est la fonction $f:x\mapsto\frac{1}{x}$, définie pour $x\neq 0$. On dit parfois que le domaine de définition est $\mathbf{R}^*$.

\columnbreak

\begin{center}
\begin{tikzpicture}[scale=0.4,domain=0.2:5,samples=50,smooth]
  \tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]

  %\draw[very thin,color=gray] (-5,-5) grid (5,5);

  \draw[->] (-5,0) -- (5,0) node[right] {$x$};
  \draw[->] (0,-5) -- (0,5) node[above] {$f(x)$};

  \draw[color=red]    plot (\x,1/\x)             node[right] {$f(x) =\frac{1}{x}$};
  \draw[color=red]    plot (-\x,-1/\x)            node { };
\end{tikzpicture}
\end{center}
\end{multicols}

\end{document}

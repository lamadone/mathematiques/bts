\documentclass[a5paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a5paper,top=1.5cm,bottom=1.5cm,left=1.5cm,right=1.5cm]{geometry}

%\usepackage{multicol}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage[upright]{kpfonts}

\usepackage{theorem}


\usepackage{pgfplots}

\title{Analyse -- Tracés de courbes}
\author{BTS Maintenance Industrielle -- 1ère année}


\makeatletter
\hypersetup{
 PDFTitle={\@title}
 PDFAuthor={\@author}
}
\makeatother


\newtheorem{theoreme}{Théorème}

\begin{document}
\section{Généralités}
Soit \(f\) une fonction définie sur \(I\), un intervalle de
\(\R\). On note \(f'\) sa dérivée si elle existe sur \(I\).
\begin{tabular}{|c|c|}
  \hline Fonction & Dérivée\\
  \hline\(k\in\R\) & \(0\)\\
  \(x\) & \(1\)\\
  \(x^2\) & \(2x\)\\
  \(x^n,\,n\in\N\) & \(nx^{n-1}\)\\
  \(\frac{1}{x}\) & \(\frac{-1}{x^2}\)\\
  \(\sqrt{x} \) & \(\frac{1}{2\sqrt{x}}\)\\
  \(\ln(x)\) & \(\frac{1}{x}\)\\
  \(e^x\) & \(e^x\)\\
  \(e^{ax}\) & \(ae^{ax}\)\\
  \hline
\end{tabular}

\begin{theoreme}
  Si \(u\) et \(v\) sont deux fonctions dérivables de dérivée \(u'\)
  et \(v'\), alors la dérivée de la fonction \(u+v\) est la fonction \(v'+v'\).
\end{theoreme}
\begin{theoreme}
  Si \(u\) et \(v\) sont deux fonctions dérivables de dérivée \(u'\)
  et \(v'\), alors la fonction \(uv\) est dérivable et sa dérivée est \(uv'+v'u\).
\end{theoreme}
\begin{tabular}{|c|c|}
  \hline Fonction & Dérivée\\
  \hline\(uv\) & \(u'v+v'u\)\\
  \(u^2\) & \(2u'u\)\\
  \(u^n,\,n\in\N\) & \(nu'u^{n-1}\)\\
  \(\frac{1}{u}\) & \(\frac{-u'}{u^2}\)\\
  \(\frac{u}{v}\) & \(\frac{u'v-v'u}{v^2}\)\\
  \(\sqrt{u} \) & \(\frac{u'}{2\sqrt{u}}\)\\
  \(e^u\) & \(u'e^u\)\\
  \(\ln(u)\) & \(\frac{u'}{u}\)\\
  \hline
\end{tabular}
\section{Étude de fonction}
Le but d'une étude de fonction est de pouvoir rapidement tracer
l'allure de la courbe représentative \(\mathcal{C}_f\) de la fonction
\(f\). Pour cela, il est recommandé de procéder de la façon suivante :
\begin{enumerate}
\item Calculer la fonction dérivée \(f'\)
\item On résout l'équation \(f'(x)=0\)
\item On vérifie que la (ou les) solution(s) de cette équation sont
  dans le domaine d'étude
\item On trace le tableau de variation en indiquant sur la première
  ligne les points où f'(x)=0, sur la seconde ligne \(f'x)\) et le
  signe de cette fonction et sur la dernière ligne, les variations de
  \(f\)
\end{enumerate}

\section{Étude de quelques fonctions usuelles}
\subsection{Étude des fonctions polynômes}
Il s'agit des fonctions de la forme $f:x\mapsto ax^2+bx+c$ où $a, b$ et $c$ sont des réels. On suppose de plus que $a$ est non nul. Ces fonctions sont définies sur $\R$. De plus, si $b=0$, la fonction $f$ est paire. On peut calculer la dérivée $f'(x)=2ax+b$. Il est clair que pour $x=\frac{-b}{2a}$, $f'(x)=0$. Il vient donc que la fonction $f$ est décroissante sur $\left]-\infty;-\frac{b}{2a}\right]$ et croissante sur $\left[-\frac{b}{2a};\infty\right[$. si $a>0$ et inversement. De plus, si $a$ est positif (respectivement négatif), le minimum (respectivement maximum) est atteint en $x=-\frac{b}{2a}$ et vaut $f(x)=\frac{b^2}{4a}-\frac{b^2}{2a}+c=\frac{4ac-b^2}{4a}$.\par
Dans ce cas précis, on peut calculer explicitement les racines : si $b^2-4ac\geq 0$, $f(x)=0$ en $x=\frac{-b+\sqrt{b^2-4ac}}{2a}$ et $x=\frac{-b-\sqrt{b^2-4ac}}{2a}$.\par
Si on suppose $a\leq 0$, on a :
\[
\begin{array}{c|ccccr}
x     & -\infty & & -\frac{b}{2a}  &    & +\infty  \\
\hline
f'(x) & & +    &  0 & - &  \\
\hline
 &  & & \frac{4ac-b^2}{4a} & & \\       % ligne des valeurs "max"
f(x) &  &\nearrow   &  &\searrow  & \\   % flèches
& -\infty & & & & -\infty \\          % ligne des valeurs "min"
\hline
\end{array}
\]

\subsection{Les fonctions logarithme et exponentielle}
\subsubsection{Définition et propriétés de la fonction logarithme}
On admet l'existence d'une fonction \(\ln\) définie sur
\(\left[0;+\infty\right]\) telle que:
\begin{enumerate}
 \item $\ln(1)=0$
 \item $\ln(1/a)=-\ln(a)$
 \item $\ln(a/b)=\ln(a)-\ln(b)$
 \item Si $r$ est un rationnel, $\ln(a^r)=r\ln a$
\end{enumerate}
On admet également que $\ln' x=\frac{1}{x}$. Cette égalité nous prouve
la monotonie de la fonction $\ln$.\par
On  a donc le tableau de variation suivant :
\[
\begin{array}{c||lcr}
x & 0 & 1 & +\infty\\
\hline f'(x) & & + & \\
\hline  & & & +\infty\\
f(x)&  &\nearrow  & \\
& -\infty  & & \\
\end{array}
\]
On note $e$ le nombre tel que $\ln(x)=1$. Ce nombre est défini de façon unique par le théorème des valeurs intermédiaires.\par

\begin{tikzpicture}
  \begin{axis}
    \addplot expression[domain=0:10] {ln(x)} ;
  \end{axis}
\end{tikzpicture}

\subsubsection{Définition et propriétés de la fonction exponentielle}
On définit la fonction exponentielle comme la fonction réciproque de la fonction logarithme. Si on note cette fonction $\exp$, on doit avoir que pour tout nombre réel $x$, $\ln(\exp(x)) = x$.\par
De cette définition, on tire que
\begin{enumerate}
 \item $\exp(0)=1$
 \item $\exp(a)\exp(b)=\exp(a+b)$
 \item $\frac{\exp(a)}{\exp(b)}=\exp(a-b)$
\end{enumerate}
$(\ln\circ\exp)'=\exp'\times\ln'\circ\exp=\frac{\exp'}{\exp}=1$ nous permet d'affirmer que $\exp'=\exp$. On prouve là aussi que $\exp$ est une fonction strictement croissante.\par
\input{exp}\par
On remarquera que $\exp(1)=e$. Ceci légitimisera l'écriture $\exp(x)=e^x$.
\subsubsection{Logarithmes et exponentielles de base $a$}
On peut définir une fonction $\log_a=\frac{1}{\ln a}\ln$. Dans ce cas, la fonction réciproque est la fonction $a^x$. En physique, on prendra souvent $a=10$ et on notera la fonction $\log$. \par
Ces fonctions partagent évidement les mêmes propriétés que les fonctions logarithmes et exponentielles.



\end{document}

\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage[align=parleft]{enumitem}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\setlength{\headsep}{0pt}
\renewcommand{\headrulewidth}{0pt}
\newcommand{\e}{\varepsilon}
\setlength\parindent{0pt}


\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\begin{document}
\maketitle

\section{Fonction logarithme}
On définit une fonction qui possède la propriété suivante :
\begin{propriete}
  Pour tout réels $a$ et $b$ strictmenent positifs, $\ln{(ab)} = \ln{a}+\ln{b}$
\end{propriete}
Il en découle les propriétés suivantes, vraies pour $a$ et $b$ deux
nombres réels strictement positifs et $n$ un entier naturel :
\begin{corollaire}~\\[-7mm]
  \begin{multicols}{3}
    \begin{itemize}
      \item $\ln{1}=0$
      \item $\ln{\displaystyle\frac{1}{a}}=-\ln{a}$
      \item $\ln{\displaystyle\frac{a}{b}}=\ln{a}-\ln{b}$
      \item $\ln{a^n}=n\ln{a}$
    \end{itemize}
  \end{multicols}
\end{corollaire}

\begin{propriete}
  Les limites de la fonctions logarithme sont :
  \begin{itemize}
    \item $\displaystyle{\lim_{\substack{x\rightarrow 0\\x>0}}\ln{x} = 
      -\infty}$
    \item $\displaystyle{\lim_{x\rightarrow +\infty}\ln{x}=+\infty}$
  \end{itemize}
\end{propriete}

\begin{propriete}
  La dérivée de $ln$ est $x\mapsto\frac1x$
\end{propriete}

\begin{propriete}
  Si $u$ est une fonction dérivable à valeurs strictement
  positives sur un intervalle $I$, alors $\ln u$ est dérivable et $(\ln
  u)' = \displaystyle\frac{u'}{u}$
\end{propriete}

Puisque la dérivée est positive pour tout $x$ réel strictement positif,
on en déduit que $\ln$ est croissante sur $\R_+^{*}$.

On admet que la courbe représentative de la fonction logarithme est
la suivante :
\begin{center}
\begin{tikzpicture}[scale=0.7,domain=0.01:5,samples=75,smooth]
  \tkzInit[xmin=-0.5,ymin=-3,xmax=9,ymax=2.5]
  \tkzClip
  %\draw[very thin,color=gray] (-5,-5) grid (5,5);

  \draw[->] (-0.5,0) -- (5,0) node[right] {$x$};
  \draw[->] (0,-5) -- (0,2.5) node[above] {$f(x)$};

  \draw[color=red]    plot (\x,ln \x)             node[right] {$f(x) =\ln{x}$};
\end{tikzpicture}
\end{center}

La fonction $\ln$ prenant toutes les valeurs réelles lorsque $x$
parcourt la demi-droite positive, on dit qu'elle est bijective. De plus,
l'équation $\ln x = x_0$ possède une seule solution dans
$\R_+^{*}$.

\section{Fonction exponentielle}
La fonction logarithme ($\ln$) étant bijective de $\mathbf{R}^*_+$ dans
$\mathbf{R}$, on peut définir sur ce dernier ensemble une réciproque,
appellée exponentielle, notée $e$ ou $\exp$.

Par définition, on a les égalités suivantes :
\begin{definition}
  Pour tout $a$ réel strictement positif, $e^{\ln{a}}=a$ et pour tout
  $b$ réel, $\ln{e^b}=b$.
\end{definition}
De cette définition découlent les propriétés suivantes, vraies pour tout
$a$ et $b$ réels, et $n$ entier naturel :
\begin{corollaire}~\\[-7mm]
  \begin{multicols}{3}
    \begin{itemize}
      \item $e^0=1$
      \item $e^a\times e^b=e^{a+b}$
      \item $\displaystyle\frac{1}{e^a}=e^{-a}$
      \item $\displaystyle\frac{e^a}{e^b}=e^{a-b}$
      \item $(e^a)^n=e^{na}$
    \end{itemize}
  \end{multicols}
\end{corollaire}

On peut également dire que pour tout $x$ réel, $e^x>0$.

Par symétrie avec la droite d'équation $y=x$, one en déduit l'allure de
la courbe représentative de la fonction $x\mapsto\text{e}^x$.
\begin{center}
\begin{tikzpicture}[scale=0.4,domain=-5:1.9,samples=50,smooth]
  \tkzInit[xmin=-5,ymin=-0.5,xmax=5,ymax=5]

  %\draw[very thin,color=gray] (-5,-5) grid (5,5);

  \draw[->] (-5,0) -- (5,0) node[right] {$x$};
  \draw[->] (0,-0.5) -- (0,5) node[above] {$f(x)$};

  \draw[color=red]    plot (\x,exp \x)             node[right] {$f(x)
  =\text{e}^{x}$};
\end{tikzpicture}
\end{center}

\begin{propriete}
  Les limites de la fonctions exponentielle sont :
  \begin{itemize}
    \item $\displaystyle{\lim_{x\rightarrow -\infty}e^{x}=0}$
    \item $\displaystyle{\lim_{x\rightarrow +\infty}e^{x}=+\infty}$
  \end{itemize}
\end{propriete}

\begin{propriete}
  La dérivée de $x\mapsto e^x$ est $x\mapsto e^x$
\end{propriete}

\begin{propriete}
  Si $u$ est une fonction dérivable sur un intervalle $I$, alors $e^u$
  est dérivable et $(e^u)' = u'e^{u}$
\end{propriete}

\section{Croissances comparées}

On parle de croissance comparées lorsqu'on compare la croissance de deux
fonctions. Ici, on compare la croissance des fonctions $x\mapsto
x^\alpha$, $\ln$ et $\exp$.

\begin{propriete}
  Si $\alpha>0$, $\displaystyle{\lim_{t\rightarrow
  +\infty}\frac{e^t}{t^\alpha} = +\infty}$
\end{propriete}

\begin{propriete}
  Si $\alpha>0$, $\displaystyle{\lim_{t\rightarrow
  +\infty}\frac{\ln t}{t^\alpha} = +0}$
\end{propriete}

\end{document}

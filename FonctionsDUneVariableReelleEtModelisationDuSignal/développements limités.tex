\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,top=1.0cm,bottom=2.0cm,hmargin=2.0cm]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\renewcommand{\headrulewidth}{0pt}
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS MI}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\begin{document}
\maketitle

Dans tout ce cours, $f$ est une fonction définie et dérivable dans un
intervalle contenant 0 et $\varepsilon$ est une fonction telle que
$\displaystyle\lim_{x\rightarrow 0}\e(x) =0$.

\section{Un premier développement limité par l'exemple}

Comme $f$ est définie est dérivable dans un intervalle contenant $0$,
elle est en particulier dérivable en $0$ et
$f'(0)=\displaystyle \lim_{h\rightarrow0}\frac{f(0+h)-f(0)}{h}$, ce qui
conduit à l'égalité suivante :
\[ \boxed{f(h)=f(0)+hf'(0) +h\varepsilon(h)} \]

L'égalité précédente est le développement limité à l'ordre 1, d'une
fonction $f$ quelconque. Prenons le cas $f=\exp$ et remplaçons $h$ par
$x$. L'équation précédente devient \[ \boxed{e^x=1+x+x\e(x)} \]

On admet que si $f$ possède un développement limité à l'ordre $n$,
celui-ci est unique.

De façon plus générale, on obtient les résultats suivants :
\begin{itemize}
  \item $e^x =
    1+x+\frac{x^2}{2}+\frac{x^3}{6}+\cdots+\frac{x^n}{n!}+x^n\e(x)$ ;
  \item $\frac{1}{x+1} = 1-x+x^2+\cdots+(-1)^nx^n+x^n\e(x)$ ;
  \item $\ln{(1+x)} =
    x-\frac{x^2}{2}+\frac{x^3}{3}+\cdots+\frac{(-1)^{n+1}x^n}{n}+x^n\e(x)$ ;
  \item $(1+x)^\alpha =
    1+\frac{\alpha}{1}x+\frac{\alpha(\alpha-1)}{2}x^2+\cdots+\frac{\alpha(\alpha-1)\cdots(\alpha-n+1)}{n!}x^n+x^n\e(x)$ ;
\end{itemize}

\begin{remarque}
  Le développement limité d'une fonction permet d'en donner une valeur
  approchée :\begin{itemize}
    \item $e^{\np{0.01}} \approx
      \FPexp{\fpres}{0.01}\FPround{\fpres}{\fpres}{10}\np{\FPprint{\fpres}}$
    \item $e^{\np{0.01}} \approx
      1+\np{0.01}+\frac{1}{2}\np{0.01}^2+\frac{1}{6}\np{0.01}^3$
    \item $\phantom{e^{\np{0.01}}} \approx
      \FPeval{\fpres}{1+0.01+(0.01^2)/2+(0.01^3)/6}
      %\FPupn{\fpres}{0.01 6 / 1 2 / + 0.01 * 1 + 0.01 * 1 +}
      \FPround{\fpres}{\fpres}{10}\np{\FPprint{\fpres}}$

    \end{itemize}
\end{remarque}

\section{Limite, tangente à la courbe et position de la tangente}

\begin{propriete}
  Le terme de degré 0 donne la limite ou la valeur en 0.
\end{propriete}

\begin{propriete}
  Les termes de degré 0 et 1 donnent l'équation de la tangente.
\end{propriete}

\begin{exemple}(en 0)
    \begin{itemize}
      \item $f$ de développement limité $1+3x+x\e(x)$ a pour équation de
        tangente $y=3x+1$
      \item $f$ de développement limité $3+x^2+x^2\e(x)$ a pour équation de
        tangente $y=3$
      \item $f$ de développement limité $-x+\frac{x^2}{3}+x^2\e(x)$ a pour
        équation de tangente $y=-x$
    \end{itemize}
\end{exemple}

Pour connaître la position de la tangente, il faut étudier le signe de
$f(x)-y(x)$ où $y(x)$ est l'équation de la tangente. Comme ici, on
approche $f$ localement par son développement limité, on a la propriété
suivante :

\begin{propriete}
  \begin{itemize}
    \item Si le premier terme non nul de degré supérieur ou égale à 2 du
  développement limité est strictement positif, la courbe est au dessus
  de la tangente ;
    \item si le premier terme non nul de degré supérieur ou égale à 2 du
  développement limité est strictement négatif, la courbe est au dessous
  de la tangente ;
    \item si le premier terme non nul de degré supérieur ou égale à 2 du
      développement limité n'est pas de signe constant, la tangente
      coupe la courbe et les raisonnements précédents s'appliquent sur
      les intervalles où le signe ne change pas.
  \end{itemize}
\end{propriete}

\section{Opérations sur les développements limités}

\begin{propriete}
  Si $f$ et $g$ possèdent un développement limité à l'ordre $n$ en 0,
  alors le développement limité de $f+g$ en 0 est la somme des
  développements limités.
\end{propriete}

\begin{exemple} (à l'ordre 2 en 0):

  $e^x+\frac{1}{1+x}=1+x+\frac{x^2}{2}+1-x+x^2+x^2\e(x)$

  $\phantom{e^x+\frac{1}{1+x}}=2+\frac{3}{2}x^2+x^2\e(x)$
\end{exemple}

\begin{propriete}
  Si $f$ et $g$ possèdent un développement limité à l'ordre $n$ en 0,
  alors le développement limité de $fg$ à l'ordre $n$ est le produit
  tronqué des développements limités.
\end{propriete}

\begin{exemple} (à l'ordre 2 en 0) :

  $\frac{e^x}{1+x}=e^x\times\frac{1}{1+x}$

  $\phantom{\frac{e^x}{1+x}}=(1+x+\frac{x^2}{2}+x^2\e(x))(1-x+x^2+x^2\e)$

  $\phantom{\frac{e^x}{1+x}}=1-x+x^2+x-x^2+\frac{x^2}{2}+x^2\e(x)$
  {\footnotesize (on ne développe que les termes de degré inférieur à
  l'ordre.)}

  $\phantom{\frac{e^x}{1+x}}=1+\frac{x^2}{2}+x^2\e(x)$
\end{exemple}

\begin{remarque}
  $x^n\e(x)$ veut dire que tous les termes d'ordre supérieur sont
  négligeables.
\end{remarque}

\begin{propriete}
  Si $f$ possède un développement limité, on peut obtenir le
  développement limité de $x\mapsto f(kx)$
\end{propriete}

\begin{exemple} (à l'ordre 3 en 0)

  $e^{2x}=1+2x+\frac{(2x)^2}{2}+\frac{(2x)^3}{3!}+x^3\e(x)$

  $\phantom{e^{2x}}=1+2x+\frac{4x^2}{2}+\frac{8x^3}{6}+x^3\e(x)$

  $\phantom{e^{2x}}=1+2x+2x^2+\frac{4}{3}x^3+x^3\e(x)$
\end{exemple}

\section*{Exercices}

\begin{exercice}
  Soit $f$ la fonction dont l'expression est $f(x)=(x-1)e^{2x}$
  \begin{enumerate}
    \item Vérifier que le développement limité à l'ordre 3 en 0 de $f$
      est $-1-x+\frac{2\,x^3}{3}+x^3\e(x)$.
    \item Donner l'équation de la tangente en 0 au graphe de $f$.
    \item Indiquer la position relative de la tangente.
  \end{enumerate}
\end{exercice}

\begin{exercice}
  Soit $g$ la fonction définie par $(2x+3)e^{-x}$.
  \begin{enumerate}
    \item Vérifier que le développement limité de $g$ à l'ordre 2 en 0
      est $3-x-\frac{x^2}{2}+x^2\e(x)$.
    \item Donner l'équation de la tangente en 0 au graphe de $g$.
    \item Indiquer la position relative de la tangente.
  \end{enumerate}
\end{exercice}
\end{document}

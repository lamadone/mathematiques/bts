\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\begin{document}
\maketitle
À notre niveau, une matrice est un tableau de nombre sur lequel on va
définir quelques opérations. Ainsi si $a_1$, $a_2$, $a_3$ et $a_4$ sont
quatres nombres réels, ils définissent la matrice $A$ de dimension
$2\times 2$, avec \[ A = \left(\begin{array}{cc} a_1 & a_2 \\ a_3 & a_4
\end{array}\right) \]

De façon plus générale, une matrice $m\times n$ comporte $m$ lignes et
$n$ colonnes. Le terme $a_{i,j}$ de la matrice $A$ est le terme situé
sur la $i$-ième ligne et la $j$-ième colonne.

On se limite dans ce cours aux matrices carrés ($n \times n$) avec $n=2$
ou $n=3$.

\begin{definition} Addition

  La somme de deux matrices $A$ et $B$ est la matrice notée $A+B$ dont
  chaque coefficient est la somme des coefficients.

  Si $A = \left(\begin{array}{cc} a_{1,1} & a_{1,2} \\ a_{2,1} & a_{2,2}
\end{array}\right) $ $B = \left(\begin{array}{cc} b_{1,1} & b_{1,2} \\
  b_{2,1} & b_{2,2} \end{array}\right) $, alors $A+B=
  \left(\begin{array}{cc} a_{1,1} + b_{1,1} & a_{1,2} + b_{1,2} \\
  a_{2,1} + b_{2,1} & a_{2,2} + b_{2,2} \end{array}\right) $

\end{definition}

\begin{exemple}
  Additionner les matrices $\left(\begin{array}{cc} 1 & 2 \\ 3 & 4
  \end{array}\right)$ et $\left(\begin{array}{cc} 6 & 7 \\ 8 & 9
  \end{array}\right)$.

\end{exemple}

On appelle matrice nulle la matrice $\left(\begin{array}{cc} 0 & 0 \\
  0 & 0 \end{array}\right)$.

\begin{definition}Multiplication par un scalaire

  Multiplier une matrice par un nombre, c'est multiplier tout ces
  éléments par ce nombre.
\end{definition}

\begin{exemple}

  Multiplier la matrice $\left(\begin{array}{cc} 1 & 2 \\ 3 & 4
  \end{array}\right)$ par 2.
\end{exemple}

\begin{definition} Multiplication

  Le produit de deux matrices $A$ et $B$ est la matrice notée $A\times B$
  dont les coefficients sont obtenus de la façon suivante :

  \newcommand{\myunit}{1cm}
  \tikzset{
    node style sp/.style={draw,circle,minimum size=0.7\myunit},
    node style ge/.style={circle,minimum size=0.7\myunit},
    arrow style mul/.style={draw,sloped,midway,fill=white},
    arrow style plus/.style={midway,sloped,fill=white},
}

\begin{center}
  \tiny

  \begin{tikzpicture}[>=latex,scale=0.7]
    \matrix (A) [matrix of math nodes,%
      left delimiter  = (,%
      nodes = {node style ge},%
    right delimiter = )] at (0,0)
    {%
      a_{1,1} & a_{1,2} & a_{1,3} \\
      \node[node style sp] {a_{2,1}}; & \node[node style sp]
      {a_{2,2}}; & \node[node style sp] {a_{2,3}}; \\
      a_{3,1} & a_{3,2} & a_{3,3} \\
    };

    \matrix (B) [matrix of math nodes,%
      left delimiter  = (,%
      nodes = {node style ge},%
    right delimiter = )] at (4*\myunit,4*\myunit)
    {
      b_{1,1} & \node[node style sp] {b_{1,2}}; & b_{1,3} \\
      b_{2,1} & \node[node style sp] {b_{2,2}}; & b_{2,3} \\
      b_{3,1} & \node[node style sp] {b_{3,2}}; & b_{3,3} \\
    };

    \matrix (C) [matrix of math nodes,%
      left delimiter  = (,%
      nodes = {node style ge},%
    right delimiter = )] at (4*\myunit,0)
    {
      c_{1,1} & c_{1,2} & c_{1,3} \\
      c_{2,1} & \node[node style sp,red] {c_{2,2}}; & c_{2,3} \\
      c_{3,1} & c_{3,2} & c_{3,3} \\
    };

    \draw[blue] (A-2-1.north) -- (C-2-2.north);
    \draw[blue] (A-2-1.south) -- (C-2-2.south);
    \draw[blue] (B-1-2.west)  -- (C-2-2.west);
    \draw[blue] (B-1-2.east)  -- (C-2-2.east);
    \draw[<->,red](A-2-1) to[in=180,out=90]
    node[arrow style mul] (x) {$a_{21}\times b_{12}$} (B-1-2);
    \draw[<->,red](A-2-2) to[in=180,out=90]
    node[arrow style mul] (y) {$a_{22}\times b_{22}$} (B-2-2);
    \draw[<->,red](A-2-3) to[in=180,out=90]
    node[arrow style mul] (z) {$a_{23}\times b_{32}$} (B-3-2);
    \draw[red,->] (x) to node[arrow style plus] {$+$} (y) to
    node[arrow style plus] {$+$} (z) ;

  \end{tikzpicture}
\end{center}

\end{definition}

\begin{exemple}

  \begin{itemize}
    \item Multiplier les matrices $\left(\begin{array}{cc} 1 & 2 \\ 2 & 4
      \end{array}\right)$ et $\left(\begin{array}{cc} 2 & 3 \\ 4 & 5
      \end{array}\right)$.


    \item Multiplier les matrices  $\left(\begin{array}{cc} 3 & -9
  \\ -1                                                        & 3
  \end{array}\right)$ et $\left(\begin{array}{cc} 0 &  0 \\ 0 & 0
  \end{array}\right)$.

    \item  Multiplier les matrices  $\left(\begin{array}{cc} 3 & -9 \\
      -1 & 3 \end{array}\right)$ et $\left(\begin{array}{cc} 2 & 6 \\
      1 & 3 \end{array}\right)$.

    \item  Inverser le sens du calcul précédent.

      Que constate-t-on ?

    \item Effectuer les produits $\left(\begin{array}{cc} 3 & -9 \\ -1 &
    3 \end{array}\right)$ $\left(\begin{array}{cc} 1 & 0 \\ 0 & 1
    \end{array}\right)$ et celui en inversant l'ordre.

  \end{itemize}
\end{exemple}

Le produit de deux matrices n'est en général pas commutatif ($A\times B
\neq B\times A$) ; de plus, il n'est pas intègre : $A \neq 0$, $B
\neq 0$ et $A\times B = 0$.

On appelle \emph{identité}, noté $I_n$ la matrice
$\left(\begin{array}{cc} 1 & 0 \\ 0 & 1 \end{array}\right)$. Cette
matrice commute avec toutes les matrices.


\end{document}

% !TEX program = latexmk
% !TEX options = -pdf -synctex=1 -interaction=nonstopmode -file-line-error -shell-escape "%DOC%"
% !TEX encoding = UTF-8

\documentclass[class=scrartcl, crop=false, french]{standalone}

\usepackage{lycee}

\title{Rappel sur les suites}
\author{Vincent-Xavier Jumel}
\date{}

\begin{document}

\IfStandalone{%
  \maketitle
}{}


\section{Généralités}

\subsection{Définition}

\begin{definition}
	Une suite numérique est une «collection infinie» de nombres numérotées à 
	partir de 0.\\
	On représente une telle suite par une lettre, définissant la suite dans 
	son ensemble et on note $u$ ou $(u_n)_{n\in\N}$.\\
	$u_n$ s'appelle le \emph{terme général} de la suite $u$.
\end{definition}

\begin{exemple}
	\begin{itemize}
		\item La suite des nombre entiers ;
		\item la suite des entiers pairs ;
		\item la suite des nombres premiers ;
		\item la suite de terme général $p_n = \pi^n$ ;
		\item …
	\end{itemize}
	sont des suites numériques.
\end{exemple}

\begin{note}
	Attention : bien noter $u_n$ et non $u$\scriptsize$n$\normalsize.
\end{note}

\begin{remarque}
	Une «collection finie» s'appelle une \emph{séquence} ou une 
	\emph{famille}.
\end{remarque}

\begin{definition}\label{analyse:suites:def:constante}
	Soit $a$ un nombre réel. On appelle suite constante de terme général $a$ 
	la suite dont tous les termes valent $a$.\\
	Si $a = 0$ , on parle alors de suite nulle.\\
	Par abus de notation, on confond les réels $a$ et les suites constantes 
	de terme général $a$.
\end{definition}

\begin{definition}
	\leavevmode
	\begin{itemize}
		\item On appelle \emph{suite arithmétique} de premier terme $u_0$ et de 
		raison $r$ toute suite définie par $u_0$ et $\forall n \in \N,\ u_{n+1} 
		= u_n + r$.
		\item On appelle \emph{suite géométrique} de premier terme $v_0$ et de
		raison $q$ toute suite définie par $v_0$ et $\forall n \in  \N,\ 
		v_{n+1} = v_n × q$
	\end{itemize}
\end{definition}


\begin{remarque}
	Cette définition est tout à fait compatible avec la 
	définition~\ref{analyse:suites:def:constante}.
\end{remarque}

\begin{definition}
	On dit qu'une suite est croissante, respectivement décroissante, lorsque 
	pour tout $n$ entier naturel, $u_n ≤ u_{n+1}$, respectivement $u_{n+1} ≤ 
	u_n$.\\
	Lorsque les inégalités sont strictes ($<$ ou $>$), on dit strictement 
	croissante ou strictement décroissante.\\
	Si les premiers termes de la suite ne sont pas ordonnées, on dit que la 
	suite est croissante, respectivement décroissante, à partir d'un certain 
	rang. On a alors, pour tout $n$ entier naturel, il existe un rang $n_0$, 
	tel que si $n ≥ n_0$, alors $u_n ≤ u_{n+1}$.\\
	Une suite croissante ou décroissante est dite \textbf{monotone}.
\end{definition}

\begin{proposition}
	\leavevmode
	\begin{itemize}
		\item $u$ est croissante si et seulement si, pour tout $n$ entier 
		naturel, $u_{n+1} - u_n ≥ 0$.
		\item Si $u$ est à valeurs strictement positives, $u_n$ est croissante 
		si et seulement si, pour tout $n$ entier naturel, $\frac{u_{n+1}}{u_n} 
		> 1$.
	\end{itemize}
\end{proposition}
\begin{proof}
	\leavevmode
	\begin{itemize}
		\item Laissée en exercice.
		\item Laissée en exercice.
  \end{itemize}
\end{proof}

\begin{definition}
	Soit $E$ un ensemble de nombres, par exemple $E = \ensemble{u_n}{n \in \N}$
	\begin{itemize}
		\item On dit que $m$ est un \textbf{minorant} de $E$ (ou de la suite), 
		lorsque, pour tout $x \in E,\ m ≤ x$. On dit alors que $E$ (ou la 
		suite) est \textbf{minorée}.
		\item On dit que $M$ est un \textbf{majorant} de $E$ (ou de la suite), 
		lorsque, pour tout $x \in E,\ x ≤ M$. On dit alors que $E$ (ou la 
		suite) est \textbf{majorée}.
		\item Un ensemble qui est à la fois majoré et minoré est dit 
		\textbf{borné}.
	\end{itemize}
\end{definition}

\begin{remarque} On peut parfois parler de minoration (ou majoration) par 
une suite termes à termes, lorsque, pour tout entier naturel $n$, $u_n ≤ 
v_n$. On note alors, abusivement $u ≤ v$.
\end{remarque}

\begin{definition}
	Soient $u$ et $v$ deux suites de terme général $u_n$ et $v_n$.
	\begin{itemize}
		\item La suite $u+v$ est la suite de terme général $u_n + v_n$
		\item La suite $uv$ est la suite de terme général $u_n × v_n$.
	\end{itemize}
\end{definition}

\begin{proposition}
		Si $u$ et $v$ sont deux suites croissantes, alors $u+v$ est 
		croissante et, si de plus, $u$ et $v$ sont à valeurs positives, $uv$ 
		est croissante.
\end{proposition}
\begin{proof}
Si $u$ est croissante, alors $u_{n+1} - u_n ≥ 0$. De même, si $v$ 
		est croissante, $v_{n+1} - v_n ≥ 0$. Il vient donc que $u_{n+1} - u_n + 
		v_{n+1} - v_n ≥ 0$ et donc $(u_{n+1}+v_{n+1})-(u_n+v_n) ≥ 0$.
		
		Soient $u$ et $v$ deux suites croissantes, à valeurs strictement 
		positives. On a alors $\frac{u_{n+1}}{u_n} ≥ 1$ et $\frac{v_{n+1}}{v_n} 
		≥ 1$ d'où $\frac{u_{n+1}v_{n+1}}{u_nv_n} ≥ 1$.
\end{proof}

\subsection{Raisonnement par récurrence}

Le raisonnement par récurrence est un mode de raisonnement particulier aux 
entiers naturels, ou aux éléments d'une suite indexée par les entiers 
naturels. Il permet en particulier de montrer une propriété sur tous les 
éléments d'une suite, en vérifiant la propriété sur le premier élément de 
la suite, puis en montrant que supposer la propriété vraie pour un élément 
quelconque entraîne que la proposition est vraie pour l'élément suivant.

\begin{theoreme}
	Soit $\mathcal{P}_n$ une proposition indexée par $n$. Si $\mathcal{P}_0$ est vraie et si 
  $\mathcal{P}_n \implies \mathcal{P}_{n+1}$, alors $\forall n \in \N,\
  \mathcal{P}_n$ est vraie.
\end{theoreme}

\begin{info}
	Ce théorème est un axiome de la théorie des entiers naturels proposée par 
	Guieseppe Peano.
\end{info}

\begin{exemple}
	Démontrons que la somme des entiers naturels de 0 à $n$ s'écrit 
	$\sum_{k=0}^n k = \frac{n(n+1)}{2}$.\\
	Soit $\P_n$ la proposition «$\sum_{k=0}^n k = \frac{n(n+1)}{2}$».\\
	La somme s'initialisant à 0, on a, pour $n = 0$, $\sum_{k=0}^0 = 0$ et 
	$\frac{0×(0+1)}{2} = 0$. La proposition $\P_0$ est donc vraie.\\
	Soit $n$ un entier arbitrairement choisi. Supposons $\P_n$ vraie et 
	démontrons $\P_{n+1}$. $\P_n$ vraie s'écrit $\sum_{k=0}^n k = 
	\frac{n(n+1)}{2}$.
	\begin{align*}
		\sum_{k=0}^{n+1} k & = \frac{n(n+1)}{2} + (n+1) \\
										 	 & = \frac{n(n+1)}{2} + \frac{2(n+1)}{2} \\
										   & = \frac{n(n+1) + 2(n+1)}{2} \\
										   & = \frac{(n+2)(n+1)}{2}
	\end{align*}
	On peut conclure que, pour tout $n$ entier naturel, $\sum_{k=0}^n k = 
	\frac{n(n+1)}{2}$.
\end{exemple}

\begin{savoirrediger}[Rédiger une démonstration par récurrence]
	Pour rédiger une démonstration par récurrence, il faut :
	\begin{itemize}
		\item bien identifier la proposition $\P_n$ et l'écrire explicitement;
		\item écrire l'étape d'\textbf{initialisation}, c'est-à-dire la 
		vérification de la proposition au rang initial ;
		\item écrire l'étape d'\textbf{hérédité}, en mentionnant l'usage de la 
		proposition $\P_n$, appelée ici \textbf{hypothèse de récurrence} ;
		\item écrire la \textbf{conclusion}.
	\end{itemize}
\end{savoirrediger}



\subsection{Quelques méthodes sur les suites}

\begin{savoirfaire}[Écrire un algorithme de calcul des termes d'une suite 
définie par une relation de récurrence.]
\begin{align*}
	& U ← \text{valeur initiale}\\
	& N ← 0\\
	& \text{Tant que } N < \text{seuil}\\
	& \hspace{5mm} U ← \text{expression contenant } U\\
	& \hspace{5mm} N ← N + 1\\	
\end{align*}
\end{savoirfaire}



\begin{savoirfaire}[Tracer les points d'une suite définie par une relation 
de récurrence.]
	Soit $u$ définie par $u_0$ et pour tout $n$ entier naturel, $u_{n+1} = 
	f(u_n)$.
	\begin{itemize}
		\item Tracer la courbe représentative de la fonction $f$ dans un repère.
		\item Tracer, dans le même repère, la droite d'équation $y = x$.
		\item Placer $u_0$ sur l'axe des abscisses.
		\item Reporter cet abscisse sur la courbe de la fonction $f$.
	\end{itemize}
	\begin{center}
 		\begin{tikzpicture}[scale=2.8]
  		\draw [very thin,dashed,gray] (0,0) grid[step=0.5] (5,3.2) ;
  		\draw [thin,dashed] (0,0) grid[] (5,3.2) ;
	  	\draw [thick, ->] (0,0) -- (5.1,0) ;
	  	\draw [thick, ->] (0,0) -- (0,3.2) ;
			\draw [very thick, ->] (0,0) -- (1,0) ;
			\draw (0.5,0) node [below] {$\V{\imath}$} ;
			\draw [very thick, ->] (0,0) -- (0,1) ;
	  	\draw (0,0.5) node [left] {$\V{\jmath}$} ;
	  	\draw (0,0) node [below left] {$O$} ;
	  	\draw [thick,blue] plot [domain=0:5,smooth] (\x, {sqrt(\x+5)} ) ;
	  	\draw [thick,red] (0,0) -- (3.2,3.2) ;
	  	
	  	\draw [blue] (0,0) node [draw,circle,inner sep=1] {} ;
	  	\draw [blue] (0,0) -- (0,2.236) node [draw,circle,inner sep=1] {} -- 
	  	(2.236,2.236) 
	  	node [draw,circle,inner sep=1pt] {};
	  	\draw [blue] (2.236,2.236) -- (2.236,2.690) node [draw,circle,inner 
	  	sep=1] {} -- (2.690,2.690) node [draw,circle,inner sep=1] {};
	  	\draw [blue] (2.690,2.690) -- (2.690,2.773) node [draw,circle,inner 
	  	sep=1] {};
	  	\draw [blue] (2.236,2.236) -- (2.236,0) node [draw,circle,inner 
	  	sep=1pt] {} ; \draw (2.236,0) node [below] {$u_1$ } ;
	  	\draw [blue] (2.690,2.690) -- (2.690,0) node [draw,circle,inner 
	  	sep=1pt] {} ; \draw (2.690,0) node [below] {$u_2$ } ;
	  	\draw [blue] (2.773,2.773) -- (2.773,0) node [draw,circle,inner 
	  	sep=1pt] {} ; \draw (2.773,0) node [below right] {$u_3$ } ;
  	\end{tikzpicture}
  \end{center}
\end{savoirfaire}



\section{Limite d'une suite}

\subsection{Limite finie d'une suite}

\begin{definition}
	On dit qu'une suite $u$ admet une limite finie $\ell$ lorsque tous les 
	intervalles ouverts contenant $\ell$ contiennent tous les termes de 
	$u_n$, à partir d'un certain rang.\\
	On dit alors que $\ell$ est la limite de la suite et on note $\lim_{n\to 
	+\infty} u_n = \ell$.\\
	On dit que la suite \textbf{converge} vers $\ell$.
\end{definition}

Cette définition peut écrire $\forall (a,b) \in \R^2,\ a < b$, tels que 
$\ell \in \interoo{a b}$, $n ≥ n_0 \implies u_n \in \interoo{a b}$. Une 
façon de prendre $a$ et $b$ respectant ces conditions est de se fixer un 
nombre quelconque, $\varepsilon >0 $ et de prendre $a = \ell - \varepsilon$ 
et $b = \ell + \varepsilon$.

\begin{proposition}
	Si une suite converge, alors sa limite est unique.
\end{proposition}
\begin{proof}
	Soit $u$ une suite convergente. Raisonnons par l'absurde. On suppose 
	qu'il existe $\ell_1$ et $\ell_2$ deux limites distinctes. Supposons de 
	plus que $\ell_1 < \ell_2$ et posons $\varepsilon = \frac{\ell_2- 
	\ell_1}{3}$.\\
	Tous les intervalles $I$ contenant $\ell_1$ contiennent tous les termes 
	de la suite. De même, tous les intervalles $J$ contenant $\ell_2$ 
	contiennent tous les termes de la suite.\\
	Or, par construction, si $u_n \in I$, alors $u_n \not\in J$ et 
	réciproquement, si $u_n \in J$, alors $u_n \not\in I$.\\
	Il y a donc contradiction et celle-ci existe toujours si $\ell_1 > 
	\ell_2$.\\
	Donc $\ell_1 = \ell_2$ et la limite est unique.
\end{proof}

\begin{remarque}
	La définition assure que si $\lim_{n\to +\infty} u_n = \ell$, alors 
	$\lim_{n\to +\infty} u_{n+1} = \ell$.
\end{remarque}

\begin{exercise}
	Montrer que la suite $u$ définie par $u_n = \frac1n$ pour tout entier 
	naturel supérieur ou égal à 1 converge vers 0.
\end{exercise}
\begin{solution}[print=true]
	Soient $a$ et $b$ deux nombres positifs, $a < b$ et $I = \interoo{a b}$. 
	$u_n \in I \iff -a < u_n < b \iff -a < \frac1n < b$.\\
	Les termes de la suite étant positifs, cette dernière inégalité est 
	équivalente à $\frac1n < b \iff n > \frac1b$.\\
	Ainsi, à partir du rang $n_0 = \left\lfloor \frac1b 
	\right\rfloor$\footnote{La fonction $\lfloor⋅\rfloor$ est la fonction 
	partie entière qui est l'entier immédiatement en dessous de son argument. 
	}, les termes de la suite $u_n$ sont dans $I$.\\
	La suite converge bien vers 0.
\end{solution}

Cet exemple est assez fondamental, il permet de montrer la convergence de 
la suite de terme général $\frac1n$. De la même façon, on pourrait 
démontrer, après conjecture de la limite, la convergence des suites de 
terme général $\frac1{\sqrt{n}^k}$, où $k \in \N^*$.

\begin{proposition}\label{analyse:suites:prop:combinaison_lineaire}
	Soient $u$ une suite convergente et $a$ un nombre réel. Alors $\lim_{n\to 
	+\infty} u_n + a = \ell + a$ et $\lim_{n\to +\infty} a×u_n = a\ell$
\end{proposition}
\begin{proof}
	Si $u$ est convergente, alors tous les intervalles contenant $\ell$ 
	contiennent tous les termes $u_n$ à partir d'un certain rang. Par 
	translation (ou par homothétie), tous les intervalles contenant $\ell + 
	a$ (ou $a\ell$) contiennent tous les termes de $u_n + a $ (ou $au_n$) à 
	partir d'un certain rang.
\end{proof}

\begin{corollaire}
	Si la suite $u$ converge vers $\ell$, alors la suite de terme général 
	$u_n - \ell$ converge vers 0.
\end{corollaire}
\begin{proof}
	Il s'agit d'une application directe du théorème précédent.
\end{proof}

La comparaison à 0 permet de simplifier des raisonnements et de ne 
considérer que le cas des suites convergentes vers 0 pour les calculs de 
limites.

\begin{proposition}\label{analyse:suites:somme_finie}
	Soient $u$ et $v$ deux suite convergente de limites \textbf{finies} 
	$\ell_1$ et $\ell_2$. 
	Alors $\lim_{n\to +\infty} u_n + v_n = \ell_1 + \ell_2$ et $\lim_{n\to 
	+\infty} u_n × v_n = \ell_1 × \ell_2$.
\end{proposition}
\begin{proof}
	Soient $u$ et $v$ deux suites convergentes. Posons $u' = u - 
	\ell_1$ et $v' = v - \ell_2$. Les suites $u'$ et $v'$ convergent vers 0.\\
	Dans ce cas, tous les intervalles contenant 0 contiennent tous les termes 
	de la suite $u'$ à partir d'un certain rang $n_1$ et ils contiennent tous 
	les termes de la suite $v'$ à partir d'un certain rang $n_2$. Ainsi, à 
	partir du rang $n = \max(n_1,n_2)$, ils contiennent tous les termes de la 
	suite $u'+v'$. D'où la suite $u + v$ converge vers $\ell_1 + \ell_2$.\\ 
	Plus précisément, on peut d'ailleurs considérer que les intervalles de 	
	rayon\footnote{La demi différence des extrémités de l'intervalle.} 
	inférieur à 1, et on obtient aussi que $u'v' - \ell_1\ell_2$ tend vers 0.
\end{proof}



La proposition~\ref{analyse:suites:somme_finie} permet en particulier de 
déterminer des limites de suites dans le cas d'une limite finie. On peut en 
particulier, en utilisant la 
proposition~\ref{analyse:suites:prop:combinaison_lineaire}, admettre que 
les suites de la forme $\suite*{\frac{a}{n}}$, $\suite*{\frac{a}{n^2}}$, 
$\suite*{\frac{a}{n^3}}$ ou $\suite*{\frac{a}{\sqrt{n}}}$ ont pour limite 0, et 
ce pour tout $a$ réel.

\subsection{Limite infinie d'une suite}

\begin{definition}
	Soit $u$ une suite.\\
	On dit que celle-ci a pour limite $+\infty$ quand $n$ tend vers $+\infty$ 
	lorsque tout intervalle de la forme $\interoo{A +\infty}$ contient tous 
	les termes $u_n$ à partir d'un certain rang.\\
	On dit que celle-ci a pour limite $-\infty$ quand $n$ tend vers $+\infty$ 
	lorsque tout intervalle de la forme $\interoo{-\infty A}$ contient tous 
	les termes $u_n$ à partir d'un certain rang.
\end{definition}

On dit parfois que la suite diverge vers $+\infty$

\begin{exemple}
	La suite de terme général $u_n = \sqrt{n}$ a pour limite $+\infty$.\\
	Soit $A$ un réel positif. $u_n > A \iff \sqrt{n} > A \iff n > A^2$. À 
	partir du rang $n_0 = \lfloor A^2 \rfloor + 1$, les termes de la suite 
	$u$ sont dans les intervalles de la forme $\interoo{A +\infty}$.
\end{exemple}

De façon générale, on peut démontrer que les suites de terme général 
$a(\sqrt{n})^k,\ a \in \R_+^*,\ k \in \N^*$ convergent vers $+\infty$.

\begin{proposition}
	Soient $a$ un réel non nul et $u$ une suite divergente vers $+\infty$. 
	Alors la suite $u + a$ diverge aussi vers $+\infty$ et la suite $a×u$ 
	diverge vers $+\infty$ si $a > 0$ et vers $-\infty$ si $a < 0$.
\end{proposition}
\begin{proof}
	Comme $u$ a pour limite $+\infty$, tous les intervalles de la forme 
	$\interoo{A +\infty}$ contiennent tous les termes $u_n$ à partir d'un 
	certain rang. On en déduit que les intervalles de la forme 
	$\interoo{A+a +\infty}$ contiennent une infinité de termes $u_n + a$.\\
	Traitons le cas $a < 0$. Comme $u$ a pour limite $+\infty$, tous les 
	intervalles de la forme $\interoo{A +\infty}$ contiennent tous les 
	termes $u_n$ à partir d'un certain rang. Les intervalles de la forme 
	$\interoo{-\infty a×A}$ contiennent donc tous les termes $a×u_n$ et donc 
	la limite de $a×u$ est $-\infty$.
\end{proof}

\begin{proposition}
	Soient $u$ une suite qui diverge vers $+\infty$ et $v$ une suite de 
	limite \textbf{finie} $\ell$. Alors la suite $u + v$ diverge vers 
	$+\infty$.
\end{proposition}
\begin{proof}
	Admis
\end{proof}

\begin{proposition}
	Soient $u$ une suite qui diverge vers $+\infty$ et $v$ une suite de 
	limite \textbf{finie $\ell$ non nulle.} Supposons de plus $\ell > 0$. 
	Alors la suite $uv$ diverge vers $+\infty$.
\end{proposition}
\begin{proof}
	Admis
\end{proof}

\begin{proposition}
	Soit $u$ une suite qui diverge vers $+\infty$. Alors la suite $\frac1u$ 
	converge vers 0.
\end{proposition}	
\begin{proof}
	Admis
\end{proof}



\subsection{Suites ne possédant pas de limites ou ne pouvant être 
déterminées facilement}

\begin{contreexemple}[Suites ne possédant pas de limites]
	Certaines suites, comme la suite de terme général $u_n = (-1)^n$ ne 
	possède pas de limite. En effet, les seuls valeurs possibles pour la 
	limite sont 1 et $-1$. Supposons que $1$ soit la limite de la suite $u$. 
	Prenons l'intervalle $\interoo{0 2}$. Il contient 1, mais ne contient 
	pas $-1$. Donc 1 n'est pas limite de la suite $u$. En appliquant ce même 
	raisonnement à $-1$, on montre que $-1$ n'est pas non plus limite de la 
	suite $u$. \hfill \qed
\end{contreexemple}

De même, les opérations sur les limites sont délicates lorsque les deux 
suites possèdent pour limite $±\infty$ ou $0$. Les tableaux ci-dessous 
récapitule les limites calculables. Le sigle F.I. signifie forme 
indéterminée.

\begin{center}
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{|l|*{6}{>{\hfill$}p{1cm}<{$\hfill~}|}}
		\hline
		$u$ a pour limite    & \ell   & \ell      & \ell      & +\infty   & 
		-\infty    & +\infty \\ \hline
		$v$ a pour limite    & \ell'  & +\infty   & -\infty   & +\infty   & 
		-\infty    & -\infty \\ \hline
		$u+v$ a pour limite  &  \ell+\ell'      &   +\infty        &   
		-\infty        &      +\infty     
		& -\infty           &  \text{F.I.} \\ \hline
	\end{tabular}
\end{center}

Pour le produit, on donne aussi le tableau suivant, pour $\ell \neq 0$
et $\ell' \neq 0$ :

\begin{center}
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{|l|*{6}{>{\hfill$}p{1cm}<{$\hfill~}|}}
		\hline
		$u$ a pour limite         & \ell  & \ell    & \ell    & +\infty & 
		-\infty & +\infty \\ \hline
		$v$ a pour limite         & \ell' & +\infty & -\infty & +\infty & 
		-\infty & -\infty \\ \hline
		$u\times v$ a pour limite & \ell\ell'      &   +\infty      &   
		-\infty      &    +\infty     
		&  +\infty       &  -\infty\\ \hline
	\end{tabular}
\end{center}

Attention, dans le cas où $\ell = 0$, on a des formes indéterminées :
\begin{center}
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{|l|*{3}{>{\hfill$}p{1cm}<{$\hfill~}|}}
		\hline
		$u$ a pour limite         & 0     & 0       & 0       \\ \hline
		$v$ a pour limite         & \ell' & +\infty & -\infty \\ \hline
		$u\times v$ a pour limite &  0     & \text{F.I.}        & 
		\text{F.I.}        \\ \hline
	\end{tabular}
\end{center}

Pour le quotient, on a toujours dans le cas où $\ell \neq 0$ et $\ell'
\neq 0$ :
\begin{center}
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{|l|*{5}{>{\hfill$}p{1cm}<{$\hfill~}|}}
		\hline
		$u$ a pour limite         & \ell  & \ell    & \ell    & +\infty & 
		-\infty \\ \hline
		$v$ a pour limite         & \ell' & +\infty & -\infty & \ell' & \ell' 
		\\ \hline
		$\frac{u}{v}$ a pour limite &  \frac{\ell}{\ell'}     & 0        &   
		0      &         ± \infty
		&  ± \infty     \\ \hline
	\end{tabular}
\end{center}

Les formes $\frac{0}{0}$, $0 × ± \infty$ ou $\frac{\infty}{\infty}$ sont 
aussi des formes indéterminées. Cependant,  il n'est pas nécessaire de 
faire apparaître cette information.

\begin{savoirfaire}[Lever une indétermination]
	Pour lever une indétermination, dans une somme ou une différence, on 
	essaie d'écrire la limite sous la forme d'un produit en factorisant par 
	le terme de plus haut degré.\\
	Pour lever une indétermination dans le cadre d'un quotient, on essaie 
	également de simplifier après avoir factorisé par le terme de plus haut 
	degré.
\end{savoirfaire}



\section{Comparaison des suites}

\subsection{Théorèmes de comparaison}

\begin{theoreme}\label{analyse:suites:thm:comparaison}
	Soient $u$ et $v$ deux suites telles que $\forall n \in \N,\ n ≥ n_0,\ 
	u_n ≤ v_n$. Si $\lim_{n\to +\infty} u_n = +\infty$, alors  $\lim_{n\to 
	+\infty} v_n = +\infty$
\end{theoreme}
\begin{proof}
	$u$ diverge vers $+\infty$, donc tous les intervalles de la forme 
	$\interoo{A +\infty}$ contiennent tous les termes $u_n$ à partir d'un 
	certain rang $n_1$. On a plus précisément même, à partir d'un certain 
	rang, $A < u_n ≤ v_n$. Donc tous les intervalles de la forme 
	$\interoo{A +\infty}$ contiennent tous les termes $v_n$ à partir d'un 
	certain rang.
\end{proof}

Ce théorème est pratique pour démontrer la divergence d'une suite dans les 
cas d'indétermination.

\begin{exercise}
	$u$ et $v$ sont les suites définies sur $\N$ par $u_n = \sqrt{n^4 + 3n}$ 
	et $v_n = n^2$.
	\begin{enumerate}
		\item Étudier quelques valeurs de $u_n - v_n$ et conjecturer une 
		comparaison.
		\item Démontrer cette conjecture.
		\item En déduire la limite de la suite $u$.
	\end{enumerate}
\end{exercise}

\begin{theoreme}[d'encadrement]
	Soient $u,\ v$ et $w$ trois suites telles qu'à partir d'un certain rang 
	$v_n ≤ u_n ≤ w_n$. Si $\lim_{n\to +\infty} w_n = \lim_{n\to +\infty} v_n 
	= \ell$, alors $\lim_{n\to +\infty} u_n = \ell$.
\end{theoreme}
\begin{proof}
	Admis
\end{proof}

Ce théorème permet d'obtenir souvent des limites de suites dans des cas 
d'indétermination.

\begin{exercise}
	$u$ est la suite définie pour tout nombre entier $n ≥ 1$ par $u_n = 
	\frac{(-1)^n\sin n}{n^3}$.
	\begin{enumerate}
		\item Démontrer que pour tout $n ≥ 1$, \[ \frac{-1}{n^3} ≤ u_n 
		≤\frac{1}{n^3}. \]
		\item En déduire la limite de la suite $u$.
	\end{enumerate}
\end{exercise}


\subsection{Inégalité de Bernoulli et conséquences}

\begin{proposition}[Inégalité de 
Bernoulli]\label{analyse:suites:prop:bernoulli}
	Soit un réel $a > 0$. Pour tout entier $n$, \[ (1+a)^n ≥ 1 +na .\]
\end{proposition}
\begin{proof}
	Par récurrence. Soit $\P_n$ la proposition «$(1+a)^n ≥ 1 +na$».
	\begin{itemize}
		\item \textbf{Initialisation :} Pour $n = 0$, on a $(1+a)^0 = 1$ et $1 
		+ 0×a = 1$. On a donc bien $(1+a)^0 ≥ 1 + 0×a$ et donc $\P_0$ est vraie.
		\item \textbf{Hérédité :} Soit $n$ un entier naturel arbitrairement 
		choisi. Supposons $\P_n$ vraie.\\
		$(1+a)^n ≥ 1 +na \implies (1+a)^{n+1} ≥ (1 +na)(1+a)$.\\
		$(1 +na)(1+a) = 1 + (n+1)a + a^2$. Or $a^2 > 0$, on obtient donc $(1 
		+na)(1+a)  ≥ 1 +  (n+1)a$ ce qui achève l'hérédité.
		\item \textbf{Conclusion :} $\forall a \in \R,\ a > 0,\ \forall n \in 
		\N,\ (1+a)^n ≥ 1 +na$
	\end{itemize}
\end{proof}

L'intérêt principal de cette proposition est de fournir un résultat 
générique concernant les suites géométriques.

\begin{proposition}
	Soit $u$ une suite géométrique de raison $q > 1$ et de premier terme $u_0 
	> 0$. Alors \[ \lim_{n\to +\infty} u_n = +\infty .\]
\end{proposition}
\begin{proof}
	$u$ étant une suite géométrique, son terme général est $u_n = u_0 q^n$. 
	$q >1$ s'écrit $q = 1 + a$, avec $a > 0$. D'après l'inégalité de 
	Bernoulli~\ref{analyse:suites:prop:bernoulli}, $u_n ≥ u_0(1+ na)$.\\
	La suite de terme général $u_0(1+ na)$ est une suite qui diverge vers 
	$+\infty$, et en utilisant le 
	théorème~\ref{analyse:suites:thm:comparaison}, on obtient que $u_n$ 
	diverge vers $+\infty$.
\end{proof}

\begin{note}[Expression explicite des suites arithmétiques et géométriques]
	\begin{itemize}
		\item Une suite arithmétique de raison $r$ et de premier terme $u_0$ a 
		pour terme général $u_n = u_0 + rn$.
		\item Une suite géométrique de raison $q$ et de premier terme $u_0$ a 
		pour terme général $u_n = u_0 q^n$.
	\end{itemize}
	La démonstration se fait par récurrence sur $n$.
\end{note}

\begin{corollaire}
	Soit $u$ une suite géométrique de raison $0 < q < 1$. Alors \[ \lim_{n\to 
	+\infty} u_n = 0 .\]
\end{corollaire}
\begin{proof}
	Soit $u$ une suite géométrique de raison $q$, $0< q < 1$. Considérons $v 
	= \frac1u$ la suite géométrique de raison $\frac1q$. On a $1 < \frac1q$ 
	et donc $\lim_{n\to +\infty} v_n = +\infty$. On en déduit que $\lim_{n\to 
	+\infty} u_n = 0$.
\end{proof}

\begin{exercise}\label{analyse:suites:exo:serie_geometrique}
	Soit $u$ une suite géométrique de premier terme $u_0$ et de raison $q ≠ 
	1$.\\
	Déterminer la somme $\sum_{i=0}^N u_i = u_0 + u_1 + … + u_N$ des $N$ 
	premiers termes de la suite.
\end{exercise}
\begin{solution}[print=true]
	Notons $S$ cette somme. $S = u_0(1 + q + q^2 + … + q^N)$ et $qS = u_0(q + 
	q^2 + q^3 + … + q^{N+1})$. Calculons la différence $S - qS$. \\
	$S - qS = u_0(1 + q + q^2 + … + q^N) - u_0(q + q^2 + q^3 + … + q^{N+1}) = 
	u_0(1 + q - q + q^2 - q^2 + q^3 - … + q^N - q^{N+1}) = u_0(1 - q^{N+1})$.
	On obtient ainsi $S(1 - q) = u_0 (1 - q^{N+1})$. D'où $S = u_0 \frac{1 - 
	q^{N+1}}{1 - q}$.
 \end{solution}

\begin{proposition}
	La série\footnote{La somme infinie des termes d'une suite} géométrique 
	converge si $0 < q < 1$ et vaut $\frac{u_0}{1-q}$.
\end{proposition}
\begin{proof}
	La preuve est dans la solution de 
	l'exercice~\ref{analyse:suites:exo:serie_geometrique}
\end{proof}

\subsection{Suite croissante}

\begin{proposition}
	Soit $u$ une suite. Si $u$ est croissante et converge vers $\ell$, alors 
	tous ses termes sont inférieurs ou égaux à $\ell$.
\end{proposition}
\begin{proof}
	Soit $u$ une suite croissante convergente. Raisonnons par l'absurde. On 
	suppose qu'il existe un rang $p$ tel que $u_p > \ell$. Il existe donc un 
	réel $\beta > 0,\ u_p = \ell + \beta$.\\
	Comme $u$ est croissante, pour $n ≥ p$, $u_n ≥ u_p$.\\
	Comme $u$ est convergente, tous les $u_n$ sont dans un intervalle de la 
	forme $\interoo{\ell - \alpha \ell + \beta}$ pour $\alpha > 0$.\\
	On devrait donc avoir simultanément, à partir d'un certain rang, $u_n ≥ 
	u_p$ et $u_n < u_p$, ce qui est contradictoire. L'hypothèse de 
	l'existence d'un rang qui dépasse la limite est fausse et donc sa 
	négation est vraie.
\end{proof}

\begin{info}[Raisonnement par l'absurde]
	Le raisonnement mis en œuvre ici est dit par l'absurde. Il consiste à 
	ajouter comme hypothèse la négation de la conclusion. On montre ainsi que 
	la proposition et la négation de sa conclusion ne peuvent être vraie 
	simultanément. Comme c'est le fait d'avoir ajouter une hypothèse qui rend 
	le tout impossible, la négation de cette hypothèse est donc vraie.\\
	Ce type de raisonnement est connu depuis l'antiquité, il est ainsi 
	utilisé par Euclide pour démontrer l'existence d'une infinité de nombres 
	premiers. Il repose sur le principe dit du «tiers exclu» qui postule 
	qu'une assertion est soit vraie, soit fausse.
\end{info}

Les théorèmes suivants permettent de conclure sur la convergence des suites.

\begin{theoreme}[convergence monotone]
	\leavevmode
	\begin{itemize}
		\item Toute suite strictement croissante majorée converge.
		\item Toute suite strictement décroissante minorée converge.
	\end{itemize}	
\end{theoreme}
\begin{proof}
	Admis
\end{proof}

Ce théorème est particulièrement utile pour les suites définies par une 
relation du type $u_{n+1} = f(u_n)$

\begin{savoirfaire}[Utiliser le théorème de convergence monotone.]
	Soit $u$ une suite définie par $u_0$ et une relation $u_{n+1} = f(u_n)$
	Si un exercice propose de démontrer successivement que
	\begin{itemize}
		\item la suite est majorée ou minorée ou bornée ;
		\item la suite est croissante ou décroissante.
	\end{itemize}
	Alors on peut certainement utiliser le théorème de convergence monotone.
\end{savoirfaire}

\begin{remarque}
	Attention, ce théorème ne donne pas la limite. Il donne au plus un 
	majorant (ou un minorant) de la limite.
\end{remarque}

\begin{corollaire}
	Toute suite strictement croissante non-majorée diverge vers $+\infty$.
\end{corollaire}
\begin{proof}
	Soit $u$ une suite strictement croissante non majorée. On peut vérifier 
	que tous les termes de la suite sont sont dans les intervalles 
	$\interoo{u_n +\infty}$ à partir du rang $n+1$. Donc la suite est 
	divergente.
\end{proof}



\end{document}


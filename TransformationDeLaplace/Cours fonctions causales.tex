\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS Elec}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\begin{document}
\maketitle
\section{Fonction causale}

\begin{definition}
  Une fonction causale est définie comme une fonction qui vaut 0 si
  $t<0$ et $f(t)$ sinon.
\end{definition}

On définit généralement une fonction causale comme une suite finie de
couples (intervalle,fonction).

Exemple de définition :

$f:t\mapsto\left\{\begin{array}{lr}
  0 & t<0 \\
  g(t) & t\geq 0
  \end{array}\right.$

où $g$ est une fonction définie sur un intervalle $I$.

Souvent, dans un souci de représentation de la réalité, les fonctions
$g$ choisies sont continues par morceaux.

\begin{definition}

  La fonction noté $\mathcal{U}$, définie par
  $t\mapsto\left\{\begin{array}{lr}
    0 & t < 0 \\
    1 & t \geq 0
    \end{array}\right.$ est une fonction causale, dite échelon unité. On
    l'appelle aussi fonction de Heaviside\footnote{Chez les
      anglophones.}.
\end{definition}

\begin{tikzpicture}[scale=0.7,domain=-2:5]
  \draw[->] (-2,0) -- (5,0) node[below] {$t$};
  \draw[->] (0,-2) -- (0,2) node[left ] {$f(t)$};
  \draw[dashed] (-2,-2) grid (5,2) ;

  \draw[color=red,thick]    plot[domain=-2:0] (\x,0);
  \draw[color=red,thick]    plot[domain=0:5] (\x,1);
\end{tikzpicture}


Si $g(t)$ est l'expression d'une fonction causale $f$ sur $[0; +\infty]$,
on peut écrire $f:t\mapsto g(t)\mathcal{U}(t)$.

L'échelon unité permet de définir d'autres fonctions causales comme la
«rampe» : l'expression d'une rampe de coefficient $a$ est
$at\mathcal{U}(t)$.

\begin{tikzpicture}[scale=0.7,domain=-2:5]
  \draw[->] (-2,0) -- (5,0) node[right] {$t$};
  \draw[->] (0,-2) -- (0,2);
  \draw[dashed] (-2,-2) grid (5,2) ;

  \draw[color=red,thick]    plot[domain=-2:0] (\x,0);
  \draw[color=red,thick]    plot[domain=0:2] (\x,\x);
\end{tikzpicture}

\begin{exercice}
  Donner, pour les fonctions suivantes, leur expression, en fonction du
  domaine, puis à l'aide de l'échelon unité.
  \begin{multicols}{3}
    \begin{enumerate}
      \item ~

        \begin{tikzpicture}[scale=0.7,domain=-2:5]
          \draw[->] (-2,0) -- (5,0) node[right] {$t$};
          \draw[->] (0,-2) -- (0,2);
          \draw[dashed] (-2,-2) grid (5,2) ;

          \draw[color=red,thick]    plot[domain=-2:0] (\x,0);
          \draw[color=red,thick]    plot[domain=0:1] (\x,1);
          \draw[color=red,thick]    plot[domain=1:3] (\x,2);
          \draw[color=red,thick]    plot[domain=3:5] (\x,0);
        \end{tikzpicture}
      \item ~

        \begin{tikzpicture}[scale=0.7,domain=-2:5]
          \draw[->] (-2,0) -- (5,0) node[right] {$t$};
          \draw[->] (0,-2) -- (0,2);
          \draw[dashed] (-2,-2) grid (5,2) ;

          \draw[color=red,thick]    plot[domain=-2:0] (\x,0);
          \draw[color=red,thick]    plot[domain=0:1] (\x,1);
          \draw[color=red,thick]    plot[domain=1:3] (\x,{1-(\x-1)});
          \draw[color=red,thick]    plot[domain=3:4] (\x,{-1+2*(\x-3)});
          \draw[color=red,thick]    plot[domain=4:5] (\x,1);
        \end{tikzpicture}
      \item ~

        \begin{tikzpicture}[scale=0.7,domain=-2:5]
          \draw[->] (-2,0) -- (5,0) node[right] {$t$};
          \draw[->] (0,-2) -- (0,2) ;
          \draw[dashed] (-2,-2) grid (5,2) ;

          \draw[color=red,thick]    plot[domain=-2:0] (\x,0);
          \draw[color=red,thick]    plot[domain=0:5] (\x,{sin(\x r)});
        \end{tikzpicture}
    \end{enumerate}
  \end{multicols}
\end{exercice}
\end{document}

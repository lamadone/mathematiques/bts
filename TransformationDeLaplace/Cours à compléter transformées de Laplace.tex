\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{exemple}{Exemple}
\newtheorem{propriete}{Propriété}
\newtheorem{theoreme}{Théorème}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{preuve}{Preuve}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS Elec}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\begin{document}
\maketitle
\tableofcontents
\pagebreak
\section{Transformée de Laplace d'une fonction causale}
\subsection{Calcul de la transformée de Laplace}

\begin{definition}
On définit, pour des fonctions causales, si elle existe\footnote{en BTS,
  les transformées de Laplace existent toujours} :
\[ \mathcal{L}[f(t)]=F(p)=\int_0^{+\infty}f(t)e^{-pt}\,dt \]
\end{definition}

\begin{remarque}
  On ne peut pas calculer directement $\int_0^{+\infty}f(t)e^{-pt}\,dt$.
  Il faut calculer $\int_0^af(t)e^{-pt}\,dt = F(a,p) - F(0,p)$, puis
  calculer la limite $\displaystyle\lim_{x\rightarrow
  +\infty}\int_0^af(t)e^{-pt}\,dt$. Si cette limite n'existe pas, ou
  infinie, on dit que l'intégrale diverge et la fonction n'aura pas de
  transformée de Laplace, sinon, l'intégrale converge et la fonction
  possède une transformée de Laplace.
\end{remarque}

\begin{exemple}~\\

  \begin{itemize}
    \item Calculer $F(p)=\int_0^{+\infty}\mathcal{U}(t)e^{-pt}\,dt$

      \framebox{
        \begin{minipage}{0.99\linewidth}
          \hspace{\linewidth}
          \vspace{4cm}
        \end{minipage}
      }
    \item Calculer $F(p)=\int_0^{+\infty}t\mathcal{U}(t)e^{-pt}\,dt$

      \framebox{
        \begin{minipage}{0.99\linewidth}
          \hspace{\linewidth}
          \vspace{4cm}
        \end{minipage}
      }
  \end{itemize}
\end{exemple}

En pratique, on limite le plus possible le calcul des transformées de
Laplace à l'aide de ces défintions et on utilise les propriétés qui
suivent :

\begin{propriete}
  Pour $f$ et $g$ deux fonctions causales, de transformée de Laplace
  respective $F$ et $G$, et k un nombre réel, on a
  \begin{enumerate}
    \item $\mathcal{L}[f+g](p) = \mathcal{L}[f](p) + \mathcal{L}[g](p)$
    \item $\mathcal{L}[kf(t)] = k\mathcal{L}[f(t)]$
  \end{enumerate}
\end{propriete}

\begin{preuve}
  Ces propriétés découlent des propriétés de linéarité de l'intégrale.
\end{preuve}

\begin{remarque}
  Attention : $\mathcal{L}[f(t)\times g(t)] \neq F(p)\times G(p)$.
  Prendre par exemple $f(t)=\cos(t)$ et $g(t)=\sin(2t)$.
\end{remarque}

\begin{theoreme} du retard

  Si $f$, fonction causale a pour transformée de Laplace $F(p)$, alors
  $\mathcal{L}[f(t-\tau)] = e^{-\tau p}F(p)$
\end{theoreme}

\begin{preuve}
  On calcule $\int_0^{+\infty}f(t-\tau)e^{-pt}\,dt$. On pose $T=t-\tau$,
  ce qui entraîne que $e^{-pt}=e^{-pT}e^{-p\tau}$. L'intégrale devient
  donc $\int_\tau^{+\infty}f(T)e^{-pT}e^{-p\tau}\,dT$. Comme $f(T)$ vaut
  0 si $T<\tau$, on obtient le résultat attendu.
\end{preuve}

\begin{exemple}~\\
  Donner la transformée de Laplace d'un échelon retardé.

  \framebox{
    \begin{minipage}{0.99\linewidth}
      \hspace{\linewidth}
      \vspace{4cm}
    \end{minipage}
  }
  En déduire la transformée de Laplace d'un créneau

  \framebox{
    \begin{minipage}{0.99\linewidth}
      \hspace{\linewidth}
      \vspace{4cm}
    \end{minipage}
  }
\end{exemple}


\begin{propriete}

  Si $f$, fonction causale a pour transformée de Laplace $F(p)$, alors
  $\mathcal{L}[f(at)] = \frac{1}{a}F\left(\frac{p}{a}\right)$
\end{propriete}

\begin{preuve}
  On calcule $\int_0^{+\infty}f(at)e^{-pt}\,dt$ avec le changement de
  variable $t=\frac{T}{a}$. On obtient
  $\frac{1}{a}\int_0^{+\infty}f(T)e^{-\frac{p}{a}T}\,dT$.
\end{preuve}


\begin{propriete}

  Si $f$, fonction causale a pour transformée de Laplace $F(p)$, alors
  $\mathcal{L}[f(t)e^{-at}] = F(p+a)$
\end{propriete}

\begin{preuve}
  On calcule $\int_0^{+\infty}f(t)e^{-at}e^{-pt}\,dt =
  \int_0^{+\infty}f(t)e^{-(p+a)t}\,dt$
\end{preuve}

\subsection{Quelques transformées de Laplace utiles}

\begin{multicols}{2}
  \begin{center}
    \textbf{Signal causal}

    \hspace{0.7cm}

    $s(t) = \mathscr{U}(t)$

    \hspace{0.7cm}

    $s(t) = t\times\mathscr{U}(t)$

  \end{center}
  \columnbreak
  \begin{center}
    \textbf{Transformée de Laplace}

    \hspace{0.7cm}

    $F(p) = $ \hfill~

    \hspace{0.7cm}

    $F(p) = $ \hfill~

  \end{center}
\end{multicols}

Avant de compléter ce tableau, il faut donner la transformée de Laplace
d'autres fonctions usuelles.

\framebox{
  \begin{minipage}{0.99\linewidth}
    En utilisant la propriété 3, calculer la transformée de Laplace de
    $e^{at}$, pour $a$ quelconque, et en déduire la transformée de
    Laplace de $e^{it}$.
    \hspace{\linewidth}
    \vspace{5cm}
  \end{minipage}
}

\framebox{
  \begin{minipage}{0.99\linewidth}
    En utilisant le fait que $\cos(t) = \dfrac{e^{it} + e{-it}}2$ et
    $sin(t) = \dfrac{e^{it} - e{-it}}{2i}$, et ce qui précède, calculer
    les transformées de Laplace de $\cos(t)$ et $\sin(t)$.
    \hspace{\linewidth}
    \vspace{7cm}
  \end{minipage}
}

\framebox{
  \begin{minipage}{0.99\linewidth}
    En utilisant la propriété 2, calculer les transformées de Laplace de
    $\cos(\omega t)$ et $\sin(\omega t)$.
    \hspace{\linewidth}
    \vspace{4cm}
  \end{minipage}
}

\pagebreak
\begin{multicols}{2}
  \begin{center}
    \textbf{Signal causal}

    \hspace{0.7cm}

    $s(t) = \mathscr{U}(t)e^{at}$

    \hspace{0.7cm}

    $s(t) = \mathscr{U}(t)\times\cos(\omega t)$

    \hspace{0.7cm}

    $s(t) = \mathscr{U}(t)\times\sin(\omega t)$

  \end{center}
  \columnbreak
  \begin{center}
    \textbf{Transformée de Laplace}

    \hspace{0.7cm}

    $F(p) = $ \hfill~


    \hspace{0.7cm}

    $F(p) = $ \hfill~

    \hspace{0.7cm}

    $F(p) = $ \hfill~

  \end{center}
\end{multicols}



\section{Dérivation, intégration et limites}

\subsection{Quelques propriétés qui seront utiles}

\begin{theoreme}[de la valeur finale]
  Valeur initiale :
  \[\lim_{p\rightarrow +\infty}pF(p) = \lim_{t\rightarrow 0}f(t)\]

  Valeur finale
  \[\lim_{p\rightarrow 0}pF(p) = \lim_{t\rightarrow +\infty}f(t)\]
\end{theoreme}

\begin{propriete}
  Transformée de Laplace d'une dérivée

  $\mathcal{L}[f'(t)] = pF(p) -f(0)$
\end{propriete}

On a également $\mathcal{L}[f"(t)] = p^2F(p)-pf(0) - f'(0)$

\begin{propriete}
  Dérivée d'une transformée de Laplace

  $F'(p) = \mathcal{L}[-tf(t)]$
\end{propriete}

\subsection{Original}

\begin{definition}
  On appelle original de $F$ la fonction causale $f$ telle que
  $\mathcal{L}[f](p) = F(p)$
\end{definition}

Si l'original existe, alors, il est unique.

L'original possède les mêmes propriétés que la transformée de Laplace.

\section{Applications}

\subsection{Équations différentielles}

On considère une équation différentielle de la forme
$a_0s"(t)+a_1s'(t)+a_2s(t) = b_0e"(t)+b_1e'(t)+b_2e(t)$ et des
conditions initiales portant sur $s(0)$ et $s'(0)$. Si les fonctions $s$
et $e$ sont causales et admettent des transformées de Laplace, alors on
peut résoudre la transformée de Laplace de cette équation
différentielle.

\begin{exemple}
Résoudre $\left\{\begin{array}{l}s'(t)+2s(t)=e(t)\\
  s(0)=0\end{array}\right.$ avec $e(t)=3\left(\mathcal{U}(t) -
  \mathcal{U}(t-4)\right)$
\end{exemple}

\subsection{Systèmes d'équations différentielles}

On procède de la même façon et on résout le système d'équation en $X$ et
$Y$, où $X$ et $Y$ sont les transformées de Laplace des fonctions $x$ et
$y$ du problème original.

\begin{exemple}
  Résoudre\\
  $\displaystyle{\left\{\begin{array}{l}x'(t)+2y(t) = x(t)\\y'(t)-2x(t)
    = y(t)\end{array}\right.}$
  avec $x(0)=1$ et $y(0)=0$
\end{exemple}

\pagebreak
\subsection{Fonction de transfert}

La fonction $H(p)=\frac{S(p)}{E(p)}$ est appellée fonction de transfert
du système.

\begin{definition}
  Les pôles d'une fraction rationnelles sont les racines de son
  dénominateur.
\end{definition}

\begin{theoreme}
  On dit que le système est stable si et seulement si les pôles de la
  fonction de transferts sont à partie réelle négative.
\end{theoreme}

\end{document}

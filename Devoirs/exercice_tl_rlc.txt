  On désigne par $U$ la fonction échelon unité, définie pour tout nombre
  réel $t$ par :

  \[\left\{\begin{array}{l c l }
  U(t) &=& 0\quad  \text{si}\: t < 0\\
  U(t)&=&1\quad  \text{si}\: t \geqslant 0
  \end{array}\right.\]

  On considère le filtre suivant :

  \begin{center}
    \begin{tikzpicture}[scale=2,circuit ee IEC,>=latex]
      \draw (0,0) to [inductor={info=$L$}] (1,0) to
      [capacitor={info=$C$}] (2,0) to (3,0) ;
      \draw (2,0) to [resistor={info=$R$}] (2,-1) ;
      \draw (0,-1) -- (4,-1) ;
      \draw [->] (3,0) -- (4,0) node [anchor=south east] { $i_s = 0$ } ;
      \def\a{0.05cm}
      \draw [->] (0cm-\a,-1cm+\a) -- (0cm-\a,0cm-\a) ;
      \draw (0cm-\a,-0.5cm) node [left] { $e(t)$ } ;
      \draw [->] (4cm+\a,-1cm+\a) -- (4cm+\a,0cm-\a) ;
      \draw (4cm+\a,-0.5cm) node [right] { $s(t)$ } ;
    \end{tikzpicture}
  \end{center}

  Les constantes $R, L$ et $C$ sont des réels strictement positifs
  caractéristiques du circuit.

  À l'entrée de ce filtre, on applique une tension modélisée par une
  fonction $e$.

  En sortie, on recueille une tension modélisée par une fonction $s$.

  Les éléments du circuit sont traversés par un même courant et
  l'intensité à la sortie, notée $i_{s}$, est nulle.

  On suppose que les deux fonctions $e$ et $s$ sont nulles pour tout
  nombre réel $t$ strictement négatif et qu'elles admettent des
  transformées de Laplace notées respectivement $E$ et $S$.

  Les fonctions $e$ et $s$ sont telles que, pour tout nombre réel $t$
  strictement positif,

  \[\dfrac{\text{d}s}{\text{d}t} + \dfrac{R}{L}s(t) +
  \dfrac{1}{LC}\displaystyle\int_{0}^t s(u)\text{d}u = \dfrac{R}{L}e(t).
  \qquad (2)\]

  De plus, $s(0) = 0$. On suppose que

  \[R = 20~\Omega,\quad  L = 0,5~\text{H}\quad  \text{et}\quad  C =
  0,001~\text{F}.\]

  \begin{enumerate}
    \item La fonction $e$ est définie, pour tout nombre réel $t$, par

      \[e(t) = U(t).\]

      Déterminer $E(p)$.
    \item En appliquant la transformation de Laplace aux deux membres de
      la relation (2), déterminer une expression de $S(p)$.
    \item Vérifier que

      \[S(p) = \dfrac{40}{(p + 20)^2 + 40^2}.\]


    \item
      \begin{enumerate}
        \item Déterminer l'original  de $\dfrac{40}{p^2 + 40^2}$.
        \item En déduire l'expression de $s(t)$ pour tout nombre réel
          $t$ positif ou nul.
        \item La courbe représentative de la fonction $s$ est tracée sur
          l'annexe \no 1.

          Déterminer graphiquement une valeur approchée à $0,01$~près du
          nombre réel $t_{1}$ à partir duquel la valeur absolue de
          $s(t)$ est strictement inférieure à $0,05$, c'est-à-dire à
          partir duquel, pour tout réel $t > t_{1}$, $|s (t)| \leqslant
          0,05$.
      \end{enumerate}
  \end{enumerate}

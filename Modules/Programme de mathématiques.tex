\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Programme de Mathématiques}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

Pour chaque spécialité de brevet de technicien supérieur, le programme
de mathématiques comporte, d'une part un exposé des objectifs, d'autre
part des modules de programmes choisis dans la liste ci- jointe en
fonction des besoins spécifiques de la section considérée. Ces modules,
qui s’appuient sur les programmes du lycée, sont conçus de façon à
favoriser l'accueil de tous les bacheliers, en particulier des
bacheliers professionnels et technologiques.

\section{Lignes directrices}
\subsection{Objectifs généraux}

L'enseignement des mathématiques doit fournir les outils nécessaires
pour permettre aux élèves de suivre avec profit d’autres enseignements
utilisant des savoir-faire mathématiques.  Il doit aussi contribuer au
développement de la formation scientifique, grâce à l'exploitation de
toute la richesse de la démarche mathématique : mathématisation d'un
problème (modélisation), mise en œuvre d'outils théoriques pour résoudre
ce problème, analyse de la pertinence des résultats obtenus au regard du
problème posé.

Il doit enfin contribuer au développement des capacités personnelles et
relationnelles : acquisition de méthodes de travail, maîtrise des moyens
d'expression écrite et orale ainsi que des méthodes de représentation
(graphiques, schémas, croquis à main levée, organisation de données
statistiques,...), avec ou sans intervention des outils informatiques.
Les moyens de documentation, qui contribuent à un développement des
capacités d'autonomie, sont à faire utiliser (documents écrits réalisés
par les enseignants, livres, revues, tables, formulaires, supports
informatiques de toute nature, Internet,...).

\par
Ces trois objectifs permettent de déterminer pour un technicien
supérieur les capacités et compétences mises en jeu en mathématiques.

\par
On a veillé à articuler l'impératif d'une formation axée sur l'entrée
dans la vie professionnelle et le développement des capacités
d'adaptation à l'évolution scientifique et technique, permettant la
poursuite éventuelle d'études.

\subsection{Objectifs spécifiques à la section}
Pour chaque spécialité, les objectifs spécifiques, qui déterminent les
champs de problèmes qu'un technicien supérieur doit être capable de
résoudre sont précisés par le règlement du BTS considéré.

\subsection{Organisation des contenus}
C'est en fonction de ces objectifs généraux et spécifiques que
l'enseignement des mathématiques est
conçu pour chaque spécialité de brevet de technicien supérieur ; il peut
s'organiser autour :
\begin{itemize}
  \item de quelques pôles significatifs de la spécialité, précisés par
    le règlement du BTS considéré ;
  \item pour l'ensemble du programme, d’une valorisation des aspects
    numériques et graphiques, d’une initiation à quelques méthodes
    élémentaires de l'analyse numérique et de l'utilisation pour tout
    cela des moyens informatiques appropriés (calculatrice, ordinateur).
\end{itemize}

\subsection{Présentation du texte du programme}
Pour chaque spécialité de BTS, le programme est constitué de plusieurs
modules, chacun comportant deux parties : un bandeau et un texte
présenté sous forme d’un tableau en trois colonnes.

Généralement, le bandeau précise les objectifs essentiels du module et
délimite le cadre du texte du tableau.

Dans la première colonne du tableau figurent les contenus : il s’agit de
l'énoncé des notions et résultats de base que '’étudiant doit connaître
et savoir utiliser.

La deuxième colonne est celle des capacités attendues : elle liste ce
que l'étudiant doit savoir faire, sous forme de verbes d’action, de
façon à faciliter l'évaluation ; il peut s’agir d’appliquer des
techniques classiques et bien délimitées, d’exploiter des méthodes
s’appliquant à un champ de problèmes, ou d’utiliser des outils
logiciels.

La troisième colonne contient des commentaires précisant le sens ou les
limites à donner à certaines questions du programme ; pour éviter toute
ambiguïté sur celles-ci, il est indiqué que certains éléments ou
certaines notions sont « hors programme » (ce qui signifie qu'ils n'ont
pas à être abordés au niveau considéré) ou qu'à leur sujet « aucune
difficulté théorique ne sera soulevée ». La mention « admis » signifie
que la démonstration du résultat visé est en dehors des objectifs du
programme. Pour limiter un niveau d’approfondissement, il peut être
indiqué en commentaire, dans la colonne de droite, que « tout excès de
technicité est exclu » ou que des « indications doivent être fournies »
aux étudiants, ou encore qu'il faut se limiter à des « exemples simples
».

Le symbole $\leftrightarrows$ introduit des thèmes d’ouverture
interdisciplinaire où le programme de mathématiques peut interagir avec
les enseignements scientifiques, technologiques ou professionnels. Les
professeurs de mathématiques doivent régulièrement accéder aux
laboratoires afin de favoriser l'établissement de liens forts entre la
formation mathématique et les formations dispensées dans les
enseignements scientifiques et technologiques. Cet accès permet de :
\begin{itemize}
  \item prendre appui sur les situations expérimentales rencontrées dans
    ces enseignements ;
  \item connaître les logiciels utilisés et l'exploitation qui peut en
    être faite pour illustrer les concepts mathématiques ;
  \item prendre en compte les besoins mathématiques des autres
    disciplines.
\end{itemize}

\subsection{Organisation des études}
L'horaire de mathématiques pour chacune des deux années de la formation
considérée est indiqué par le règlement du BTS considéré.

\par
Les étudiants ont acquis dans les classes antérieures un bagage qu'on
aura soin d’exploiter en tenant compte de la diversité des parcours
scolaires. Il importe en particulier de prévoir en début d’année un
accompagnement des bacheliers professionnels de façon à faciliter la
transition vers les études supérieures.

Dans la continuité des programmes du lycée, la résolution de problème
doit être mise au cœur de l'activité mathématique des étudiants.

Le professeur dispose en général de séances de travaux dirigés
nécessaires pour affermir les connaissances des élèves par un
entraînement méthodique et réfléchi à la faveur d'activités de synthèse
disciplinaires et interdisciplinaires.

Réguliers et de nature variée, les travaux hors du temps scolaire
contribuent à la formation des étudiants et sont absolument essentiels à
leur progression. Ils sont conçus de façon à prendre en compte la
diversité et l'hétérogénéité de leurs acquis.

Le cours proprement dit doit être bref. Une part très importante du
temps de travail doit être consacrée à la mise en activité des
étudiants, sous forme de travaux dirigés ou pratiques, mettant en œuvre
les contenus du programme.

Le professeur de mathématiques pourra admettre certains résultats ; il
s'attachera avant tout à faire acquérir aux élèves un noyau de
connaissances solides, en particulier celles qui sont directement
utilisées dans les autres enseignements scientifiques, techniques et
professionnelles, ainsi qu'à développer la capacité à les mobiliser pour
résoudre des problèmes issus de secteurs variés des mathématiques et des
autres disciplines.

\subsection{Place des outils logiciels}

Les outils logiciels fournissent un ensemble de ressources
particulièrement utiles pour l'enseignement des mathématiques en
sections de techniciens supérieurs, où ils peuvent intervenir de façon
très efficace dans la réalisation des objectifs de cet enseignement :
\begin{itemize}
  \item en fournissant rapidement des résultats, dans les domaines du
    calcul (y compris à l'aide d’un logiciel de calcul formel), des
    représentations graphiques et pour les applications à d’autres
    disciplines ;
  \item en contribuant par leur intervention au développement de la
    formation scientifique, à différents moments de la démarche
    mathématique, lors de la résolution de certains problèmes, de la
    reconnaissance de l'adéquation de modèles avec les observations ou
    de la réalisation d’une synthèse sur certains concepts ;
  \item en favorisant le développement des capacités personnelles et
    relationnelles, notamment la maîtrise des moyens d’expression écrite
    et des méthodes de représentation, ainsi que l'autonomie dans la
    recherche documentaire intégrant l'usage d’Internet.
\end{itemize}

Pour l'ensemble des spécialités de brevet de technicien supérieur, le
travail effectué soit à l'aide de la calculatrice programmable à écran
graphique de chaque étudiant, soit sur un ordinateur muni d’un tableur,
de logiciels de calcul formel, de logiciels de géométrie ou de logiciels
d’application (modélisation, simulation,...) permet de centrer
l'activité mathématique sur l'essentiel : identifier un problème,
expérimenter sur des exemples, conjecturer un résultat, bâtir une
argumentation, mettre en forme une démonstration, contrôler les
résultats obtenus et analyser leur pertinence en fonction du problème
posé.

De plus, pour les spécialités où l'informatique joue un rôle
particulièrement important, une approche de quelques modèles
mathématiques intervenant dans la conception et l'utilisation de ces
technologies est de nature à favoriser l'unité de la formation.

\par
Ces apports des outils logiciels doivent s’intégrer dans la mise en
œuvre des textes définissant le programme de mathématiques, en veillant
à distinguer les objectifs de formation et les exigences lors des
évaluations.

\subsection{Articulation avec les épreuves du BTS}
En ce qui concerne les épreuves du BTS, il est précisé que les étudiants
doivent connaître l'énoncé et la portée des résultats figurant au
programme, mais que la démonstration de ces résultats n'est pas
exigible. En outre, pour les rubriques du programme figurant sous la
forme « Exemples de », seule la mise en œuvre des méthodes explicites
dans l'énoncé de l'épreuve est exigible et aucune connaissance
spécifique préalable n'est requise.
\par
L'emploi des calculatrices est défini par la réglementation en vigueur
spécifique aux examens et concours relevant du ministère de l'éducation
nationale. Dans ce cadre, les étudiants doivent savoir utiliser une
calculatrice programmable à écran graphique dans les situations liées au
programme de la spécialité considérée. Cet emploi combine les capacités
suivantes, qui constituent un savoir-faire de base et sont seules
exigibles :
\begin{itemize}
  \item savoir effectuer les opérations arithmétiques sur les nombres et
    savoir comparer des nombres ;
  \item savoir utiliser les touches des fonctions et lois de
    probabilités qui figurent au programme de la spécialité considérée
    et savoir programmer le calcul des valeurs d'une fonction d'une
    variable permis par ces touches ;
  \item savoir afficher à l'écran la courbe représentative d’une
    fonction ;
  \item savoir programmer une séquence, une instruction conditionnelle
    ou itérative comportant éventuellement un test d’arrêt.
\end{itemize}

L'usage des calculatrices, y compris celles possédant un logiciel de
calcul formel, d’autres moyens de calcul (tables numériques,
abaques,...), des instruments de dessin est autorisé aux épreuves de
mathématiques du BTS, dans le cadre de la réglementation en vigueur pour
les examens et concours de l'éducation nationale ; ce point doit être
précisé en tête des sujets.

\section{Programmes}

\textbf{Modules d’analyse}
\begin{itemize}
  \item Suites numériques
  \item Fonctions d’une variable réelle
  \item Fonctions d’une variable réelle et modélisation du signal
  \item Calcul intégral
  \item Équations différentielles
  \item Séries de Fourier
  \item Transformation de Laplace
  \item Transformation en z
\end{itemize}
\textbf{Modules de statistique et probabilités}
\begin{itemize}
  \item Statistique descriptive
  \item Probabilités 1
  \item Probabilités 2
  \item Statistique inférentielle
  \item Fiabilité
  \item Plans d’expérience
\end{itemize}
\textbf{Modules d’algèbre et géométrie}
\begin{itemize}
  \item Configurations géométriques
  \item Calcul vectoriel
  \item Représentations de l'espace
  \item Modélisation géométrique
  \item Nombres complexes
  \item Calcul matriciel
  \item Arithmétique
  \item Algèbres de Boole
  \item Éléments de la théorie des ensembles
  \item Graphes et ordonnancement
  \item Algorithmique appliquée
\end{itemize}


\end{document}

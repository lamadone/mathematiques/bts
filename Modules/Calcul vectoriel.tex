\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Calcul vectoriel}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

Le but de ce module est double :
\begin{itemize}
  \item consolider les acquis de calcul vectoriel des années précédentes
    en tenant compte des connaissances acquises antérieurement ou non
    par les étudiants ;
  \item apporter des compléments de calcul vectoriel, qui peuvent être
    utiles pour étudier des situations rencontrées dans les autres
    enseignements.
\end{itemize}

On prend appui sur les enseignements scientifiques et technologiques qui
fournissent un large éventail de problèmes. On utilise les possibilités
offertes par les logiciels de géométrie dynamique. Il est également
pertinent de connaître les logiciels qui sont utilisés par les
disciplines technologiques et l'exploitation qui peut en être faite en
lien avec le cours de mathématiques.

\begin{longtable}{*{3}{|p{0.3\linewidth}}|}
  \hline
  \centering\textbf{CONTENUS} & \centering\textbf{CAPACITÉS ATTENDUES} &
  \hfill\textbf{COMMENTAIRES}\hfill~ \\ \hline
  \textbf{Décomposition d'un vecteur dans une base du plan ou de
  l'espace} & & \\
   & \textbullet Décomposer un vecteur dans une base et exploiter une
  telle décomposition. & On ne se limite pas au cas de la géométrie
  repérée. \\
   & & $\leftrightarrows$ Vecteur vitesse, force. \\ \hline
  \textbf{Barycentre} & & \\[1ex]
  Barycentre de deux points pondérés du plan ou de l'espace. Coordonnées
  dans un repère. & \textbullet Construire le barycentre de deux points
  pondérés. & On peut introduire la notion de barycentre en la reliant à
  l'équilibrage des masses ou à la moyenne pondérée.

  Selon les besoins, on étudie des réductions d'une somme de la forme
  $\alpha\overrightarrow{MA} + \beta\overrightarrow{MB}$ avec $\alpha +
  \beta \neq 0$.

  On fait remarquer que la barycentre de deux points distincts
  appartient à la droite définie par ces deux points. \\[1ex]

  Extension de la notion de barycentre à trois points pondérés. &
  \textbullet Utiliser, sur des exemples simples liés aux enseignements
  technologiques, la notion de barycentre partiel. & Sur des exemples
  issus des enseignements technologiques, on met en place le théorème du
  barycentre partiel. \\[1ex]
   & & $\leftrightarrows$ Centre d'inertie d'un assemblage de solides.
  \\ \hline
  \textbf{Produit scalaire} & & \\[1ex]
  Expressions du produit scalaire :
  \begin{itemize}
    \item à l'aide d'une projection orthogonale ;
    \item à l'aide des normes et d'un angle ;
    \item à l'aide des coordonnée.
  \end{itemize} & \textbullet Choisir l'expression du produit scalaire
  la plus adaptée en vue de la résolution d'un problème.

  \vfill
  \textbullet Calculer un angle ou une longueur à l'aide d'un produit
  scalaire. & On exploite des situations issues des domaines
  scientifiques et technologiques.

  On illustre en situation quelques propriétés du produit scalaire.
  \\[1ex]

   & & $\leftrightarrows$ Travail, puissance d'une force. \\[0.7ex]
  \hline

  \textbf{Produit vectoriel} &  &  \\
  Orientation de l'espace    &  &  \\
  Produit vectoriel de deux vecteurs de l'espace :
  \begin{itemize}
    \item définition ;
    \item calcul des coordonnées dans une base orthonormale directe ;
    \item application à l'aire d'un triangle et d'un parallélogramme.
  \end{itemize}
  & \textbullet Calculer une aire à l'aide d'un produit vectoriel. & La
  découverte du produit vectoriel, de ses propriétés et de ses
  applications est à mener en liaison étroite avec les autres
  enseignements.

  Les notions de vecteur glissant, de torseur et le produit mixte sont
  hors programme. \\[1ex]
   & & $\leftrightarrows$ Moment d'une force \\[0.7ex] \hline
\end{longtable}

\end{document}

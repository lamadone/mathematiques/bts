#!/bin/bash - 
#===============================================================================
#
#          FILE: build_programmes.sh
# 
#         USAGE: ./build_programmes.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Vincent-Xavier (), 
#  ORGANIZATION: 
#       CREATED: 18/04/2015 18:31
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#rubber -d *.tex
#rubber --clean *.tex
latexmk --pdf *.tex
pdfjoin -o Programme\ complet\ maintenance\ industrielle.pdf Programme\
  de\ mathématiques.pdf Les\ capacités\ et\ compétences.pdf Répartition\
  modules\ maintenance\ industrielle.pdf Fonctions\ d\'une\ variable\
  réelle.pdf Calcul\ intégral.pdf Équations\ différentielles.pdf \
Statistique\ descriptive.pdf Probabilités\ 1.pdf Probabilités\ 2.pdf \
Statistique\ inférentielle.pdf Calcul\ matriciel.pdf

pdfjoin -o Programme\ complet\ électrotechnique.pdf Programme\
    de\ mathématiques.pdf Les\ capacités\ et\ compétences.pdf \
  Répartition\ modules\ électrotechnique.pdf Suites\ numériques.pdf
  Fonctions\ d\'une\ variable\ réelle.pdf Fonctions\ d\'une\ variable\
    réelle\ et\ modélisation\ du\ signal.pdf Calcul\ intégral.pdf \
  Équations\ différentielles.pdf Série\ de\ Fourier.pdf Transformation\
    de\ Laplace.pdf Probabilités\ 1.pdf Calcul\ vectoriel.pdf Nombres\
    complexes.pdf


\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage{pdfpages}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Électrotechnique}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

\section{Lignes directrices}
\emph{Objectifs spécifiques à la section}

\emph{L'étude des conversions d'énergie} (énergie électrique, énergie
mécanique) constitue un des objectifs essentiels de la formation des
techniciens supérieurs en électrotechnique, ainsi que l'étude des
signaux, qui porte à la fois sur des problèmes de description (analyse
et synthèse), d'évolution et de commande.  Selon que l'on s'intéresse
aux aspects continus ou discrets, l'état des systèmes automatisés est
décrit mathématiquement par des fonctions ou des suites, qu'il s'agit
alors de représenter de façon pertinente à l'aide de codages, de
méthodes géométriques, ou de transformations permettant d'étudier la
dualité entre les valeurs prises aux différents instants et la
répartition du spectre. En outre, certains problèmes doivent être placés
dans un contexte aléatoire. Enfin, il est largement fait appel aux
ressources de l'informatique.

\emph{Organisation des contenus}
C'est en fonction de ces objectifs que l'enseignement des mathématiques
est conçu ; il peut s'organiser autour de quatre pôles :
\begin{itemize}
  \item une étude des fonctions, mettant en valeur l'interprétation des
    opérations en termes de signaux (sommes, produits, dérivation,
    intégration, translation du temps, changement d'échelle...) et les
    relations avec l'étude des suites. La maîtrise des fonctions
    usuelles s'insère dans ce contexte et on a fait place aussi bien aux
    fonctions exponentielles réelles ou complexes qu'aux fonctions
    représentant des signaux moins réguliers : échelon unité, créneaux,
    dents de scie. De même, il convient de viser une bonne maîtrise des
    nombres complexes et des fonctions à valeurs complexes, notamment
    par l'emploi de représentations géométriques appropriées.
  \item l'analyse et la synthèse spectrale des fonctions périodiques
    (séries de Fourier) ou non périodiques (transformation de Laplace),
    occupent une place importante : pour des raisons de progression et
    de niveau, d'autres questions n'ont pu être introduites, malgré leur
    utilité pour la formation considérée : c'est le cas pour la
    transformation de Fourier, la convolution et le calcul opérationnel.
    En revanche, on a voulu marquer l'importance des équations
    différentielles, en relation avec les problèmes d'évolution et de
    commande ;
  \item une initiation au calcul matriciel ;
  \item une initiation au calcul des probabilités, centrée sur la
    description des lois fondamentales, permet de saisir l'importance
    des phénomènes aléatoires dans les sciences et techniques
    industrielles ;
  \item une valorisation des aspects numériques et graphiques pour
    l'ensemble du programme, une initiation à quelques méthodes
    élémentaires de l'analyse numérique et l'utilisation à cet effet des
    moyens informatiques appropriés : calculatrice programmable à écran
    graphique, ordinateur muni d’un tableur, de logiciels de calcul
    formel, de géométrie ou d’application (modélisation, simulation,…).
    On initiera les étudiants à la recherche et à la mise en forme des
    algorithmes signalés dans le programme mais aucune connaissance
    théorique sur ces algorithmes n'est exigible des élèves.
\end{itemize}

On notera à ce propos que les notions sur les systèmes de numération,
sur les codages et sur les opérations logiques nécessaires à
l'enseignement de l'électronique de commande sont intégrées à cet
enseignement et ne figurent pas au programme de mathématiques. Les
professeurs se concerteront de manière à assurer une bonne progression
pour les élèves dans ces domaines.

\emph{Organisation des études}
L'horaire est de 3 heures + 1 heure en première année et de 2 heures + 1
heure en seconde année.
\section{Programme}
Le programme de mathématiques est constitué des modules suivants :
\begin{itemize}
  \item Suites numériques.
  \item Fonctions d’une variable réelle.
  \item Fonctions d’une variable réelle et modélisation du signal.
  \item Calcul intégral.
  \item Équations différentielles.
  \item Séries de Fourier.
  \item Transformation de Laplace.
  \item Probabilités 1.
  \item Calcul vectoriel, à l'exception des paragraphes « Barycentre »
    et « Produit vectoriel ».
  \item Nombres complexes.
\end{itemize}

\end{document}

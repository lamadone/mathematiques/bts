\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Séries de Fourier}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

Le but de ce module est d’étudier et exploiter la décomposition de
signaux périodiques en séries de Fourier. Ce module est à mener en
liaison étroite avec les enseignements des autres disciplines : les
séries de Fourier sont un outil indispensable pour l'étude des
phénomènes vibratoires en électricité, en optique et en mécanique.


\begin{longtable}{*{3}{|p{0.3\linewidth}}|}
  \hline
  \centering\textbf{CONTENUS} & \centering\textbf{CAPACITÉS ATTENDUES} &
  \textbf{COMMENTAIRES} \\ \hline
  \textbf{Exemples de séries numériques} & & \\[1ex]
  Séries géométriques : convergence, somme. \par\vspace{1ex} Séries de
  Riemann : convergence. & \textbullet~Reconnaître une série
  géométrique et connaître la condition de convergence. \par\vspace{1ex}
  Connaître la condition de convergence d'une série de Riemann. &
  L'étude de ces deux exemples a pour objectifs :
  \begin{itemize}
    \item de familiariser les étudiants avec les «sommes infinies» et la
      notation $\sum$ ;
    \item d'introduire la notion de convergence et de somme d'une série
      numérique.
  \end{itemize}
  Toute théorie générale sur les série est exclue. L'outil informatique
  est utilisé pour conjecturer les résultats concernant les séries de
  Riemann. Ces résultats sont admis.\\ \hline
  \textbf{Séries de Fourier} & & \\
  Série de Fourier associée à une fonction $T$-périodique continue par
  morceaux sur $\R$ : $a_0+\sum_{n> 1}(a_n\cos(n\omega t) +
  b_n\sin(n\omega t))$. & \textbullet~Représenter graphiquement une
  fonction $T$-périodique continue par morceaux sur $\R$. & En liaison
  avec les autres disciplines, on met en valeur le lien entre la notion
  de série de Fourier et l'étude des signaux : composantes d'un signal
  dans une fréquence donnée, reconstitution du signal à partir de ses
  composantes, spectre. \\
  & \textbullet~Exploiter la représentation graphique d'une fonction
  $T$-périodique affine par morceaux pour en déterminer :
  \begin{itemize}
    \item la périodicité ;
    \item la parité ;
    \item une expression sur une période ou une demi-période.
  \end{itemize}
  & On montre l'intérêt d'exploiter, dans le calcul intégral, les
  propriétés des fonctions périodiques, des fonctions paires et des
  fonctions impaires. \\
  & \textbullet~Calculer les coefficients de Fourier d'une fonction :
  \begin{itemize}
    \item à la main dans le cas d'un signal en créneau ;
    \item à l'aide d'un logiciel de calcul formel dans tous les cas.
  \end{itemize}
  & En complément, on traite à la main un exemple de calculs de
  coefficients de Fourier d'une fonction associée à un signal rampe pour
  faire comprendre les résultats fournis par les logiciels dans d'autres
  disciplines. C'est l'occasion de réinvestir les techniques de calcul
  intégral. \\
  Cas d'une fonction paire, impaire. & & En liaison avec les méthodes
  vues dans les autres disciplines, on montre qu'il peut être utile de
  se ramener à des fonctions paires et impaires. \\ \hline
  Convergence d'une série de Fourier lorsque $f$ est de classe $C^1$ par
  morceaux sur $\R$ (conditions de Dirichlet). & \textbullet~Représenter
  à l'aide d'un logiciel une somme partielle d'une série de Fourier et
  la comparer à la fonction associée au signal
  étudié.\par\vspace{1ex}\textbullet~Savoir identifier parmi plusieurs
  développements proposés celui correspondant à une fonction donnée. &
  L'utilisation de l'outil informatique permet de visualiser
  graphiquement la Convergence de la série de Fourier.\par\vspace{1ex}
  Aucune difficulté ne doit être soulevée sur la convergence des séries
  de Fourier. Dans les cas étudiés, les conditions de convergence sont
  toujours remplies. \\
  Formule de Parseval & \textbullet~Calculer et comparer :
  \begin{itemize}
    \item la valeur exacte de $\frac1T\int_0^Tf^2(t)\,\mathrm{d}t$ ;
    \item une valeur approchée de $\frac1T\int_0^Tf^2(t)\,\mathrm{d}t$ à
      l'aide des coefficients de Fourier de $f$.
  \end{itemize}
  & On met en relation la formule de Parseval et le calcul de la valeur
  efficace d'un signal. \\
  & & $\leftrightarrows$ Analyse harmonique d'un signal. \\ \hline
\end{longtable}

\end{document}

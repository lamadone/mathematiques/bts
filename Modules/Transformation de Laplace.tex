\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Transformation de Laplace}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

Dans ce module, on étudie et on exploite la transformation de Laplace en
vue de déterminer les solutions causales d’une équation différentielle
linéaire. Cette présentation est à mener en liaison étroite avec les
enseignements des autres disciplines où la transformation de Laplace
permet d’obtenir la réponse d’un système linéaire usuel à un signal
d’entrée donné.

\newcommand{\U}{\mathscr{U}}
\newcommand{\Ut}{\U(t)}

\begin{longtable}{*{3}{|p{0.3\linewidth}}|}
  \hline
  \centering\textbf{CONTENUS} & \centering\textbf{CAPACITÉS ATTENDUES} &
  \textbf{COMMENTAIRES} \\ \hline
  \textbf{Transformation de Laplace} & & \\[1ex]
  Transformée de Laplace d'une fonction causale $f$ & & La théorie
  générales des intégrales impropres est hors programme. \\[1ex]
  Transformée de Laplace des fonctions causales usuelles. & & On se
  limite aux fonctions usuelles suivantes :

  $t\mapsto \Ut$ ;

  $t\mapsto t^n\Ut$ ;

  $t\mapsto e^{at}\Ut$ ;

  $t\mapsto \sin(\omega t)\Ut$ et $t\mapsto \cos(\omega t)\Ut$ avec $\U$
  la fonction unité, $n$ un entier naturel et $a$ et $\omega$ deux
  réels. \\[1ex]

  Propriétés de la transformation
  de Laplace :
  \begin{itemize}
    \item linéarité ;
    \item effet d’une translation ou d’un changement d’échelle sur la
      variable ;
    \item effet de la multiplication par $e^{-at}$.
  \end{itemize}
  &
  \textbullet Représenter graphiquement une fonction causale donnée
  par une expression.

  \textbullet Déterminer une expression d’une fonction causale dont
  la représentation graphique est de type « créneau » ou « rampe ».

  \textbullet Déterminer la transformée de Laplace d’une fonction
  causale simple, dont les fonctions de type « créneau » et « rampe
  ».

  \textbullet Déterminer la fonction causale (original) dont la
  transformée de Laplace est donnée.

  & On se limite au cas où les fonctions
  données ou recherchées sont :
  \begin{itemize}
    \item soit des combinaisons linéaires à coefficients réels de
      fonctions de la forme $t \mapsto \U(t - \alpha)$ et$t \mapsto
      t\U(t - \alpha)$ ;
    \item soit de la forme $t \mapsto \U(t - \alpha)e^{rt}$ ;
  \end{itemize}
  où $\alpha$ est un nombre réel positif et $r$ un réel.

  Dans les autres cas, le calcul est facilité par l'utilisation d’un
  logiciel.

  \\[1ex]

  Théorème de la valeur initiale et théorème de la valeur finale. & &
  L'exploitation de situations issues des autres disciplines permet
  d’illustrer la pertinence de ce théorème. \\[1ex] \hline

  Transformée de Laplace d'une dérivée. & \textbullet Exploiter la
  transformée de Laplace pour résoudre une équation différentielle
  linéaire d'ordre 1 ou 2 à coefficients constants.
  & On se limite au cas où les fonctions
  données ou recherchées sont :
  \begin{itemize}
    \item soit des combinaisons linéaires à coefficients réels de
      fonctions de la forme $t \mapsto \U(t - \alpha)$ et$t \mapsto
      t\U(t - \alpha)$ ;
    \item soit de la forme $t \mapsto \U(t - \alpha)e^{rt}$ ;
  \end{itemize}
  où $\alpha$ est un nombre réel positif et $r$ un réel.

  $\leftrightarrows$ Fonction de transfert d'un système linéaire.
  Application à la stabilité. \\[1ex] \hline
  Transformée de Laplace d'une primitive. & & En liaison avec les
  enseignements d'autres disciplines, on étudie un exemple d'équation
  différentielle de la forme $ay'(t) + by(t) +c \int_0^ty(s)\mathrm{d}s
  = f(t)$ où $a$, $b$ et $c$ sont des constantes et $f$ une fonction
  causale. \\[1ex] \hline
\end{longtable}

\end{document}

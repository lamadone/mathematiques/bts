\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Fonctions d'une variable réelle}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

On se place dans le cadre des fonctions à valeurs réelles, définies sur
un intervalle ou une réunion d’intervalles de $\R$, qui servent à
modéliser des phénomènes continus. Les étudiants doivent savoir traiter
les situations issues des disciplines techniques et scientifiques qui se
prêtent à une telle modélisation. Pour aider les étudiants à faire le
lien avec ces autres disciplines, il est indispensable d’employer
régulièrement des notations variées sur les fonctions et de diversifier
les modes de présentation d’une fonction : fonction donnée par une
courbe, par un tableau de valeurs ou définie par une formule et un
ensemble de définition.


Le but de ce module est double :
\begin{itemize}
  \item consolider les acquis sur les fonctions en tenant compte,
    notamment sur les limites, des programmes de mathématiques suivis
    antérieurement par les étudiants ;
  \item apporter des compléments sur les fonctions d’une variable
    réelle, qui peuvent être utiles pour aborder de nouveaux concepts.
\end{itemize}
Tout particulièrement dans ce module, on utilise largement les moyens
informatiques (calculatrice, ordinateur), qui permettent notamment de
faciliter la compréhension d’un concept en l'illustrant graphiquement et
numériquement, sans être limité par d’éventuelles difficultés
techniques.


\begin{longtable}{*{3}{|p{0.3\linewidth}}|}
  \hline
  \centering\textbf{CONTENUS} & \centering\textbf{CAPACITÉS ATTENDUES} &
  \textbf{COMMENTAIRES} \\ \hline
  \textbf{Fonctions de référence} & & \\[1ex]
  Fonctions affines.\par Fonctions polynômes de degré 2.\par Fonction
  logarithme népérien et exponentielle de base $e$.\par Fonction racine
  carrée.\par Fonctions sinus et cosinus. & \textbullet~Représenter une
  fonction de référence et exploiter cette courbe pour retrouver les
  propriétés de la fonction. & En fonction des besoins, on met l'accent
  sur les fonctions de référence les plus utiles.\vspace{1ex}\par En cas
  de besoin lié à la spécialité, on peut être amené à étudier l'une ou
  l'autre des fonctions suivantes :\begin{itemize}
    \item la fonction logarithme décimal ;
    \item des cas particuliers des fonctions puissances $t\mapsto
      t^\alpha$, avec $\alpha\in\R$ ou exponentielles de base $a$ avec
      $a\in\mathopen{]}0,+\infty\mathclose{[}$. 
   \end{itemize}\\ \hline
  \textbf{Dérivation} & & On privilégie des exemples de fonctions issues
   de problématiques abordées dans les autres disciplines. \\[1ex]
  Dérivée des fonctions de référence.\par\vspace{1ex} Dérivée d'une somme,
  d'un produit et d'un quotient. & \textbullet~Calculer la dérivée
  d'une fonction : \begin{itemize}
    \item à la main dans les cas simples ;
    \item à l'aide d'un logiciel de calcul formel dans tous les cas.
  \end{itemize} & Il s'agit de compléter et d'approfondir les
  connaissances antérieures sur la dérivation. En particulier, il
  est important de rappeler et de travailler l'interprétation
  graphique du nombre dérivé. \\[1ex]
  Dérivée de fonctions de la forme $x\mapsto u^n(x)$ avec $n$ entier
  naturel non nul, $x\mapsto \ln(u(x))$ et $x\mapsto e^{u(x)}$. &
  \textbullet~Étudier les variations d'une fonction simple. & \\\hline
  & \textbullet~Exploiter le tableau de variation d'une fonction $f$
  pour obtenir :
  \begin{itemize}
    \item un éventuel extremum de $f$ ;
    \item le signe de $f$ ;
    \item le nombre de solution d'une équation du type $f(x)=k$.
  \end{itemize}
  & Les solutions d'une équation du type $f(x)=k$ sont déterminées :
  \begin{itemize}
    \item explicitement dans les cas simples ;
    \item de façon approchée sinon.
  \end{itemize} \\
  & \textbullet~Mettre en œuvre un procédé de recherche d'une valeur
  approchée d'une racine. & On étudie alors, sur des exemples, des
  méthodes classiques d'obtention de ces solutions : balayage,
  dichotomie, méthode de Newton par exemple. C'est notamment l'occasion
  de développer au moins un algorithme et d'utiliser des logiciels.\\
  \hline
  \textbf{Limites de fonctions} & & \\[1ex]
  Asymptotes parallèles aux axes :
  \begin{itemize}
    \item limite finie d'une fonction à l'infinie ;
    \item limite infinie d'une fonction en un point.
  \end{itemize}
  & \textbullet~Interpréter une représentation graphique en terme de
  limite. \vspace{1ex}\par\textbullet~Interpréter graphiquement une
  limite en terme d'asymptote. & La diversité des programmes du lycée
  doit particulièrement inciter à veiller aux connaissances sur les
  limites acquises antérieurement ou non par les étudiants.\\
  & & \\
  Limite infinie d'une fonction à l'infini. Cas d'une asymptote oblique.
  & & Toute étude de branche infinie, notamment la mise en évidence
  d'asymptote, doit comporter des indications sur la méthode à suivre.
  \\
  Limites et opérations. & \textbullet~Déterminer la limite d'une
  fonction simple.\vspace{1ex}\par\textbullet~Déterminer des limites
  pour des fonctions de la forme :
  \begin{itemize}
    \item[] $x\mapsto u^n(x)$, $n$ entier naturel non nul ;
    \item[] $x\mapsto \ln(u(x))$ ;
    \item[] $x\mapsto e^{u(x)}$.
  \end{itemize}
  & On se limite aux fonctions déduites des fonctions de référence par
  addition, multiplication ou passage à l'inverse et on évite tout excès
  de technicité. \\ \hline
  \textbf{Approximation locale d'une fonction} & & \\[1ex]
  Développement limité en 0 d'une fonction. & \textbullet~Déterminer, à
  l'aide d'un logiciel, le Développement limité en 0 et à un ordre donné
  d'une fonction. & On introduit graphiquement la notion de
  Développement limité en 0 d'une fonction $f$ en s'appuyant sur
  l'exemple de la fonction exponentielle sans soulever de difficultés
  théoriques.\\
  Développement limité en 0 et tangente à la courbe représentative d'une
  fonction. & \textbullet~Exploiter un développement limité pour donner
  l'équation de la tangente et préciser sa position par rapport à la
  courbe représentative de la fonction. & L'utilisation et
  l'interprétation des développements limités trouvés doivent être
  privilégiées.\\ \hline
  \textbf{Courbes paramétrées} & & \\[1ex]
  Exemples de courbes paramétrées définies par des fonctions
  polynomiales. & \textbullet~Déterminer un vecteur directeur de la
  tangente en un point où le vecteur dérivé n'est pas
  nul.\vspace{1ex}\par\textbullet~Tracer une courbe à partir des
  variations conjointes. & L'étude des ces quelques exemples a pour
  objectif de familiariser les étudiants avec le rôle du paramètre, la
  notion de courbe paramétrée et de variations
  conjointes.\vspace{1ex}\par On se limite à quelques exemples où les
  fonctions polynômes sont de degré inférieur ou égal à
  deux.\vspace{1ex}\par$\leftrightarrows$ Trajectoire d'un solide, design \\
  \hline
\end{longtable}

\end{document}

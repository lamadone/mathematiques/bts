\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Les capacités et compétences}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

Pour être capable de résoudre des problèmes, il est indispensable de
connaître les définitions et les énoncés des théorèmes figurant au
programme. De plus, certaines démonstrations, rencontrées en cours ou en
exercice, gagnent à être mémorisées si elles ont valeur de modèle.

Disposer de connaissances solides dans un nombre limité de domaines
mathématiques est une nécessité pour un technicien supérieur, sans
cependant constituer ni un but en soi ni un préalable à toute activité
mathématique pendant la formation.

\par
Comme il est indiqué dans les « Lignes Directrices » du Programme de
Mathématiques, l'enseignement des mathématiques dans les sections de
technicien supérieur doit fournir les outils nécessaires pour suivre
avec profit d'autres enseignements, et doit contribuer au développement
de la formation scientifique et des capacités personnelles et
relationnelles des étudiants.

L'enseignement des mathématiques ne se limite donc pas à la seule
présentation d'un savoir spécifique, mais doit participer à
l'acquisition de capacités et de compétences plus générales.  La
formation mathématique des étudiants de STS vise essentiellement le
développement des six compétences suivantes :
\begin{itemize}
  \item s’informer ;
  \item chercher ;
  \item modéliser ;
  \item raisonner, argumenter ;
  \item calculer, illustrer, mettre en œuvre une stratégie ;
  \item communiquer.
\end{itemize}

\section{S'informer}
Dans sa vie professionnelle un technicien supérieur est amené à utiliser
très fréquemment diverses sources d'information : il s'agit, face à un
problème donné et d’une documentation, d’extraire un maximum de
renseignements pertinents.
L'enseignement des mathématiques où, en plus de la mémoire, les sources
d'information sont très variées (documents réalisés par les enseignants,
livres, revues, formulaires, supports informatiques de toute nature,
Internet,...), doit contribuer à un tel apprentissage.

\section{Chercher}
Face à un problème, il convient d'abord de se poser plusieurs questions
:

Quelles sont les données? Que cherche-t-on ? Quelle stratégie peut-on
espérer mettre en œuvre pour aborder la résolution du problème ?

À partir des réponses à ces questions, trouver ne signifie pas
nécessairement inventer mais souvent repérer dans sa documentation
écrite, se remémorer, identifier des analogies avec un autre problème
mais aussi expérimenter sur des exemples, tester, formuler des
hypothèses.

Une stratégie est considérée comme adaptée à un problème donné lorsque,
compte tenu des connaissances mathématiques figurant au programme de la
spécialité, elle permet d'en aborder la résolution avec de bonnes
chances de réussites ; ainsi « une » stratégie n'est pas synonyme de «
la meilleure » stratégie.

\section{Modéliser}
La modélisation est ici à prendre au sens de représentation. Un
technicien supérieur est amené à représenter toutes sortes de situations
ou d’objets du monde réel, de traduire un problème donné en langage
mathématique pour identifier les éléments mathématiques qui s’y
rapportent. Il doit ensuite utiliser les outils mathématiques pour le
traiter (suite, fonction, graphe, configuration géométrique, outil
statistique, simulation informatique...). Le résultat de cette étude
mathématique fournira des informations sur la situation réelle si le
modèle, c'est-à-dire la représentation, a été bien choisie.

\section{Raisonner, argumenter}
C’est le cœur de toute activité mathématique. Il s’agit là d’effectuer
des inférences (inductives et déductives), de conduire une
démonstration. Le technicien supérieur doit pouvoir donner les
justifications nécessaires à chaque étape de son raisonnement
(utilisation d'une définition, d’un théorème, d'une hypothèse de
l'énoncé, d’une propriété caractéristique,...).

\section{Calculer, illustrer, mettre en œuvre une stratégie}
La capacité à mener efficacement un calcul simple, à manipuler des
expressions contenant des symboles fait partie des compétences attendues
des étudiants de STS. Les situations dont la gestion manuelle ne
relèverait que de la technicité seront traitées à l'aide d’outils
informatiques.
Les capacités mathématiques exigibles des élèves sont précisées dans la
colonne « capacités attendues » ; tout autre capacité fait l'objet
d'indications précises dans l'énoncé.
Par ailleurs, tout technicien doit analyser la pertinence d’un résultat
obtenu : cela consiste à s'assurer de sa vraisemblance et de sa
cohérence avec les données de l'énoncé et les résultats antérieurs
(graphiques, numériques,...), y compris dans un contexte
non-exclusivement mathématique où les indications nécessaires sont
données ; cela signifie aussi faire preuve de discernement dans
l'utilisation de l'outil informatique, d’esprit critique face à la
démarche effectuée et aux résultats obtenus.

\section{Communiquer}
Dans l'ensemble des enseignements, y compris en mathématiques, cette
capacité conditionne la réussite à tous les niveaux ; on ne peut pas
apprécier la justesse d'un raisonnement, la nature d'une erreur ou d'un
point de blocage d'un étudiant si celui-ci s'exprime d'une manière trop
approximative.
Dans la communication interviennent la clarté d'exposition, la qualité
de la rédaction, les qualités de soin dans la présentation de tableaux,
figures, représentations graphiques, mais également la qualité de
l'expression en français à l'écrit comme à l'oral.

\section*{En conclusion}
On peut dire qu'en mathématiques les capacités mises en jeu permettent,
face à un problème donné, de déterminer sa nature, de trouver une
stratégie, de la mettre en œuvre et d’en apprécier les résultats, le
tout dans un langage écrit ou oral adapté à son destinataire. Une telle
description respecte la diversité des démarches intellectuelles et
permet d'étudier sous différents angles une copie d'examen, un exposé,
un dossier..., c'est-à-dire toute production écrite ou orale d'un
travail mathématique.


\end{document}

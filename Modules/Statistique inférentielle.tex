\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Statistique inférentielle}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

La statistique inférentielle permet de développer les compétences des
étudiants sur les méthodes et les raisonnements statistiques permettant
d’induire, à partir de faits observés sur un échantillon, des propriétés
de la population dont il est issu.

Il s’agit d’approfondir, à partir d’exemples, ce que sont les procédures
de décision en univers aléatoire, ainsi que leur pertinence, dans la
continuité des programmes de lycée.  La validité d’une méthode
statistique est liée à l'adéquation entre la réalité et le modèle la
représentant ; aussi les situations artificielles sont à éviter et les
exemples issus de la vie économique et sociale ou du domaine
professionnel sont à privilégier, en liaison avec les enseignements
d’autres disciplines.\\

Dans la continuité des programmes de lycée, on approfondit la prise de
décision en formalisant la notion de test d’hypothèse et en se centrant
sur la notion de risques d’erreur.

\begin{longtable}{*{3}{|p{0.3\linewidth}}|}
  \hline
  \centering\textbf{CONTENUS} & \centering\textbf{CAPACITÉS ATTENDUES} &
  \hfill\textbf{COMMENTAIRES}\hfill~ \\ \hline
  \textbf{Estimation ponctuelle} & & \\
  Estimation ponctuelle d'un paramètre & \textbullet Estimer
  ponctuellement une proportion, une moyenne ou un écart type d'une
  population à l'aide de la calculatrice ou d'un logiciel, à partir d'un
  échantillon. & La simulation d'échantillons permet de sensibiliser au
  choix de l'estimation de l'écart type de la population. \\ \hline
  \textbf{Tests d'hypothèse} & & \\
  Tests bilatéraux et unilatéraux relatifs à :
  \begin{itemize}
    \item une proportion dans le cas d'une loi binomiale puis dans le
      cas d'une loi binomiale approximable par une loi normale ;
    \item une moyenne.
  \end{itemize} & \textbullet Déterminer la région de rejet de
      l'hypothèse nulle et énoncer la règle de décision. & On souligne
      le fait que la décision prise, rejet ou non, dépend des choix
      faits a priori par l'utilisateur : choix de l'hypothèse nulle, du
      type de test et du seuil de signification. Ces choix sont fournis
      à l'étudiant dans les cas délicats. \\
  Tests bilatéraux et unilatéraux de comparaison de deux proportions ou
  de deux moyennes dans le cadre de la loi normale. & \textbullet
  Utiliser les tests bilatéraux et unilatéraux relatifs à une proportion
  ou à une moyenne ainsi qu'à la comparaison de deux proportions ou de
  deux moyennes. & \\
  Risques d'erreur de première et de seconde espèce. & \textbullet
  Analyse les risques d'erreur de première et de seconde espèce associés
  à la prise de décision. & On compare, à l'aide d'un algorithme ou de
  simulations, les différents seuils de significations et on met en
  évidence les risques d'erreur de première et seconde espèce.\par La
  notion de puissance d'un test est abordée. \\
  & & En liaison avec les enseignements des disciplines professionnelles
  ou les situations rencontrées en entreprise, on peut traiter quelques
  exemples d'autres procédures, par exemple le test du khi deux ou test
  de Student. \\
  & & $\leftrightarrows$ Maîtrise statistique des procédés. \\ \hline
  \textbf{Estimation par intervalle de confiance} & & \\
  Intervalle de confiance d'une proportion ou d'une moyenne. &
  \textbullet Déterminer un intervalle de confiance à un niveaux de
  confiance souhaité pour :\begin{itemize}
    \item une proportion, dans le cas d'une loi binomiale approximable
      par une loi normale ;
    \item une moyenne, dans le cas d'une loi normale quand l'écart type
      est connu ou dans le cas des grands échantillons. 
  \end{itemize} & On distingue confiance et probabilité :\begin{itemize}
    \item avant le tirage d'un échantillon, la procédure d'obtention de
      l'intervalle de confiance a une probabilité de \np{0.95} ou de
      \np{0.99} que cette intervalle contienne le paramètre inconnu ;
    \item après le tirage, le paramètre est dans l'intervalle de calculé
      avec une confiance de \np[\%]{95} ou \np[\%]{99}.
  \end{itemize} \\
  & \textbullet Exploiter un intervalle de confiance. & La simulation
  permet de mieux comprendre la notion d'intervalle de confiance. \\
  & \textbullet Déterminer la taille nécessaire d'un échantillon pour
  estimer une proportion ou une moyenne avec une précision donnée. & \\
  & & $\leftrightarrows$ Incertitude de mesure. \\ \hline

\end{longtable}

\end{document}

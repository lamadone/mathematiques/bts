\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}
\usepackage{multicol}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=*}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newcommand{\ensemble}[1]{\mathbf{#1}}
\newcommand{\R}{\ensemble{R}}

\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Équations différentielles}
\author{}
\date{}

\begin{document}

\vspace{1cm}
\maketitle
\vspace{1cm}

On s'attache à relier les exemples étudiés avec les enseignements
scientifiques et technologiques, en montrant l'importance de l'étude de
phénomènes continus définis par une loi d'évolution et une condition
initiale.\\
L'utilisation des outils logiciels est sollicitée ; elle a pour
finalités:
\begin{itemize}
  \item de mettre en évidence, expérimentalement, la signification ou
    l'importance de certains paramètres ou phénomènes ;
  \item de dépasser la seule détermination des solutions d’une équation
    différentielle en donnant la possibilité de visualiser des familles
    de courbes représentatives de ces solutions ;
  \item de permettre, avec l'aide du calcul formel, de donner une
    expression des solutions dans certains cas complexes.
\end{itemize}
Si, dans ce module, on développe plus particulièrement deux types
d’équations différentielles, on est également attentif à donner une
vision plus large de ces notions en présentant des équations
différentielles dont on ne peut donner qu'une solution approchée tout en
faisant saisir des principes généraux comme la notion de famille de
solutions.  On introduit les nombres complexes et les résolutions
d’équations du second degré à coefficients réels pour disposer de
l'équation caractéristique d’une équation différentielle linéaire du
second ordre.

\begin{longtable}{*{3}{|p{0.3\linewidth}}|}
  \hline
  \centering\textbf{CONTENUS} & \centering\textbf{CAPACITÉS ATTENDUES} &
  \textbf{COMMENTAIRES} \\ \hline
                      & & En lien avec les autres disciplines, on
  habitue les étudiants à différentes écritures : variable, fonction,
  notation différentielle.\\
  \textbf{Équations linéaires du premier ordre} & & \\[1ex]
  Équation différentielle $ay'+by = c(t)$ où $a$, $b$ sont des constantes
  réelles et $c$ une fonction continue à valeurs réelles. & 
  \textbullet~Représenter à l'aide d’un logiciel la famille des courbes
  représentatives des solutions d’une équation
  différentielle.\par\vspace{1ex}\textbullet~Résoudre une équation
  différentielle du premier ordre :
  \begin{itemize}
    \item à la main dans les cas simples ;
    \item à l'aide d’un logiciel de calcul formel dans tous les cas.
\end{itemize}\par\vspace{1ex}\textbullet~Déterminer
  la solution vérifiant une condition initiale donnée :
  \begin{itemize}
    \item à la main dans les cas simples ;
    \item à l'aide d’un logiciel de calcul formel dans tous les cas.
  \end{itemize}
  & On présente sur un exemple la résolution approchée d’une équation
  différentielle par la méthode d’Euler.\par\vspace{1ex} Les indications
  permettant d’obtenir une solution particulière sont
  données.\par\vspace{1ex}En liaison avec les autres disciplines, on
  peut étudier des exemples simples de résolution d'équations
  différentielles non linéaires, du premier ordre à variables
  séparables, par exemple en mécanique ou en cinétique chimique, mais ce
  n’est pas un attendu du programme.\par\vspace{1ex}$\leftrightarrows$
  Loi de refroidissement, cinétique chimique. \\ \hline
  \textbf{Nombres complexes} & & \\[1ex]
  Forme algébrique d’un nombre complexe : somme, produit, conjugué. & &
  On se limite à l'écriture algébrique des nombres complexes.\\
  Équation du second degré à coefficients réels. & \textbullet~Résoudre
  une équation du second degré à coefficients réels. & \\ \hline
  \textbf{Équations linéaires du second ordre à coefficients réels
  constants} & & \\[1ex]
  Équation différentielle $ay''+by'+cy = d(t)$ où $a$, $b$ et $c$ sont
  des constantes réelles et $d$ une fonction continue à valeurs réelles.
  & \textbullet~Représenter à l'aide d’un logiciel la famille des
  courbes représentatives des solutions d’une équation différentielle. &
  La fonction $d$ est une fonction polynôme du type :
  \begin{itemize}
    \item[] $t\mapsto e^{\alpha t}$ ;
    \item[] $t\mapsto\cos(\omega t +\varphi)$ ;
    \item[] $t\mapsto\sin(\omega t +\varphi)$.
  \end{itemize} \\
  & \textbullet~Résoudre une équation différentielle du second ordre :
  \begin{itemize}
    \item à la main dans les cas simples ;
    \item à l'aide d’un logiciel de calcul formel dans tous les cas.
  \end{itemize}
  & \\
  & \textbullet~Déterminer la solution vérifiant des conditions initiales
  données :
  \begin{itemize}
    \item à la main dans les cas simples ;
    \item à l'aide d’un logiciel de calcul formel dans tous les cas.
  \end{itemize}
  & Les indications permettant d’obtenir une solution particulière sont
  données.\par\vspace{1ex}$\leftrightarrows$ Résistance des matériaux,
  circuit électronique.\\\hline

\end{longtable}

\end{document}

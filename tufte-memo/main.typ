#import "@preview/tufte-memo:0.1.2": *

#set text(lang: "fr")

#show: template.with(
  title: [Cours de probabilités en BTS],
  shorttitle: [Probabilités],
  subtitle: [Cours de probabilités en BTS],
  authors: (
    (
      name: "Vincent-Xavier Jumel",
      role: "Concepteur",
      affiliation: "Lycée Charles de Foucauld, Paris",
      email: "vincent-xavier.jumel@ac-paris.fr"
    ),
  ),
  document-number: [Version 0.1.2],
  abstract: [Ce cours présente ce qu'il faut savoir pour aborder l'épreuve de mathématiques approfondies en BTS SIO],
  publisher: [Campus Charles de Foucauld],
  distribution: [Lycée Charles de Foucauld],
  toc: false,
  footer-content: ([Cours sous licence CC-by-sa],[]),
  draft: false,
  /* bib: bibliography("references.bib") */
)

= Vocabulaire et définition

== Vocabulaire

- Expérience aléatoire
  Toute expérience dont le résultat n'est pas connu à l'avance
- Événément
  Réalisation d'une exépérience aléatoire
- Événément élémentaire
  Événément le plus simple
  #context[#note[On utilise généralement la notation ensembliste pour les probabilités]]
- Événement certain
  Événement dont on est sur qu'il se réalise
- Événement impossible
  Événément qui ne peut pas se réaliser
- Événément contraire
  Il ne se réalise que lorsque l'événement n'est pas réalisé
  #context[#note[Un événement et son contraire réalisent une partition de l'univers]]
- Univers des possibles
  Tous les événements qui peuvent exister
- 




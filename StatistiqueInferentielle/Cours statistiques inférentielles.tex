\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS MI}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\begin{document}
\maketitle

une information (moyenne, écart type, proportion) sur une population à
partir d'une mesure sur un échantillon. On cherche à faire «le
contraire» de ce que nous avons posé dans le chapitre précédent.

\section{Estimation d'une moyenne}

\subsection{Estimation ponctuelle}

On considère une variable aléatoire $X$ de moyenne $\mu$ et d'écart type
$\sigma$ inconnues. On suppose qu'on a prélevé un échantillon de taille
$n$, sur lequel on a calculé sa moyenne $\mu_e$ et son écart type
$\sigma_e$.

\begin{propriete}
  \begin{itemize}
    \item L'estimation ponctuelle de la moyenne $\hat\mu$ est
      $\hat\mu=\mu_e$
    \item L'estimation ponctuelle de la moyenne $\hat\sigma$ est
      $\hat\sigma=\sqrt{\dfrac{n}{n-1}}\; \sigma_e$
  \end{itemize}
\end{propriete}

Dès que $n$ est grand ($n\geq 30$), le coefficient $\sqrt{\frac{n}{n-1}}$
est proche de 1 et  $\hat\sigma=\sigma_e$.

De façon intuitive, on a le sentiment que cette estimation ne donne
aucun indice de confiance du résultat. On pourrait avoir pris un
échantillon contenant des données très éloignés de la moyenne.

\subsection{Estimation par intervalle}

On va raisonner en deux temps : une phase \textit{a priori} où on
suppose que l'échantillon n'est pas encore prélevé et une phase
\textit{a posteriori} où on suppose que connue la moyenne $\mu_e$ et
l'écart type $\sigma_e$ de l'échantillon.

D'après la théorie de l'échantillonnage, si $\overline{X}$ est une
variable aléatoire correspondant à la moyenne d'un échantillon de taille
$n$, alors celle-ci suit la loi normale $\mathcal{N}(\mu ;
\frac{\sigma}{\sqrt{n}}$.

On va chercher un intervalle qui contient $\mu$ avec une confiance
arbitraire de $95\%$. Cela revient à chercher $r$ tel que \[
P(\overline{X} - R \leq \mu \leq \overline{X} + r) = \np{0.95} \]
L'écriture précédente est équivalente à \[ P(\mu -r \leq \overline{X}
\leq \mu +r) = \np{0.95} \] Cette écriture est encore équivalente à
chercher $r$ tel que \[ \Pi\left( \frac{r\sqrt{n}}{\sigma}\right) =
\np{0.95} \] La lecture inverse de la table de la loi normale
centrée-réduite nous donne
$\boxed{\dfrac{r\sqrt{n}}{\sigma}=\np{1.96}}$.

Cette valeur changera avec le coefficient de confiance que l'on choisit.

Si on note $t$ cette valeur précédent, notre rayon de confiance vaut \[
\boxed{r=t\frac{\sigma}{\sqrt{n}}} \]

On suppose désormais l'échantillon tiré. On peut donc calculer une
représentation de $\overline{X}$, c'est à dire $\mu_e$. Pour cet
échantillon, on peut affirmer que, «à 95\%», la moyenne se situe dans
l'intervalle $\left[\mu_e-r ; \mu_e +r\right]$.

Si on connaît $\sigma$, le calcul de $r$ est direct. Si on ne connaît
pas $\sigma$, on remplace par $\hat\sigma=\sigma_e\sqrt{\frac{n}{n-1}}$.

\begin{exemple}

  Une université comporte 1500 étudiants. On mesure la taille de 20
  d'entre eux, pris au hasard. On trouve $\mu_e = \np[cm]{176}$ et
  $\sigma_e = \np[cm]{6}$.

  \begin{enumerate}
    \item Calculer $\hat\mu$ et $\hat\sigma$.
    \item Retrouver par le calcul pourquoi on peut dire que, avec une
      confiance de 95\%, la taille moyenne des étudiants se situe entre
      \np[cm]{173.3} et \np[cm]{178.7}.
  \end{enumerate}
\end{exemple}

\section{Estimation d'une proportion}

\subsection{Estimation ponctuelle}

Sans rentrer dans les détails, on trouve que $\hat{p} = p_e$ et
$\hat{\sigma_p} = \sqrt{\dfrac{p_e(1-p_e)}{n}}$.

\subsection{Estimation par intervalle}

On procède exactement de la même façon que pour l'estimation d'une
moyenne. Les calculs nous mènent à \[ \boxed{r=t\sigma_p} \]
L'intervalle de confiance est donc \[ \left[ p_e -
    t\sqrt{\frac{p_e(1-p_e)}{n}} ; p_e +
t\sqrt{\frac{p_e(1-p_e)}{n}} \right] \]

\begin{remarque}
  \begin{itemize}
    \item Si $n<30$, on divise par $n-1$.
    \item Si on est pessimiste, on prends $\sigma = \sqrt{\dfrac{1}{4n}}$
      qui correspond au maximum de la parabole d'équation $y=x(x-1)$.
  \end{itemize}
\end{remarque}

\begin{exemple}
  Un candidat effectue un sondage sur 150 personnes. 45 se disent
  prêtes à voter pour lui.

  Montrer qu'au seuil de confiance de 80\%, que le pourcentage
  d'électeurs qui votera pour lui sera compris entre $\np[\%]{25.3}$ et
  $\np[\%]{34.7}$.
\end{exemple}

\end{document}

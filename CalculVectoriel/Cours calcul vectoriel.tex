\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,top=0.7cm,bottom=0.7cm,left=1.4cm,right=1.4cm,includefoot,includehead]{geometry}

\usepackage{multicol}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{amsmath}
\usepackage{ntheorem}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{tikz}

\title{Calcul vectoriel}
\author{BTS}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\@title},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={\@author}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}

\makeatother

\begin{document}

\maketitle

Ce cours ne se veut pas un cours exhaustif sur le calcul vectoriel ni
sur les vecteurs. Il vise à donner des bases sur ces notions sans
rentrer dans les détails en tentant de ne pas sacrifier des propos plus
généraux lorsque c'est possible.

\section{Généralités sur le calcul vectoriel}

\begin{definition}
  Un vecteur est un élément d'un espace structuré dit «espace vectoriel»
  construit sur un corps de scalaires.
\end{definition}

Autrement dit, on peut additionner deux vecteurs pour construire un
nouveau vecteur, on peut soustraire deux vecteurs pour obtenir un
nouveau vecteur ou encore on peut multiplier un vecteur par un nombre.

On peut également noter les éléments «mécaniques» d'un vecteur :
\begin{itemize}
  \item une direction (une droite) ;
  \item un sens ;
  \item une norme (la valeur de la force) ;
  \item un point d'application.
\end{itemize}

Deux vecteurs sont égaux si et seulement s'ils ont même direction, même
sens et même norme.

\begin{minipage}{0.45\linewidth}
  \begin{center}
    \begin{tikzpicture}
      \draw (-2,1) -- (4,5) ;
      \draw [thick,blue,->] (-0.5,2) node[below] {$A$} -- (2.5,4)
      node[below] {$B$} ;
    \end{tikzpicture}
  \end{center}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
  Ici, on note $\vec{u}=\overrightarrow{AB}$, le vecteur dirigé par la
  droite $(AB)$, de longueur (norme) $AB$ et de sens de $A$ vers $B$.

  Le vecteur $\overrightarrow{AB}$ «envoie» $A$ sur $B$.

  On note la norme  $\lVert\overrightarrow{AB}\rVert =
  \lVert\vec{u}\rVert$ et on a $\lVert\lambda\vec{u}\rVert =
  \lvert\lambda\rvert\lVert\vec{u}\rVert$.

  Le vecteur $\overrightarrow{BA}$ est le vecteur opposé au vecteur
  $\overrightarrow{AB}$.
\end{minipage}

\begin{definition}
  On définit un vecteur nul, comme le vecteur qui envoie tout point sur
  lui-même. On le note $\vec{0}$.
\end{definition}

\begin{definition}
  On dit que deux vecteurs sont colinéaires s'ils sont portés par la
  même droite ou par des droites parallèles.
\end{definition}

\begin{propriete}
  \label{prop:colinearite1}
  Deux vecteurs colinéaires diffèrent par leur sens et par leur norme :
  $\vec{u}$ et $\vec{v}$ sont colinéaires $\iff \vec{u} = \lambda
  \vec{v}$, où $\lambda$ est un nombre réel non nul.
\end{propriete}

\begin{minipage}{0.45\linewidth}
  \begin{center}
    \begin{tikzpicture}
      \draw [thick,blue,->] (-0.5,2) node[below] {$A$} -- (2.5,4)
      node[below] {$B$} ;
      \draw [thick,blue,->] (-2.5,2) node[below] {$A'$} -- (0.5,4)
      node[below] {$B'$} ;
      \clip (-3,0) rectangle (3,5) ;
    \end{tikzpicture}

    $\overrightarrow{AB} = \overrightarrow{A'B'}$
  \end{center}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
  \begin{center}
    \begin{tikzpicture}
      \draw [thick,blue,<-] (-0.25,1) node[below] {$A$} -- (1.25,2)
      node[below] {$B$} ;
      \draw [thick,blue,->] (-2.5,2) node[below] {$D$} -- (0.5,4)
      node[below] {$C$} ;
      \clip (-3,0) rectangle (3,5) ;
    \end{tikzpicture}

    $\overrightarrow{AB} = -2\overrightarrow{CD}$
  \end{center}
\end{minipage}

\begin{propriete}[règle du parallélogramme]
  La somme et la différence de deux vecteurs sont respectivement la
  diagonale issue du point commun aux deux vecteurs et la seconde
  diagonale du parallélogramme construit sur ces deux vecteurs.
\end{propriete}

\begin{center}
  \begin{tikzpicture}
    \draw [thick,blue,->] (-2.5,2) node[below] {$A$} -- (0.5,4)
    node[above] {$C$} ;
    \draw [thick,red,->] (-2.5,2) -- (1,0.5) node[below] {$B$} ;
    \draw [thick,blue,->,dashed] (1,0.5) -- (4,2.5) ;
    \draw [thick,red,->,dashed] (0.5,4) -- (4,2.5) ;
    \draw [thick,->] (-2.5,2) -- (4,2.5) node[above] {$D$} ;
    \draw [thick,->,dotted] (1,0.5) -- (0.5,4) ;
    \clip (-3,0) rectangle (3,5) ;
  \end{tikzpicture}

  $\overrightarrow{AD} = \overrightarrow{AB}+\overrightarrow{AC}$ et
  $\overrightarrow{BC} = \overrightarrow{AC}-\overrightarrow{AB}$
\end{center}

\begin{propriete}[relation de Chasles]
  Pour tout vecteur $\overrightarrow{AB}$ et tout point $M$, on a
  $\overrightarrow{AB} = \overrightarrow{AM} + \overrightarrow{MB}$
\end{propriete}

Dans un autre contexte, la relation de Chasles s'exprime $\int_a^c f =
\int_a^b f + \int_b^c f$.

\begin{remarque}
  $\overrightarrow{AB}+\overrightarrow{BA} = \overrightarrow{AA} =
  \vec{0}$
\end{remarque}

La propriété \ref{prop:colinearite1} est équivalente à la proposition
suivante :
\begin{propriete}
  $\vec{u}$ et $\vec{v}$ ne sont pas colinéaires si, et seulement si
  $\alpha\vec{u} + \beta\vec{v} = \vec{0} \implies \alpha = \beta = 0$.
\end{propriete}

\begin{definition}
  \begin{itemize}
    \item Deux vecteurs non-colinéaires forment une base ;
    \item si on ajoute un point, on obtient un repère.
  \end{itemize}
\end{definition}

Une conséquence de cette définition est la suivante :
\begin{propriete}
  \begin{itemize}
    \item Tout vecteur $\vec{u}$ s'exprime comme une combinaison
      linéaire de vecteurs de la base choisie ;
    \item tout point de l'espace peut s'exprimer comme une translation
      de l'origine par une combinaison linéaire des vecteurs de la base.
  \end{itemize}
\end{propriete}

On peut désormais définir les coordonnées d'un vecteur et d'un point.
\begin{definition}
  \begin{itemize}
    \item Les coordonnées d'un vecteur dans une base sont les
      coefficients de la combinaison linéaire des vecteurs de la base ;
    \item les coordonnées d'un point sont les coefficients de la
      combinaisons linéaire du vecteur de «translation».
  \end{itemize}
\end{definition}

\begin{center}
  \begin{tikzpicture}
    \draw [thick,blue,->] (-2.5,2) node[below] {$A$} -- (0.5,4)
    node[above] {$C$} ;
    \draw [thick,red,->] (-2.5,2) -- (1,0.5) node[below] {$B$} ;
    \draw [thick,blue,->,dashed] (1,0.5) -- (4,2.5) ;
    \draw [thick,blue,->,dashed] (4,2.5) -- (7,4.5);
    \draw [thick,red,->,dashed] (0.5,4) -- (4,2.5) ;
    \draw (4,2.5) node[above] {$D$} ;
    \draw [thick,->] (-2.5,2) -- (7,4.5) node[above] {$E$} ;
    \clip (-3,0) rectangle (3,5) ;
  \end{tikzpicture}

  $\overrightarrow{AE} = \overrightarrow{AB}+2\overrightarrow{AC}
  \implies$ les coordonnées de $\overrightarrow{AE}$ sont $(1;2)$ dans
  la base $\left(\overrightarrow{AB},\overrightarrow{AC}\right)$ et les
  coordonnées de $E$ sont $(1;2)$ dans le repère
  $\left(A,\overrightarrow{AB},\overrightarrow{AC}\right)$.
\end{center}

\begin{remarque}
  Attention, dans la base
  $\left(\overrightarrow{AC},\overrightarrow{AB}\right)$, les
  coordonnées de $\overrightarrow{AE}$ deviennent $(2;1)$.
\end{remarque}

\begin{exercice}
  Donner les coordonnées de $\overrightarrow{AB}$ dans la base
  $(\overrightarrow{AE},\overrightarrow{AC})$.
\end{exercice}

En général, on se donne une base, ou un repère de façon «canonique», en
fixant une origine, $O$ et en se donnant deux vecteurs unitaires (c'est
à dire de norme 1) $\vec{\imath}$ et $\vec{\jmath}$.

Les coordonnées d'un vecteur s'obtiennent de la façon suivante, avec
$A:(x_A ; y_A)$ et $B:(x_B ; y_B)$ :
\[ \overrightarrow{AB} : (x_B-x_A ; y_B-y_A) \]

\begin{remarque}[milieu d'un segment]
  Les coordonnées du point $I$, milieu de $[AB]$ sont
  $I:\left(\frac{x_B+x_A}{2} ; \frac{y_B+y_A}{2}\right)$ avec $A:(x_A ; y_A)$ et $B:(x_B ; y_B)$.
\end{remarque}

\section{Produit scalaire}

Jusqu'à présent, nous n'avons fait qu'additionner des vecteurs entre eux
ou les multiplier par des nombres (appelés scalaires). On veut désormais
construire une «multiplication» entre deux vecteurs qui possède des
propriétés géométriques «intéressantes».

\begin{definition}[produit scalaire]
  Soient $\vec{u}$ et $\vec{v}$ deux vecteurs du plan.

  On définit $\vec{u}\cdot\vec{v}$ (lu «u scalaire v») de la façon
  suivante :
  \begin{itemize}
    \item si $\vec{u}$ et $\vec{v}$ sont portés par des droites
      perpendiculaires, $\vec{u}\cdot\vec{v}=0$ (on dit qu'ils sont
      orthogonaux) ;
    \item sinon $\vec{u}\cdot\vec{v}$ est le produit de la norme de
      $\vec{u}$ avec la norme du projeté orthogonal de $\vec{v}$ sur la
      direction de $\vec{u}$.
  \end{itemize}
\end{definition}

\begin{center}
  \begin{tikzpicture}
    \draw [thick,blue,->] (-2.5,2) node[below] {$A$} -- (0.5,4)
    node[above] {$C$} ;
    \draw [thick,red,->] (-2.5,2) -- (2,2.5) node[below] {$B$} ;
    \draw [thick,black] (0.5,4) -- (0.5+0.5/2.75,4-4.5/2.75) ;
    \draw [thick,black,dashed] (-2.5,2) -- (0.5+0.5/2.75,4-4.5/2.75)
    node [below] {$M$} ;
    \draw[rotate=6,xshift=0.5,yshift=-0.2]
    (0.5+0.5/2.75+0.21,4-4.5/2.75-0.07) rectangle
    (0.5+0.5/2.75-0.21+0.2,4-4.5/2.75+0.2-0.07) ;
    \clip (-1,1) rectangle (3,5) ;
  \end{tikzpicture}

  $\overrightarrow{AB}\cdot\overrightarrow{AC} = AB\times AM$.
\end{center}

\begin{remarque}
  Le produit \emph{scalaire} de deux vecteurs est un nombre (un
  scalaire).
\end{remarque}

Les relations dans le triangle rectangle nous conduisent à une propriété
permettant le calcul effectif.

\begin{propriete}
  \[ \overrightarrow{AB}\cdot\overrightarrow{AC} = AB\times AC \times
  \cos\widehat{BAC} \] où $AB=\left\lVert\overrightarrow{AB}\right\rVert$
  et $AC=\left\lVert\overrightarrow{AC}\right\rVert$.
\end{propriete}

On a les propriétés suivantes :
\begin{propriete}
  Soient $\vec{u}$, $\vec{v}$ et $\vec{w}$ trois vecteurs et $\lambda$ un nombre
  réel.
  \begin{itemize}
    \item $(\vec{u}+\vec{v})\cdot\vec{w} = \vec{u}\cdot\vec{w} +
      \vec{v}\cdot\vec{w}$
    \item $\vec{u}\cdot(\vec{v}+\vec{w}) = \vec{u}\cdot\vec{v} +
      \vec{u}\cdot\vec{w}$
    \item $\vec{u}\cdot\vec{0} = \vec{0}\cdot\vec{u} = 0$
    \item $\vec{u}\cdot\vec{v} = \vec{v}\cdot\vec{u}$
    \item $(\lambda\vec{u})\cdot\vec{v} = \vec{u}\cdot(\lambda\vec{v}) =
      \lambda(\vec{u}\cdot\vec{v})$
  \end{itemize}
\end{propriete}

On peut aussi définir le cosinus de deux vecteurs (ou d'un angle orienté
de vecteur).

\begin{definition}
  Soient deux vecteurs $\vec{u}$ et $\vec{v}$.

  \[ \cos(\vec{u},\vec{v}) =
    \frac{\vec{u}\cdot\vec{v}}{\lVert\vec{u}\rVert \times
  \lVert\vec{v}\rVert} .\]
\end{definition}

Si on impose que les vecteurs $\vec{\imath}$ et $\vec{\jmath}$ soient
orthogonaux, on dit alors que la base est orthogonale et comme ils sont
unitaire, on dit même que la base est orthonormée. On peut désormais
démontrer la proposition suivante qui donne une autre méthode pratique
du calcul du produit scalaire.

\begin{propriete}
  Soient $\vec{u}$ et $\vec{v}$ deux vecteurs dont les coordonnées dans
  la base $(\vec{\imath},\vec{\jmath})$ sont $\vec{u}:(x;y)$ et
  $\vec{v}:(x';y')$. \[ \vec{u}\cdot\vec{v} = xx' + yy' .\]
\end{propriete}

On désormais la propriété suivante donnant une autre méthode de calcul
de la norme d'un vecteur et qui complète le lien avec la distance de
deux points.

\begin{propriete}
  \[ \vec{u}\cdot\vec{u} = x^2+y^2 = \lVert\vec{u}\rVert^2 \] qui
  justifie que \[ \lVert\vec{u}\rVert = \sqrt{x^2+y^2} \] avec
  $\vec{u}:(x ; y)$.
\end{propriete}

\section{Produit vectoriel, produit mixte}

On peut sans problème étendre les parties précédentes aux vecteurs de
l'espace à trois dimensions en rajoutant une dernière composante $z$
associée à un vecteur directeur de base (unitaire) $\vec{k}$ orthogonal
aux deux précédents.

Le produit scalaire $\vec{u}\cdot\vec{v}$ devient $\vec{u}\cdot\vec{v} =
xx'+yy'+zz'$ et la norme de $\vec{u}$ devient $\lVert\vec{u}\rVert =
\sqrt{x^2+y^2+z^2}$ avec $\vec{u}:(x,y,z)$ et $\vec{v}:(x',y',z')$ dans
la base $(\vec{\imath},\vec{\jmath},\vec{k})$.

Ceci étant posé, on peut désormais définir un nouveau «produit» dont le
résultat sera cette fois un vecteur.

\begin{definition}[produit vectoriel]
  Soient $\vec{u}$ et $\vec{v}$ deux vecteurs de l'espace, on définit le
  produit vectoriel $\vec{u}\wedge\vec{v}$ comme un vecteur possédant
  les caractéristiques suivantes :
  \begin{itemize}
    \item si $\vec{u}$ et $\vec{v}$ sont colinéaires,
      $\vec{u}\wedge\vec{v}=\vec{0}$ ;
    \item sinon, $\vec{u}\wedge\vec{v}$ est un vecteur orthogonal aux
      deux précédents, de norme $AB\times AC\times\sin(\widehat{BAC})$
      et dont l'orientation est compatible avec celle de la base.
  \end{itemize}
\end{definition}

\begin{remarque}
  On peut en fait définir $\vec{k}$ par $\vec{k}=\vec{u}\wedge\vec{v}$
\end{remarque}

On a les propriétés suivantes :
\begin{propriete}
  Soient $\vec{u}$, $\vec{v}$ et $\vec{w}$ trois vecteurs et $\lambda$
  un nombre réel.
  \begin{itemize}
    \item $(\vec{u}+\vec{v})\wedge\vec{w} = \vec{u}\wedge\vec{w} +
      \vec{v}\wedge\vec{w}$
    \item $\vec{u}\wedge(\vec{v}+\vec{w}) = \vec{u}\wedge\vec{v} +
      \vec{u}\wedge\vec{w}$
    \item $\vec{u}\wedge\vec{0} = \vec{0}\wedge\vec{u} = \vec{0}$
    \item $\vec{u}\wedge\vec{v} = -\vec{v}\wedge\vec{u}$
    \item $(\lambda\vec{u})\wedge\vec{v} = \vec{u}\wedge(\lambda\vec{v})
      = \lambda(\vec{u}\wedge\vec{v})$
  \end{itemize}
\end{propriete}

On peut aussi définir le sinus de deux vecteurs (ou d'un angle orienté
de vecteur).

\begin{definition}
  Soient deux vecteurs $\vec{u}$ et $\vec{v}$.

  \[ \sin(\vec{u},\vec{v}) =
    \frac{\lVert\vec{u}\wedge\vec{v}\rVert}{\lVert\vec{u}\rVert \times
  \lVert\vec{v}\rVert} .\]
\end{definition}

On admet l'expression analytique suivante pour le produit vectoriel.
\[ \vec{u}\wedge\vec{v} = \left(\begin{array}{c} x \\ y \\ z
  \end{array}\right) \wedge \left(\begin{array}{c} x' \\ y' \\ z'
  \end{array}\right) = \left(\begin{array}{c} yz' - y'z \\ zx' - xz' \\
yx' - xy' \end{array}\right) \]

La dernière quantité définie par les vecteurs est le «produit mixte» qui
utilise à la fois le produit scalaire et le produit vectoriel. Il s'agit
d'un triplet de trois vecteurs qui permet de caractériser s'ils sont
coplanaires.

\begin{definition}[produit mixte]
  Soient $\vec{u}$, $\vec{v}$ et $\vec{w}$ trois vecteurs. Le produit
  mixte de $\vec{u}$, $\vec{v}$ et $\vec{w}$ se note \[[\vec{u},
  \vec{v}, \vec{w}] = \vec{u}\cdot(\vec{v}\wedge\vec{w}) \]
\end{definition}

\section{Applications}

L'aire d'un parallélogramme $ABCD$ peut s'exprimer avec la norme du
produit vectoriel : $\mathscr{A} = \lVert\overrightarrow{AB} \wedge
\overrightarrow{AD}\rVert$. Pour obtenir l'aire du triangle $ABD$, il
suffit de diviser la quantité précédente par 2.

Le volume d'un parallélèpipède construit sur les vecteurs $\vec{u}$,
$\vec{v}$ et $\vec{w}$ s'exprime par le produit mixte : $\mathscr{V} =
\vec{u}\cdot(\vec{v}\wedge\vec{w})$.

Le travail d'une force $\vec{F}$ le long d'un chemin
$\overrightarrow{AB}$ s'exprime par le produit scalaire
$W=\vec{F}\cdot\overrightarrow{AB}$.

Le moment d'une force $\vec{F}$ d'une force appliquée en $A$ par rapport
à une rotation en $P$ s'exprime par le produit vectoriel
$M_{F/PA}=\overrightarrow{PA}\wedge\vec{F}$.

\section*{Vade-mecum}

Cette section regroupe quelques méthodes pratiques issues du calcul
vectoriel.

Pour montrer que deux vecteurs sont orthogonaux, il faut calculer leur
produit scalaire. Si celui-ci est nul, les vecteurs sont orthogonaux.

Pour montrer que deux vecteurs sont colinéaires, il faut calculer leur
produit vectoriel. Si celui-ci est nul, les vecteurs sont colinéaires.

Pour montrer que trois vecteurs sont coplanaires, il faut calculer leur
produit mixte. Si celui-ci est nul, les vecteurs sont colinéaires.

Pour donner l'équation d'une droite, à partir d'un vecteur directeur
$\vec{u}$, il faut chercher un vecteur $\vec{v}: (x, y, z)$ tel que
$\vec{u}\wedge\vec{v}=\vec{0}$.

Pour donner l'équation d'une droite à partir d'un vecteur normal
$\vec{n}$, il faut chercher un vecteur $\vec{u}: (x, y, z)$ tel que
$\vec{n}\cdot\vec{u}=0$.



\end{document}

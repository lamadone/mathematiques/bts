\documentclass[a4paper,12pt,frenchb]{article}

\usepackage[utf8]{inputenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{BTS}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\newtheorem{propriete}{Propriété}
\newtheorem{definition}{Définition}

\begin{document}
\maketitle
Ici, $f$ désigne une fonction continue et dérivable sur $I=[a;b]$.

\section{Primitives}

On admet qu'il existe une fonction $F$ dérivable sur $I$ telle que pour
tout $x$ de $I$, $F'(x)=f(x)$. On appelle une telle fonction $F$ primitive
de $f$ sur $I$.

Si $F$ est une primitive de $f$ sur $I$, alors pour tout $k$ réel, $x\mapsto
F(x)+k$ est aussi une primitive de $f$ sur $I$.

Les primitives s'obtiennent par «lecture inverse» des dérivées.

\begin{multicols}{2}
  \begin{center}
    \begin{tabular}{|c|c|}\hline
      $f:x\mapsto$  & $F:x\mapsto$ \\ \hline
      0             & $k$ \\ \hline
      $a$           & $ax+k$ \\ \hline
      $x$           & $\dfrac{1}{2}x^2+k$ \\ \hline
      $x^n$         & $\dfrac{1}{n+1}x^{n+1}+k$ \\ \hline
    \end{tabular}
  \end{center}
 \begin{center}
    \begin{tabular}{|c|c|}\hline
      $f:x\mapsto$     & $F:x\mapsto$ \\ \hline
      $\dfrac{1}{x^2}$ & $-\dfrac{1}{x}+k$ \\ \hline
      $\dfrac{1}{x}$   & $\ln{x}+k$ \\ \hline
      $e^x$            & $e^x+k$ \\ \hline
      $e^{ax}$         & $\dfrac{1}{a}e^{ax}+k$ \\ \hline
    \end{tabular}
  \end{center}
\end{multicols}

On a également les propriétés suivantes :
\begin{itemize}
  \item si $F$ et $G$ sont respectivement des primitives de $f$ et $g$, alors
    $F+G$ est une primitive de $f+g$ ;
  \item si $F$ est une primitive de $f$ et $\lambda$ un réel, alors $\lambda
    F$ est une primitive de $\lambda f$.
\end{itemize}

\section{Intégrale d'une fonction}

Il s'agit du \textbf{nombre} $\boxed{\int_a^b{f(x)\mathrm{d}x}}$. Il se lit
«intégrale \emph{(ou somme)} de $a$ à $b$ de $f$ de $x$ $\mathrm{d}x$».

Si $F$ est une primitive de $f$ sur $[a;b]$,
\[\boxed{\int_a^b{f(x)\mathrm{d}x}=\Bigl[F(x)\Bigr]_a^b=F(b)-F(a)}\].

\section{Interprétation graphique}

Pour une fonction positive sur I ($f(x)>0)$, l'intégrale de $f$ entre $a$ et
$b$ est l'aire limitée par $\mathcal{C}_f$ et les courbes d'équations $y=0$,
$x=a$ et $x=b$. C'est l'aire bleue du dessin ci-dessous.

\begin{center}
  \scriptsize
  \begin{tikzpicture}[domain=0.9:3.1,smooth]
    \draw plot (\x,\x^3-6*\x^2+11*\x-5) node[below,right] {$\mathcal{C}_f$} ;
    \draw [->] (-1,0) -- (4,0) ;
    \draw [->] (0,-1) -- (0,2) ;
    \filldraw [fill=blue!50] plot [domain=1:2.5] (\x,\x^3-6*\x^2+11*\x-5) --
    (2.5,0) -- (1,0) -- cycle ;
    \draw [thick,red] (1,0) -- (2.5,0) ;
    \draw (1,0) node[below] {$a$} -- (1,1) ;
    \draw (0,1) node[left] {$f(a)$} ;
    \draw [dashed] (0,1) -- (1,1) ;
    \pgfmathparse{2.5^3-6*2.5^2+11*2.5-5}
    \let\b\pgfmathresult
    \draw (2.5,0) node[below] {$b$} -- (2.5,\b) ;
    \draw [dashed] (0,\b) -- (2.5,\b) ;
    \draw (0,\b) node[left] {$f(b)$} ;
    \draw [<-] (1.5,0.5) -- (3.5,1.5) node[above,right] {$\displaystyle
    I=\int_a^b{f(x)\mathrm{d}x}$};
  \end{tikzpicture}
  \normalsize
\end{center}

Dans le cas d'une fonction négative, $-f$ est positive et $f$ et $-f$ sont
symétriques par rapport à l'axe des abscisses. Dans ce cas, l'aire est
l'intégrale de $-f$, c'est à dire
$\mathcal{A}=\int_a^b{\left(-f(x)\right)\mathrm{d}x}$.

Dans le cas d'une fonction de signe non constant, il faut séparer les
intervalles sur lesquels la fonction est positive de ceux sur lesquels la
fonction est négative. On peut aussi retenir que $\mathcal{A} =
\int_a^b{\left|f(x)\right|\mathrm{d}x}$.

\section{Propriétés générales}

\begin{propriete}Relation de Chasles

  Soient $c\in\left]a;b\right[$ et $f$ une fonction, on a
  \[\boxed{\int_a^c{f(x)\mathrm{d}x} + \int_c^b{f(x)dx} = \int_a^b{f(x)dx}} \]
\end{propriete}

\begin{propriete}
  Linéarité

  Soient $f$ et $g$ deux fonctions dérivables sur $\left[a;b\right]$ et
  $\lambda$ un réel, on a
  \[\boxed{\int_a^b{\left(f(x)+g(x)\right)\mathrm{d}x} = \int_a^b{f(x)dx} +
  \int_a^b{g(x)\mathrm{d}x}} \]
  et
  \[\boxed{\int_a^b{\lambda f(x)\mathrm{d}x} = \lambda\int_a^b{f(x)dx}}\]
\end{propriete}

\begin{propriete}
  Inégalités

  Soient $f$ et $g$ deux fonctions dérivables sur $[a;b]$, on a

  \begin{itemize}
    \item Si $f\leq g$, alors $\displaystyle \int_a^b{f(x)\mathrm{d}x} \leq
      \int_a^b{g(x)\mathrm{d}x}$ ;
    \item Si $f(x) \geq 0$ pour $x\in[a;b]$, $\displaystyle
      \int_a^b{f(x)\mathrm{d}x} \geq 0$ ;
    \item $\displaystyle \left|\int_a^b{f(x)\mathrm{d}x} \right| \leq
      \int_a^b{\left|f(x)\right|\mathrm{d}x}$.
  \end{itemize}
\end{propriete}

\begin{propriete}
  Inégalité de la moyenne

  Soit $f$ une fonction dérivable sur $[a;b]$. S'il existe des réels $m$ et
  $M$ tels que pour tout $x\in[a;b]$, $m \leq f(x) \leq M$, alors
  $\displaystyle m(b-a) \leq \int_a^b{f(x)\mathrm{d}x} \leq M(b-a)$.
\end{propriete}

\begin{definition}
  Soit $f$ une fonction dérivable sur $[a;b]$, on appelle \textbf{valeur
  moyenne} de $f$ sur $[a;b]$ le nombre $\mu_f$ défini par
  \[ \mu_f = \frac{1}{b-a}\int_a^b{f(x)\mathrm{d}x}.\]
\end{definition}

\begin{propriete}
  Inégalités des accroissements finis

  Soit $f$ une fonction dérivable sur $[a;b]$. S'il existe deux réels $m$ et
  $M$ tels que pour tout $x\in[a;b]$, si $m \leq f'(x) \leq M$, alors $m(b-a)
  \leq f(b)-f(a) \leq M(b-a)$.
\end{propriete}

\section*{Remarque : pourquoi $\mathrm{d}x$ ?}

\begin{center}
  \begin{tikzpicture}[domain=0.9:3.1,smooth]
    \scriptsize
    \draw [->] (-1,0) -- (4,0) ;
    \draw [->] (0,-1) -- (0,2) ;
    \filldraw [fill=blue!50] plot [domain=1:2.5] (\x,\x^3-6*\x^2+11*\x-5) --
    (2.5,0) -- (1,0) -- cycle ;

    \newcommand{\A}{1}
    \newcommand{\B}{2.5}
    \pgfmathsetmacro{\dx}{(\B-\A)/2/5}
    \foreach \pas in {0,...,9}{
      \pgfmathsetmacro{\couleur}{(\pas+1)*10}
      \pgfmathsetmacro{\increment}{\A+\dx*\pas}
      \pgfmathsetmacro{\f}{\increment^3-6*\increment^2+11*\increment-5}
      \filldraw [fill=green!\couleur] (\increment,0) -- (\increment,\f) --
      (\increment+\dx,\f) -- (\increment+\dx,0) -- cycle ;
    }

    \draw plot (\x,\x^3-6*\x^2+11*\x-5) node[below,right] {$\mathcal{C}_f$} ;
    \draw [thick,red] (1,0) -- (2.5,0) ;
    \draw (1,0) node[below] {$a$} -- (1,1) ;
    \draw (0,1) node[left] {$f(a)$} ;
    \draw [dashed] (0,1) -- (1,1) ;
    \pgfmathparse{2.5^3-6*2.5^2+11*2.5-5}
    \let\b\pgfmathresult
    \draw (2.5,0) node[below] {$b$} -- (2.5,\b) ;
    \draw [dashed] (0,\b) -- (2.5,\b) ;
    \draw (0,\b) node[left] {$f(b)$} ;

    \draw (\A+4*\dx,0.05) -- (\A+4*\dx,-0.05) ;
    \draw (\A+5*\dx,0.05) -- (\A+5*\dx,-0.05) ;
    \draw [<->] (\A+4*\dx,-0.05) -- (\A+5*\dx,-0.05) ;
    \draw (\A+4.5*\dx,-0.2) node {\tiny $\Delta x$} ;

  \end{tikzpicture}
  \normalsize
\end{center}

L'intégrale peut s'approcher graphiqument comme une somme de rectangles de
hauteur $f(x)$, avec $x$ prenant un nombre fini de valeurs entre $a$ et $b$ et
de largeur $\Delta x$. Comme $\Delta x$ est connu et fixe (c'est le pas), on
peut le «mettre en facteur».

Afin d'obtenir un résultat qui «colle» le plus possible à la courbe, on réduit
$\Delta x$, ce qui a conduit à la notation $\mathrm{d}x$.


\end{document}

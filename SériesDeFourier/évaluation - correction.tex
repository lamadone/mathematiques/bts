\documentclass[12pt,a4paper,frenchb]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}
\usepackage{babel}
\frenchbsetup{og=«,fg=»}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\renewtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}


% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Séries de Fourier}
\author{BTS}
\date{14 octobre}

\begin{document}

\maketitle

\begin{exercice} «Redressement biphasé»

  On considère la fonction $f$ définie sur $\mathbf{R}$ par $f(t) =
  \left|\cos t\right|$.

  Soient $a_n$ et $b_n$ les coefficients de Fourier de cette fonction.

  \begin{enumerate}
    \item Représenter la fonction $f$ sur l'intervalle $\left[-2\pi ;
      4\pi\right]$.

      \begin{center}
        \begin{tikzpicture}[xscale=0.7]
          \draw[gray, thin, dashed] (-2*3.14159,-0.0) grid
          (4*3.14159,1.4) ;
          \draw[->] (-2*3.14159,0) -- (4*3.14159,0) node[above left] {$t$};
          \draw[->] (0,-0.4) -- (0,1.7) node[below right] {$f(t)$} ;

          \draw plot[smooth,domain=-2*3.14159:4*3.14159]
          (\x,{abs(cos(\x r))}) ;

          \foreach \i/\j in {-2/-2,-1/-,1/,2/2,3/3,4/4} {
            \draw (\i*3.14159,0) -- (\i*3.14159,-0.1) ;
            \draw (\i*3.14159,-0.25) node { $\j\pi$ } ;
          }
       \end{tikzpicture}
      \end{center}


    \item Prouvons que $f$ est paire de période $\pi$.

      \emph{Parité} : $f(-t) = \left| \cos(-t) \right| = \left| \cos(t)
      \right|$. On utilise ici la parité de la fonction $\cos$ et le
      caractère bijectif de $\left|\cdot\right|$ sur les deux
      demi-droites réelles d'origine 0.

      \emph{Périodicité} : Soit $t$ un nombre réel. $f(t+\pi) = \left|
      \cos(t+\pi) \right| = \left|-cos(t) \right| = f(t)$.

      Donc $f$ est paire, de péridoe $\pi$.

    \item Déterminon les valeurs des coefficients $b_n$.

      Par parité de $f$, on a $\forall n \geq 1,\ b_n = 0$

    \item Calculer la valeur moyenne de $f$ sur une période.

      Ici, il s'agit de calculer $a_0 = \frac1T\int_a^{a+T}f(t)
      \mathrm{d}t$

      En prenant ici $a = \frac{\pi}2$, on obtient $a_0 =
      \frac1{\pi}\int_{-\frac{\pi}{2}}^{\frac{\pi}{2}}\cos t\mathrm{d}t
      = \frac2\pi \int_0^{\frac\pi2}\cos t\mathrm{d}t = \frac2\pi
      \big[\sin t\big]_0^\frac\pi2 = \frac2\pi$


    \item Soit $n>0$, pour le calcul de $a_n$, partant de la définition,
      on a :

      \[ a_n = \frac2\pi\int_{-\frac\pi2}^{\frac\pi2} \cos(2n\pi
        t)\left| \cos t \right| \mathrm{d}t = \frac4\pi\int_0^{\frac\pi2}
      \cos(2n\pi t) \cos t \mathrm{d}t \]

      On utilise ici la parité et la positivité de $\cos$ sur le segment
      d'intégration pour réduire $\left|\cos\right|$ à $\cos$.

      Développons $\cos((2n+1)t) + \cos((2n-1)t)$.

      \begin{align*}
        \cos((2n+1)t) + \cos((2n-1)t) & = \cos(2nt+t) + \cos(2nt+t) \\
                                      & = \cos(2nt)\cos(t) - \sin(2nt)
        \sin(t) \\
        & + \cos(2nt)\cos(t) + \sin(2nt)\sin(t) \\
        & = 2\cos(2nt)\cos(t)
      \end{align*}

      D'où le résultat attendu :

      \[ a_n = \frac2\pi \int_0^{\frac\pi2}{\left[\cos((2n+1)t) +
      \cos((2n-1)t)\right]\,dt} \]

      En intégrant, on obtient :
      \[ a_n = \frac2{\pi(2n+1)} \big[\sin((2n+1)t)\big]_0^{\frac\pi2} +
      \frac2{\pi(2n-1)} \big[\sin((2n-1)t)\big]_0^{\frac\pi2} \]

      En évaluant en 0 et en $\frac\pi2$, on obtient :
      \[ a_n = \frac2\pi\left(\frac{(-1)^n}{1+2n} - \frac{(-1)^n}{1-2n}
      \right) \]

      Il suffit de réduire au même dénominateur pour en déduire que
      $a_n=\frac{4(-1)^n}{\pi(1-4n^2)}$.
    \item Écrire les cinq premiers termes de la série de Fourier
      associée à $f$.
  \end{enumerate}
\end{exercice}

\begin{exercice}~

  \begin{center}
    \begin{tikzpicture}
      \draw[gray, thin, dashed] (-5,-1.2) grid (5,1.2) ;
      \draw[->] (-5,0) -- (5,0) node[above left] {$t$};
      \draw[->] (0,-1.2) -- (0,1.2) node[below right] {$s(t)$} ;

      \draw[dashed] plot[domain=-5:-4] (\x,\x+5) ;
      \draw plot[domain=-4:-2] (\x,\x+3) ;
      \draw plot[domain=-2:0] (\x,\x+1) ;
      \draw plot[domain=0:2] (\x,\x-1) ;
      \draw plot[domain=2:4] (\x,\x-3) ;
      \draw[dashed] plot[domain=4:5] (\x,\x-5) ;

%     \draw plot[domain=0:4,samples=100]
%     (\x,{(0-1/3.14159*sin(3.14159*\x r)-1/2/3.14159*sin(2*3.14159*\x r)
%     -1/3/3.14159*sin(3*3.14159*\x r) -1/4/3.14159*sin(4*3.14159*\x r)
%     -1/5/3.14159*sin(5*3.14159*\x r) -1/6/3.14159*sin(6*3.14159*\x r)
%     -1/7/3.14159*sin(7*3.14159*\x r) -1/8/3.14159*sin(8*3.14159*\x r)
%     )*2}) ;
    \end{tikzpicture}
  \end{center}

  \begin{enumerate}
    \item Donner l'expression de cette fonction sur une période.
    \item Calculer les coefficients de Fourier de cette fonction et
      donner une expression de son développement en série de Fourier.
    \item Donner le spectre de cette fonction et le tracer dans un
      repère.
    \item
      \begin{enumerate}
        \item Écrire l'égalité de Parseval.
        \item Calculer l'intégrale du carré de $f$ sur une période.
        \item Factoriser le membre de gauche.
        \item En déduire la valeur de $\sum\frac1{n^2}$.
      \end{enumerate}
  \end{enumerate}
\end{exercice}

\end{document}

\documentclass[10pt,a4paper,frenchb]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.5cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{lastpage}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}
\usepackage{babel}
\frenchbsetup{og=«,fg=»}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\makeatother


%\theoremstyle{break}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{exemple}{Exemple}
\newtheorem{theoreme}{Théorème}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Séries numérique, fonctions périodiques et séries de Fourier}
\author{BTS}
\date{}

\begin{document}

\maketitle

\section{Généralités}

Soit $u$ une suite numérique, on peut construire la suite
$\sum_{k=0}^n{u_k}$.
\begin{definition}
  Pour toute suite $u$, on peut associer sa série, c'est à dire la suite
  $\sum_{k=0}^n{u_k}$.

  Si $\sum_{k=0}^n{u_n}$ converge, on peut dit que la série
  $\sum{u_n}$ converge et on note sa somme $\sum{u_n} =
  \sum_{n=0}^{+\infty}{u_n}$.

  Si $\sum_{k=0}^n{u_n}$ diverge, on dit alors que la série $\sum{u_n}$
  diverge.
\end{definition}

Attention, cette notation ne possède de sens que si on a convergence.

\begin{exemple}
  Soit $x$ un nombre réel fixé. La suite $\sum_{k=0}^n{\frac{x^k}{k!}}$
  converge, car c'est le développement de l'exponentielle. Pour cet $x$,
  on peut même écrire $\sum{\frac{x^n}{n!}}=e^x$
\end{exemple}

\subsection{Des séries convergentes}

\begin{propriete}
  On admet qu'une série géométrique converge si, et seulement si sa
  raison est comprise strictement entre -1 et 1.

  On a $\sum_{k=0}^n{u_0q^k} = u_0\frac{1-q^n}{1-q}$, et par passage à
  la limite $\sum{u_0q^n} = u_0\frac1{1-q}$.
\end{propriete}

\begin{definition}
  Une série de terme général $\frac1{n^{\alpha}}$ s'appelle une série de
  Riemann
\end{definition}

\begin{propriete}
  On admet qu'une série de terme général $\frac1{n^{\alpha}}$ avec
  $\alpha > 1$ converge.
\end{propriete}

\subsection{De la convergence des séries}

On peut noter la condition nécessaire suivante, mais non suffisante.

\begin{propriete}
  Si $\sum{u_n}$ est une série convergente, alors $\lim u_n = 0$
\end{propriete}

La réciproque est bien évidement fausse.

\begin{exemple}
  La suite de terme général $u_n=\frac1{n+1}$ converge vers 0, alors que
  la série $\sum\frac1{n+1}$ est une série divergente
\end{exemple}

Néanmoins, on dispose des propriétés suivantes, pour conclure dans de
nombreux cas quand à la convergence des séries.

\begin{propriete}
  \begin{itemize}
    \item Si, à partir d'un certain rang, $u_n\leq v_n$, alors si $\sum
      v_n$ converge, $\sum u_n$ converge.
    \item Si, à partir d'un certain rang, $u_n\geq v_n$, alors si $\sum
      v_n$ diverge, $\sum u_n$ diverge.
    \item Si, à partir d'un certain rang, $u_n \sim v_n$, alors $\sum
      u_n$ et $\sum v_n$ ont le même comportement.
  \end{itemize}
\end{propriete}

\begin{propriete}
  Toute série de terme général inférieur à $\frac1{n^{\alpha}}$
  converge.
\end{propriete}

\begin{propriete}[Critère de Cauchy]
  \begin{itemize}
    \item Une suite est convergente si et seulement si, à partir d'un
      certain rang $N$, si $p>n>N, |u_n-u_p| < \varepsilon$.
    \item Une série est convergente si et seulement si, à partir d'un
      certain rang $N$, si $p>n>N, \left|\sum_{k=n}^pu_n\right| <
      \varepsilon$.
  \end{itemize}
\end{propriete}

Ce critère traduit le fait que si, à partir d'un certain rang, une suite
ne varie pas trop, elle converge. Ce critère permet de déterminer si une
suite possède une limite sans la calculer de façon explicite. Pour les
sériesl, il s'agit juste de l'application du critère sur les suites des
sommes partielles.

\begin{exemple}
  La série harmonique est divergente.

  En effet, $\frac1n+\frac1{n+1} = \frac{2n+1}{n^2+n} > \varepsilon$
\end{exemple}

\section{Séries de Fourier}

\subsection{Généralités}

Soit $f$ une fonction de la variable réelle à valeurs dans $\mathbf{R}$,
$T$-périodique et continue par morceaux.

\begin{definition}
  Une fonction $T$-périodique est une fonction telle que pour tout $t$
  réel, $f(t+T)=f(t)$
\end{definition}

\begin{exemple}\label{exemple:rampe_periodique}
  Le signal $s(t)$ ici est 2-périodique, continu sur
  $\left]-1~;~1\right[$.
  \begin{center}
    \begin{tikzpicture}
      \draw[gray, thin, dashed] (-5,-1.2) grid (5,1.2) ;
      \draw[->] (-5,0) -- (5,0) node[above left] {$t$};
      \draw[->] (0,-1.2) -- (0,1.2) node[below right] {$s(t)$} ;

      \draw[dashed] plot[domain=-5:-4] (\x,\x+5) ;
      \draw plot[domain=-4:-2] (\x,\x+3) ;
      \draw plot[domain=-2:0] (\x,\x+1) ;
      \draw plot[domain=0:2] (\x,\x-1);
      \draw plot[domain=2:4] (\x,\x-3) ;
      \draw[dashed] plot[domain=4:5] (\x,\x-5) ;
    \end{tikzpicture}
  \end{center}
  On peut vérifier, que pour tout $t$ réel, $s(t+2)=s(t)$
\end{exemple}

\begin{propriete}
  Par récurrence, on montre que, pour tout entier $n$ et pour tout $t$
  réel, si $f$ est $T$-périodique, $f(t~+~nT)~=~f(t)$
\end{propriete}

Une classe de fonctions «naturellement» périodiques sont les fonctions
$\sin$ et $\cos$.

Pour les fonctions, on rappelle également les notions suivantes.

\begin{definition}
  \begin{itemize}
    \item On dit que $f$ est \emph{paire} si pour tout $x$ réel,
      $f(-x)=f(x)$. Sa courbe $\mathscr{C}_f$ est symétrique par rapport
      à l'axe des ordonnées.
    \item On dit que $f$ est \emph{impaire} si pour tout $x$ réel,
      $f(-x)=-f(x)$. Sa courbe $\mathscr{C}_f$ est symétrique par
      rapport à l'origine.
  \end{itemize}
\end{definition}

\begin{exemple}
  \begin{itemize}
    \item Le signal représenté dans l'exemple
      \ref{exemple:rampe_periodique} est celui d'une fonction impaire.
    \item $\cos$ est une fonction paire.
  \end{itemize}
\end{exemple}

\subsection{Série de Fourier associée à une fonction}

Pour un signal $s(t)$, $T$-périodique, on peut chercher à approcher ce
signal par une somme de $\cos$ et de $\sin$ sous la forme suivante. On
pose désormais $\omega=\frac{2\pi}T$.

\begin{definition}

  La série $a_0 + \sum_{n\geq 1}\left(a_n\cos\left(n\omega t\right) +
  b_n\sin\left(n\omega t\right)\right)$, où $a_n$ et $b_n$ sont des
  réels est une série de Fourier si cette somme converge pour tout $t$.
\end{definition}

On s'intéresse désormais au problème «inverse» : étant donné une
fonction, peut-on trouver des coefficients tels que la série de Fourier
construite avec ces coefficients converge vers la fonction.

\begin{theoreme}
  Soit $f$ une fonction continue par morceaux, à dérivée continue par
  morceaux, telle que $f$ et $f'$ possèdent des limites à droite et à
  gauche aux points de discontinuité. Dans ce cas, pour tout $x$ réel,
  la série de Fourier calculée en $x$ converge vers $f(x)$, avec les
  coefficients :
  \begin{itemize}
    \item[\textbullet] $a_0=\frac1T\int_0^{T}{s(t)\,dt}$
    \item[\textbullet] $a_n=\frac2T\int_0^{T}{s(t)\cos\left(n\omega
      t\right)\,dt}$
    \item[\textbullet] $b_n=\frac2T\int_0^{T}{s(t)\sin\left(n\omega
      t\right)\,dt}$
  \end{itemize}
\end{theoreme}

\begin{propriete}
  \begin{itemize}
    \item Si $s(t)$ est un signal pair, alors $\forall n\in\mathbf{N}^*$,
      $b_n=0$.
    \item Si $s(t)$ est un signal impair, alors $\forall n\in\mathbf{N}$,
      $a_n=0$.
  \end{itemize}
\end{propriete}

\begin{exemple}
  On considère la fonction créneau de période 4.

  \begin{center}
    \begin{tikzpicture}
      \draw[gray, thin, dashed] (-5,-0.2) grid (5,1.2) ;
      \draw[->] (-5,0) -- (5,0) node[above left] {$t$};
      \draw[->] (0,-0.2) -- (0,1.2) node[below right] {$s(t)$} ;

      \draw[thick,dashed] plot[domain=-5:-4] (\x,0) ;
      \draw[thick] plot[domain=-4:-2] (\x,1) ;
      \draw[thick] plot[domain=-2:0] (\x,0) ;
      \draw[thick] plot[domain=0:2] (\x,1);
      \draw[thick] plot[domain=2:4] (\x,0) ;
      \draw[thick,dashed] plot[domain=4:5] (\x,1) ;
    \end{tikzpicture}
  \end{center}

  Ici, on a $a_0=\frac12\int_0^4{s(t)\,dt} = 1$. La fonction étant nulle
  sur $[2;4]$, il suffit de calculer les coefficients avec des
  intégrales entre 0 et 2. On a ici $\omega = \frac{\pi}2$.

  $a_n=\frac22\int_0^2{\cos(n\frac{\pi}2 t)\,dt} =
  \frac1{n\frac{\pi}2}\big[\sin(n\frac{\pi}2 t)\big]_0^2=0$ et

  $b_n=\frac22\int_0^2{\sin(n\frac{\pi}2 t)\,dt} =
  -\frac1{n\frac{\pi}2}\big[\cos(n\frac{\pi}2
  t)\big]_0^2 = \frac1{n\pi}(\cos(2n\pi)-1) = \frac{2 - 2
  \left(-1\right)^{n}}{\pi n}$
\end{exemple}

Les coefficients $a_0$, $(a_n)_{n\geq 1}$ et $(b_n)_{n\geq 1}$ suffisent
à donner la série de Fourier.

\subsection{Convergence d'une série de Fourier}

Après avoir défini la série de Fourier d'une fonction, on souhaite
savoir si cette série converge et si oui quelle est sa limite.
Intuitivement, on souhaiterait que la série converge vers le signal $s$
pour lequel on a calculer des coefficients de Fourier.

\begin{theoreme}[Conditions de Dirichlet]
  On suppose que $f$ est $T$-période, et que de plus, $f$ est continue
  par morceaux sur une période et qu'en tout point d'une période, elle
  possède une dérivée à gauche et à droite.

  Sous ces conditions, la série de Fourier associée à $f$ converge vers
  $f$.
\end{theoreme}

\begin{exemple}[Convergence de la série de Fourier pour certaines
  valeurs de $n$.]
  Pour le signal de l'exemple \ref{exemple:rampe_periodique}, la série
  de Fourier donne :
  \begin{itemize}
    \item $n=4$

      \begin{center}
        \begin{tikzpicture}
          \draw[gray, thin, dashed] (-5,-1.2) grid (5,1.2) ;
          \draw[->] (-5,0) -- (5,0) node[above left] {$t$};
          \draw[->] (0,-1.2) -- (0,1.2) node[below right] {$s(t)$} ;

          \draw[dashed] plot[domain=-5:-4] (\x,\x+5) ;
          \draw plot[domain=-4:-2] (\x,\x+3) ;
          \draw plot[domain=-2:0] (\x,\x+1) ;
          \draw[dashed] plot[domain=4:5] (\x,\x-5) ;

          \draw plot[domain=0:4,samples=200]
          (\x,{(0-1/3.14159*sin(3.14159*\x r)-1/2/3.14159*sin(2*3.14159*\x r)
            -1/3/3.14159*sin(3*3.14159*\x r) -1/4/3.14159*sin(4*3.14159*\x r)
          )*2}) ;
        \end{tikzpicture}
      \end{center}

    \item $n=6$
      \begin{center}
        \begin{tikzpicture}
          \draw[gray, thin, dashed] (-5,-1.2) grid (5,1.2) ;
          \draw[->] (-5,0) -- (5,0) node[above left] {$t$};
          \draw[->] (0,-1.2) -- (0,1.2) node[below right] {$s(t)$} ;

          \draw[dashed] plot[domain=-5:-4] (\x,\x+5) ;
          \draw plot[domain=-4:-2] (\x,\x+3) ;
          \draw plot[domain=-2:0] (\x,\x+1) ;
          \draw[dashed] plot[domain=4:5] (\x,\x-5) ;

          \draw plot[domain=0:4,samples=200]
          (\x,{(0-1/3.14159*sin(3.14159*\x r)-1/2/3.14159*sin(2*3.14159*\x r)
            -1/3/3.14159*sin(3*3.14159*\x r) -1/4/3.14159*sin(4*3.14159*\x r)
            -1/5/3.14159*sin(5*3.14159*\x r) -1/6/3.14159*sin(6*3.14159*\x r)
          )*2}) ;
        \end{tikzpicture}
      \end{center}

    \item $n=8$
      \begin{center}
        \begin{tikzpicture}
          \draw[gray, thin, dashed] (-5,-1.2) grid (5,1.2) ;
          \draw[->] (-5,0) -- (5,0) node[above left] {$t$};
          \draw[->] (0,-1.2) -- (0,1.2) node[below right] {$s(t)$} ;

          \draw[dashed] plot[domain=-5:-4] (\x,\x+5) ;
          \draw plot[domain=-4:-2] (\x,\x+3) ;
          \draw plot[domain=-2:0] (\x,\x+1) ;
          \draw[dashed] plot[domain=4:5] (\x,\x-5) ;

          \draw plot[domain=0:4,samples=200]
          (\x,{(0-1/3.14159*sin(3.14159*\x r)-1/2/3.14159*sin(2*3.14159*\x r)
            -1/3/3.14159*sin(3*3.14159*\x r) -1/4/3.14159*sin(4*3.14159*\x r)
            -1/5/3.14159*sin(5*3.14159*\x r) -1/6/3.14159*sin(6*3.14159*\x r)
            -1/7/3.14159*sin(7*3.14159*\x r) -1/8/3.14159*sin(8*3.14159*\x r)
          )*2}) ;
        \end{tikzpicture}
      \end{center}
  \end{itemize}
\end{exemple}


\subsection{Analyse spectrale}

On considère désormais que les fonctions vérifient les conditions de
Dirichlet, c'est-à-dire qu'elles sont continues par morceaux, à dérivée
continue par morceaux et qu'aux points où elles ne sont pas continues,
ni dérivables, $f$ et $f'$ possèdent toutes deux des limites à droites
et à gauche.

En remarquant que, pour tout $n$ entier $a_n\cos(n\omega t) +
b_n\sin(n\omega t) = A_n\sin(n\omega t - \phi_n)$, avec
$A_n=\sqrt{a_n^2+b_n^2}$ pour tout $n>0$.

\begin{definition}
  Les coefficients $A_n$ constituent le \emph{spectre} de la série de
  Fourier.
\end{definition}

C'est bien souvent ces seuls coefficients qui sont donnés.

Ils sont souvent utiles grâce à la formule de Parseval.

\begin{theoreme}
  Soit $f$ une fonction périodique continue par morceaux. On a alors \[
  \frac1T\int_0^T{\left(f(t)\right)^2\,dt} = a_0^2 +
\frac12\sum_{n=1}^{+\infty}{\left(a_n^2+b_n^2\right)}\]
\end{theoreme}

Avec le spectre, on peut réécrire la formule précédente comme
$a_0^2 + \frac12\sum_{n=1}^{+\infty}{A_n^2}$

Cette formule est particulièrement utile pour donner la valeur moyenne
de la puissance sur une période, connaissant le spectre du courant ou de
l'intensité.

\begin{exemple}

  On considère, dans cet exemple, le signal $s$, pair,
  $2\pi$-périodique, défini par $s(t)=t$ pour $t\in [0,\pi[$.
  \begin{center}
    \begin{tikzpicture}
      \draw[gray, thin, dashed] (-4,-0.2) grid (4,1.2) ;
      \draw[->] (-4,0) -- (4,0) node[above left] {$t$};
      \draw[->] (0,-0.2) -- (0,1.2) node[above right] {$s(t)$} ;

      \draw plot[domain=-4:-3] (\x,{abs(\x+4)}) ;
      \draw plot[domain=-3:-1] (\x,{abs(\x+2)}) ;
      \draw plot[domain=-1:1] (\x,{abs(\x)});
      \draw plot[domain=1:3] (\x,{abs(\x-2)}) ;
      \draw plot[domain=3:4] (\x,{abs(\x-4)}) ;
    \end{tikzpicture}
  \end{center}

  Ici, le signal est pair, donc $\forall n\in\mathbf{N}$, $b_n=0$.\\
  Calculons $a_n$

  Un logiciel de calcul formel donne
  $\frac1{2\pi}\int_{-\pi}^{\pi}{\,(s(t))\cos(nt)\,dt} =
  \frac2{2\pi}\int_{t}{t\cos(t)\,dt} = \frac{2((-1)^n-1)}{\pi n^2}$

  Il faut calculer maintenant la moyenne sur une période, c'est à dire
  le coefficient $a_0$

  On trouve $a_0=\frac{\pi}{2}$.

  Ici, $s$ est continue, $\mathcal{C}^1$ par morceaux, donc
  \[ s(t) = \frac\pi2 +
  \frac4\pi\sum_{n=0}^{+\infty}{\frac{\cos((2n+1)t)}{(2n+1)^2}} \]

  En $t = 0$, on obtient que
  $\sum_{n=0}^{+\infty}{\frac1{(2n+1)^2}} = \frac{\pi^2}{8}$ et
  $\sum_{n=0}^{+\infty}{\frac1{(2n+1)^2}} + \frac14
  \sum_{n=1}^{+\infty}{\frac1{n^2}} =
  \sum_{k=1}^{+\infty}{\frac1{k^2}}$.

  Donc $\boxed{\sum_{k=1}^{+\infty}{\frac1{k^2}} = \frac{\pi^2}{6}}$
  En utilisant l'égalité de Parseval, on obtient que
  $\frac1{2\pi}\int_{-\pi}^{\pi}{t^2\,dt} = \frac{\pi^2}4 +
  \frac8{\pi^2}\sum_{n=0}^{\infty}{\frac1{(2n+1)^4}}$.
  De plus, $\frac1{2\pi}\int_{-\pi}^{\pi}{t^2\,dt} = \frac{\pi^2}{3}$,
  d'où $\sum_{n=0}^{\infty}{\frac1{(2n+1)^4}} = \frac{\pi^4}{96}$ et
  $\boxed{\sum_{k=1}^{+\infty}{\frac1{k^4}} = \frac{\pi^4}{90}}$

\end{exemple}

\end{document}

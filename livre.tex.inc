% vim: set ft=tex :
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fontawesome}
\usepackage{stmaryrd}
%\usepackage{cmbright}
%\usepackage{kpfonts}
\usepackage{lmodern}

%\pdfminorversion 7
%\pdfobjcompresslevel 3

%\PrerenderUnicode{é}

\usepackage[colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\usepackage[a4paper,vmargin=12.7mm,hmargin=6.35mm,includefoot,includehead]{geometry}
%\usepackage[top=6.35mm,bottom=6.35mm,left=2cm,right=2cm,includefoot]{geometry}

%\PassOptionsToPackage{framemethod=tikz}{mdframed}
\usepackage{tipfr}
\usepackage[tikz]{bclogo}
\usepackage{tikz}
\usetikzlibrary{circuits.ee.IEC}

\usepackage{tikz-cd}

\usepackage{tkz-euclide}
\usepackage{tkz-tab}

\usetkzobj{all}
\usetikzlibrary{hobby,shapes.misc}

\tikzset{cross/.style={cross out, draw, 
minimum size=2*(#1-\pgflinewidth), 
inner sep=0pt, outer sep=0pt}}

\usepackage{amsmath,amsfonts,amssymb}
\usepackage{mathrsfs}

\usepackage{mathtools}% To colour equation numbers in proof

\definecolor{prop}{RGB}{128,24,24}
\definecolor{preuve}{RGB}{24,24,128}

\usepackage{amsthm}
\newtheoremstyle{prop}{}{}{\color{prop}}{}{\color{prop}\bfseries}{}{ }{}
\theoremstyle{prop}
%\theoremstyle{definition}
\newtheorem{definition}{Définition}
%\theoremstyle{plain}
\newtheoremstyle{prop}{}{}{\color{prop}}{}{\color{prop}\bfseries}{}{ }{}
\theoremstyle{prop}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem{theoremeetdefinition}[definition]{Théorème et définition}
\theoremstyle{plain}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\let\oldproof\proof
\renewcommand{\proof}{\color{preuve}\oldproof}

\usepackage{titling}
\usepackage{titlesec}
\usepackage{sectsty}

\usepackage{multicol}

\usepackage{xsim}
\xsimsetup{solution/print=false}

\usepackage{eurosym}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{cutwin}
\usepackage{floatrow}
\usepackage{wrapfig}

\usepackage[e]{esvect}
\usepackage[extdef]{delimset}

\usepackage[inline]{enumitem}
\setlist{noitemsep}
%\setlist[1]{\labelindent=\parindent} % < Usually a good idea
\setlist[itemize]{leftmargin=*}
\setlist[itemize,1]{label=$\triangleright$}
\setlist[itemize,2]{label=\textbullet}
\setlist[enumerate]{labelsep=*, leftmargin=1.5pc}
\setlist[enumerate,1]{label=\textbf{\arabic*.}, ref=\arabic*}
\setlist[enumerate,2]{label=\emph{\alph*}),
ref=\theenumi.\emph{\alph*}}
\setlist[enumerate,3]{label=\roman*), ref=\theenumii.\roman*}
\setlist[description]{font=\sffamily\bfseries}


\usepackage{fancyhdr}
\pagestyle{fancy}

\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi

\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{Lycée \textsc{LaSalle Saint-Denis}}}}
\rfoot{\footnotesize{Page \thepage/ \pageref{LastPage}}}
\rhead{}
\lhead{}

\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\usepackage{array,multirow,makecell}
\setcellgapes{1pt}
\makegapedcells
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

\newenvironment{contreexemple}[1][]{%
  \begin{bclogo}[logo=\bcdz,noborder=true,barre=none]{#1}
    }{%
  \end{bclogo}
}
\newenvironment{note}[1][]{%
\begin{bclogo}[logo=\bctrombone,noborder=true,barre=none]{#1}
}{%
\end{bclogo}
}
\newenvironment{savoirfaire}[1][]{%
\begin{bclogo}[logo=\bcoutil,noborder=true,barre=none]{#1}
}{%
\end{bclogo}
}
\newenvironment{savoirrediger}[1][]{%
\begin{bclogo}[logo=\bccrayon,noborder=true,barre=none]{#1}
}{%
\end{bclogo}
}
\newenvironment{info}[1][]{%
\begin{bclogo}[logo=\bcinfo,noborder=true,barre=none]{#1}
}{%
\end{bclogo}
}

\usepackage{multicol}
\setlength{\columnseprule}{0.8pt}

% Packages pour les algorithmes
\usepackage[french]{algorithm2e}
\usepackage{pythontex}

% Pour le français
\usepackage{babel}

\newcommand{\R}{\mathbf{R}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Dec}{\mathbf{D}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\kk}{\mathbf{k}}

\newcommand{\A}{\mathcal{A}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\E}{\mathcal{E}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Cf}{\mathcal{C}}
\newcommand{\p}{\mathcal{P}}
\renewcommand{\P}{\mathcal{P}}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\U}{\mathcal{U}}
\newcommand{\V}{\mathcal{V}}
\newcommand{\SO}{\mathcal{SO}}
\newcommand{\D}{\mathcal{D}}
\newcommand{\M}{\mathcal{M}}

\newcommand{\Cl}{\mathscr{C}}
\newcommand{\CM}{\mathscr{CM}}
\renewcommand{\L}{\mathscr{L}}
\newcommand{\GL}{\mathcal{G}\!\ell}
\newcommand{\SL}{\mathcal{S}\!\ell}

% redéfinition partie réelle et partie imaginaire
\renewcommand{\Re}{\operatorname{Re}}
\renewcommand{\Im}{\operatorname{Im}}
\newcommand{\Id}{\operatorname{Id}}
\newcommand{\diag}{\operatorname{diag}}
\newcommand{\grad}{\operatorname{grad}}

\newcommand{\diff}{\mathop{}\mathopen{}\mathrm{d}}

\DeclareMathOperator{\card}{card}
\DeclareMathOperator{\gr}{gr}
\DeclareMathOperator{\rg}{rg}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Vect}{Vect}
\DeclareMathOperator{\supp}{supp}

\newcommand{\GO}[1]{\mathopen{}\mathcal{O}\mathclose{}\brk*{#1}}
\newcommand{\po}[1]{\mathopen{}o\mathclose{}\brk*{#1}}
\newcommand{\conj}{\overline}
\renewcommand{\vec}[1]{\mathbf{#1}}

\newcommand{\transpose}[1]{{\vphantom{#1}}^{t} \! #1}
% essai de \intercal et \top
\newcommand*{\EnsembleQuotient}[2]%
{\ensuremath{%
#1/\!\raisebox{-.65ex}{\ensuremath{#2}}}}
\newcommand*{\ZnZ}[1]{\ensuremath{\EnsembleQuotient{\Z}{#1\Z}}
}

%\usepackage{draftwatermark}

\newcommand{\repOij}{$(O,\vv{\imath},\vv{\jmath})$}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\everymath{\displaystyle{\everymath{}}}

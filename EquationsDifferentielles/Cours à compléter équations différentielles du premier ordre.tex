\documentclass[a4paper,12pt,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=2.0cm,hmargin=2.0cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\rhead{}
\newcommand{\e}{\varepsilon}

\title{Équations différentielles du premier ordre à coefficients
constants}
\author{BTS}
\date{}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title ~-- \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\newcommand{\cadre}[1]%
{\hspace{-\parindent}\framebox{
    \begin{minipage}{0.98\linewidth}
      \hspace{\linewidth}
      \vspace{#1}
    \end{minipage}
  }
}
\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{exemple}{Exemple}
\newtheorem{theoreme}{Théorème}
\theoremstyle{nonumber}
\newtheorem{remarque}{Remarque}

\begin{document}
\maketitle

De nombreux problèmes physiques peuvent se mettre en équation mettant en
jeux des grandeurs. Si on considère que ces granderus évoluent au cours
du temps, celles-ci vont se traduire par des fonctions d'une variable.
Les problèmes sont désormais traduits par une relation liant une
fonction à ses dérivées.

Une telle relation s'appelle une équation différentielle : c'est une
égalité comporte une inconnue (ici une fonction.)

On va ici préciser qu'on va se restreindre aux équations différentielles
du premier ordre, c'est à dire qui ne font intervenir que la fonction et
sa dérivée.

\begin{exemple}
  \begin{itemize}
    \item $f'=0$ est une équation différentielle dont la solution est
      définie pour tout $t$ réel par $f(t)=k$.
    \item $f'=f$ est une équation différentielle dont une solution est
      la fonction définie pour tout $t$ par $f(t) = e^t$. Pour $k$ un
      nombre réel, on constate que les fonctions $f(t) = ke^t$ sont
      aussi solutions de cette équation différentielle.
  \end{itemize}
\end{exemple}

\section{Généralités, vocabulaire, définition et premières propriétés}

Soit $y$ une fonction continue, dérivable sur $I$, un intervalle de
$\mathbf{R}$.

On se donne deux nombres réels, $a\neq 0$ et $b$ et une fonction $g$
définie sur $I$.

\cadre{1cm}

\begin{definition}
  L'équation $ay'(t)+by(t) = 0$ est l'équation homogène associée à
  l'équation différentielle précédente.
\end{definition}

Les solutions générales d'une telle équation différentielle (l'équation
homogène) sont de la forme :

\cadre{1cm}

Pour l'équation complète, il faut trouver une solution particulière, qui
est de la même forme que $g$. En pratique, on teste pour une ou
plusieurs fonctions si celles-ci sont solutions de l'équation
différentielle complète.

\begin{theoreme}
  La solution complète d'une équation différentielle est la somme d'une
  solution particulière $y_p$ et d'une solution de l'équation homogène
  $y_h$.
\end{theoreme}

Le problème ainsi posé possède une infinité de solutions.

\begin{definition}
  La donnée d'une équation différentielle du premier ordre et d'une
  condition initiale forme un «problème de Cauchy».
\end{definition}

\begin{theoreme}[admis]
  Un problème de Cauchy sur un intervalle $I$ de $\mathbf{R}$ possède
  une et une seule solution.
\end{theoreme}

\begin{remarque}
  Les théories générales d'algèbre linéaire permettent d'identifier les
  fonctions à des vecteurs et on parle souvent d'une droite de solution.
\end{remarque}

\section{Résolution en pratique}

\subsection{$a$ et $b$ constante, $c$ polynomiale}

Ici $\left(E\right)$ est l'équation \[ ay'(t)+by(t)=P(t) \]

On commence par résoudre l'équation dite homogène $\left(E_h\right)$
associée à l'équation complète $\left(E\right)$ : \[ ay'(t)+by(t)=0 \]

L'ensemble des solutions est $\left\{ke^{-\frac{b}{a}t}, k\in\mathbf{R}
\right\}$.

On cherche une solution particulière sous la forme d'un polynôme. En
pratique, on vérifie qu'une certaine fonction est solution, en dérivant
cette fonction, multipliant et additionnant.

La solution complète est la somme des deux solutions (homogène et
particulière).

\begin{exemple} Soit l'équation différentielle $y'-3y=x^2+2x-1$
  \begin{enumerate}
    \item Donner la solution de l'équation différentielle $y'(t) - 3y=
      0$.
    \item Vérifier que la fonction $h$ définie pour tout $t$ réel par
      $h(x) = -\frac{x^2}{3} - \frac{8x}{9} + \frac1{27}$ est une
      solution de l'équation différentielle.
    \item Donner la solution complète de l'équation différentielle.
    \item Trouver la fonction $f$, solution de l'équation différentielle
      telle que $f(0) = 0$.
  \end{enumerate}
\end{exemple}

\subsection{$b(t)$ proportionnelle à $a'(t)$, $c$ polynomiale}

Dans ce cas, on peut se ramener à un cas de la forme $(uv)'=p$ où $u$
est la fonction inconnue et $v$ est une fonction qui ne s'annule pas.
Les solutions sont de la forme $\dfrac{P(x)}{u(x)}$, avec $P$ une
primitive de $p$.

Des indications sont données dans ces cas de figure.

\end{document}

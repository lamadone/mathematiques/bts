% !TEX program = latexmk
% !TEX options = -pdf -synctex=1 -interaction=nonstopmode -file-line-error -shell-escape "%DOC%"
% !TEX encoding = UTF-8
% !TEX root = SIO_maths_approfondies.tex
\documentclass[class = scrartcl, crop = false, french]{standalone}

\usepackage{lycee}


\title{Calcul intégral}
\author{Vincent-Xavier Jumel}
\date{}

\begin{document}

\IfStandalone{%
  \maketitle
}{}

\begin{minipage}{0.7\linewidth}
  \textbf{Objectifs :}
  \begin{itemize}
    \item Concevoir et exploiter une simulation dans le cadre d'une loi
      uniforme
    \item Interpréter l'espérance et l'écart type d'une loi uniforme
    \item Connaître et intepréter graphiquement les paramètres d'une loi
      normale
    \item Déterminer les paramètres d'une loi normale
    \item Savoir déterminer les paramètres des loix «composées» $aX+b$,
      $X+Y$ et $X-Y$ dans le cas où les variables aléatoires $X$ et $Y$
      sont indépendantes.
    \item Savoir déterminer les paramètres de la loi normale
      correspondant à une moyenne dans le cadre du théorème de la limite
      centrée.
  \end{itemize}
\end{minipage}

\section{Lois à densité}

Sans rentrer dans les détails, on va donner quelques définitions
générales, qui seront mise en application dans les deux cas particuliers
de loi à densité qui sont la loi uniforme et la loi normale.

Dans le cadre ce cours, sans rentrer dans la théorie, nous admettrons
que les densité de probabilités sont des fonctions continues.

\subsection{Généralités}

De façon générale, une loi à densité est définie par \[ p(X\leq t) =
\int_{-\infty}^t f(x)\mathrm{d}x \] où $f$ est une fonction continue,
appellée fonction de densité qui respecte la condition suivante : \[
\int_{-\infty}^{\infty} f(x)\mathrm{d}x = 1.\]

On définit $p(a\leq X\leq b) = \int_{a}^b f(x)\mathrm{d}x$.

On appelle la fonction $F:t\mapsto \int_{-\infty}^t f(x)\mathrm{d}x$ la
\emph{fonction de répartition}.

Par analogie avec les variables aléatoires discrètes, où l'espérance est
définie comme une moyenne des valeurs prises par la variable aléatoire
pondérée par les probabilités ($E[X] = \sum_{k\in X(\Omega)} kp(X=k)$),
on définit l'espérance d'une variable aléatoire continue par \[E[X] =
\int_{X(\Omega)}xf(x)\mathrm{d}x.\]
De la même façon, \[V[X] = \int_{X(\Omega)}x^2f(x)\mathrm{d}x.\]

En fait, il faut bien y voir dans ces deux formulations, en apparence
différence la même notion de moyenne ou de somme, avec la notation
$\sum$ sur une partie de $\N$ et $\int$ sur $\R$.

\subsection{Loi uniforme}

Traitons plus en détail ici le cas de la loi uniforme, analogue dans le
cas continu des situations d'équiprobabilités dans un ensemble fini.

On suppose désormais que $a<b$.

\begin{proposition}
  La fonction $f:[a;b] \to [0;1], x\mapsto \frac{x-a}{b-a}$ est une
  bijection de fonction réciproque $f^{-1}:[0;1] \to [a;b], x\mapsto
  x(b-a) + a$.
\end{proposition}

\begin{corollaire}
  L'étude de la loi uniforme sur un segment $[a;b]$ peut se limiter à
  l'étude sur le segment $[0;1]$.
\end{corollaire}

\begin{definition}[Loi uniforme]
  On appelle loi uniforme sur le segment $[0,1]$ une loi dont la densité
  de probabilité est de 1 sur le segment en question et 0 partout
  ailleurs.

  Mathématiquement $X$ est une variable aléatoire telle que
  $\left\lbrace \begin{array}{l}p(X\leq 0) = 0 \\ p(0\leq X\leq 1) = 1
  \\ p(X \geq 1) = 0\end{array}\right.$
\end{definition}

\begin{proposition}[fonction de répartition d'une loi uniforme]
  ~\\[-5mm]
  Pour $t\in ]0;1[$, $p(0\leq X\leq t) = \int_0^t 1\mathrm{d}x = t$.
\end{proposition}

En utilisant la propriété de bijection entre les segments réels, on
obtient la généralisation suivante pour une loi uniforme sur un segment
quelconque : \[ \forall t \in ]a ; b[, p(X\leq t) = \int_a^t
\frac1{b-a}\mathrm{d}x \]

Une variable aléatoire qui suit une loi uniforme -- on note $X\leadsto
U([a;b])$ -- est entièrement caractérisée par son espérance et sa
variance.

\begin{proposition}[espérance d'une loi uniforme]
  L'espérance d'une variable aléatoire qui suit une loi uniforme est
  $E[X] = \frac{b+a}{2}$
\end{proposition}

\begin{proposition}[variance d'une loi uniforme]
  La variance d'une variable aléatoire qui suit une loi uniforme est
  $V[X] = \frac{(b-a)^2}{12}$.
\end{proposition}

Ces deux résultats s'obtiennent par intégration.

\subsection{Loi normale}

Posons $f_{\mu,\sigma}:\R\to\R_+^*, x\mapsto
\frac1{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-\mu)^2}{\sigma^2}}$

Cette fonction, pour $\mu = 0$ et $\sigma = 1$ s'appelle gaussienne et
permet de définir ce qu'on appelle communément loi de Laplace-Gauss.
Plus précisément, toute variable aléatoire qui suit une loi de densité
de probabilité $f_{\mu,\sigma}$ est dite normale de paramètre $\mu$ et
$\sigma$.

On dit alors que la variable aléatoire $X$ suit une loi normale de
paramètres $\mu$ et $\sigma$ -- on note $X\leadsto \mathcal{N}(\mu,
\sigma)$.

En pratique, pour la fonction de répartition de la loi normale, on
utilisera la calculatrice ou un logiciel de calcul.

\begin{proposition}[intégrale de la gaussienne]
  ~\\[-5mm]
  On admet que $\int_{-\infty}^{\infty} f_{\mu=0,\sigma=1}(x) \mathrm{d}x
  = 1$
\end{proposition}

\begin{proposition}[espérance d'une loi normale]
  Pour une variable qui suit un loi normale de paramètres $\mu$,
  $\sigma$, on a $E[X] = \mu$
\end{proposition}

\begin{proposition}[variance d'une loi normale]
  Pour une variable qui suit un loi normale de paramètres $\mu$,
  $\sigma$, on a $V[X] = \sigma^2$
\end{proposition}

Ces deux résultats se démontrent en utilisant les formules usuelles
d'intégration.

\section{Interaction des lois entre elles}

On s'intéresse ensuite aux interactions qui peuvent exister entre
différentes lois en particulier si une loi peut approcher une autre loi,
ou savoir quels sont les caractéristiques de position et de dispersion
d'une loi obtenue par combinaison linéaire 'autre lois et enfin se poser
la question de la répétition d'une même expérience aléatoire.

\subsection{Approximation d'une loi binomiale}

Sous certaines conditions, on peut vouloir appriocher une variable
aléatoire qui suit une loi binomiale par une variable aléatoire qui suit
une autre loi, ici la loi normale.

Dans ce cas, il faut garantir l'égalité des caractéristiques de
dispersion et de position. Cette approximation n'est possible que sous
certaines conditions concernant la loi binomiale :
\begin{itemize}
  \item il faut que $n$ soit «suffisament grand» ($n>50$) ;
  \item il faut que $p$ soit «moyen» ($0,2<p<0,8$) ;
  \item il faut que $np(1-p)$ soit «assez grand» ($np(1-p) >10$).
\end{itemize}
Les deux lois devant avoir même espérance et variance, on dit que, sous
les conditions précédentes, on peut approcher une loi binomiale
$\mathcal{B}(n,p)$ par une loi normale de paramètres :
\begin{itemize}
  \item $\mu = np$
  \item $\sigma^2 = np(1-p)$
\end{itemize}

\subsection{Composition des lois}

On se pose ici la question des caractéristiques de la loi de $aX+bY$ en
fonction des caractéristiques des lois de $X$ et de $Y$. Dans la suite,
on supposera que $X$ et $Y$ sont indépendantes.

On suppose également que $a$ et $b$ sont deux nombres réels.

On peut regrouper les propriétés de linéarité de l'espérance dans la
proposition suivante :

\begin{proposition}[linéarité de l'espérance]
  \begin{itemize}
    \item $E[aX+b] = aE[X] + b$ ;
    \item $E[X+Y] = E[X] + E[Y]$ ;
    \item $E[X-Y] = E[X] - E[Y]$.
  \end{itemize}
\end{proposition}

\textbf{Attention :} la variance fait intervenir un carré dans son
expression, ce qui entraîne la perte du caractère linéaire. On peut
regrouper dans la proposition suivante :

\begin{proposition}[non linéarité de la variance]
  \begin{itemize}
    \item $V[aX+b] = \left\lvert a\right\rvert E[X]$ ;
    \item $V[X+Y] = V[X] + V[Y]$ ;
    \item $V[X-Y] = V[X] + V[Y]$.
  \end{itemize}
\end{proposition}

Le dernier point de cette proposition peut s'interpréter comme une somme
d'erreurs. En effet, dans une série de mesure, il est usuel de dire que
«les erreurs s'additionnent.» Voici justement une version mathématique
de ce fait.

\subsection{Théorème de la limite centrée}

Le théorème de la limite centrée est un résultat important en théorie
des probabilités mais son énoncé est parfois difficile à comprendre et
appréhender.

Profitons en pour donner un théorème connexe, mais d'interprétation
plus aisé :
\begin{theoreme}[loi faible des grands nombres]
  Soient $X_1, X_2, \dots, X_n$ $n$ variables aléatoires indépendantes
  de même moyenne $\mu$ et de même variance $\sigma^2$.

  Soit $\overline{X} = \frac{X_1 + X_2 + \cdots + X_n}{n}$, alors pour
  tout $\varepsilon >0,\ \lim_{n\to +\infty}{p(\abs{X_n-\mu} \leqslant
  \varepsilon)} = 1$
\end{theoreme}
Ce théorème s'interprète de la façon suivante : à la limite, la
probabilité que la moyenne des variables aléatoires soit aussi proche de
la moyenne est totale.

Autrement dit encore, plus $n$ est grand, plus la variable aléatoire
$\overline{X}$ se rapproche de l'espérance $\mu$.

Encore autrement dit, la répétition d'un très grand nombre de fois d'une
même expérience aléatoire permet de se rapprocher de la moyenne
théorique.

Enfin, pour conclure ce section, on évoque le théorème de la liite
centrée, dans une version simplifiée :

\begin{theoreme}[de la limite centrée]
  Soient $X_1, X_2, \dots, X_n$ $n$ variables aléatoires indépendantes
  de même moyenne $\mu$ et de même variance $\sigma^2$.

  Soient $S_n = X_1 + X_2 +\cdots + X_n$ et $\overline{X} = \frac{X_1 +
  X_2 + \cdots + X_n}{n}$, alors
  \begin{itemize}
    \item la variable aléatoire $S_n$ suit approximativement une loi
      normale $\mathcal{N}(n\times\mu,\sigma\sqrt{n})$ ;
    \item la variable aléatoire $\overline{X_n}$ converge en loi vers
      une variable aléatoire $Y \leadsto \mathcal{N}(m,
      \frac{\sigma}{\sqrt{n}})$.
  \end{itemize}
\end{theoreme}

En pratique, on considère qu'il y'a convergence dès que $n>30$, mais
cela dépend du contexte de l'étude.

Ce théorème permet entre autre de déduire des informations sur des
échantillons prélevés au hasard dans une population dont on connaît les
caractéristiques de position et de dispersion.

En particulier, cela nous permet d'affirmer que la moyenne d'un
échantillon prélevé au hasard est la moyenne de la population et que
l'écart type d'un échantillon est $\frac{\sigma}{\sqrt{n}}$.

\end{document}

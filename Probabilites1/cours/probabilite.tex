% !TEX program = latexmk
% !TEX options = -pdf -synctex=1 -interaction=nonstopmode -file-line-error -shell-escape "%DOC%"
% !TEX encoding = UTF-8
% !TEX root = SIO_maths_approfondies.tex
\documentclass[class = scrartcl, crop = false, french]{standalone}

\usepackage{lycee}


\title{Calcul intégral}
\author{Vincent-Xavier Jumel}
\date{}

\begin{document}

\IfStandalone{%
  \maketitle
}{}

~\par
\begin{minipage}{0.7\linewidth}
  \textbf{Objectifs :}
  \begin{itemize}
    \item Connaître le vocabulaire usuel des probabilités
    \item Calculer des probabilités dans des cas simples
    \item Utiliser la notion d'indépendance et la formule de Bayes
    \item Avoir connaissance de la notion de variable aléatoire
    \item Calculer une probabilité sur un ensemble discret
      lorsqu'elle suit une des lois suivantes : schéma de Bernoulli,
      loi binomiale
  \end{itemize}
\end{minipage}

Dans ce document, on ne rentrera pas dans tout le détail du calcul des
probabilités et on cherchera à rester au niveau nécessaire à un
technicien supérieur. Cependant, certains points seront développés, sans
rentrer dans la théorie complète, afin de disposer de la vision la plus
exacte possible des probabilités.

\section{Généralités, vocabulaire}

\subsection{Vocabulaire}

\begin{definition}[Mesure de probabilité]
  On appelle \emph{mesure de probabilité} (ou \emph{probabilité}) une
  application d'un espace $\Omega$ dans $[0;1]$.
\end{definition}

Il est important de noter ici qu'une probabilité est un nombre compris
entre 0 et 1, valeurs particulières auxquelles nous donneront des
significations particulières.

\begin{definition}[Espace probabilisé]
  La donnée d'un espace $\Omega$ d'événements et d'une mesure de
  probabilité constitue un \emph{espace probabilisé}.
\end{definition}

On ne s'attachera pas plus que cela à cette définition théorique dans un
premier temps pour lui donner du sens lors de la découverte de la notion
de variable aléatoire.

\begin{definition}[événément]
  On appelle \emph{événement} toute partie $A$ de $\Omega$.
\end{definition}

On en profite pour rappeler la notation suivante : $A\subset \Omega
\iff \forall x \in A, x\in \Omega$, où $\iff$ est une notation pour
signifier l'équivalence logique (les deux propositions sont
interchangeables) et $\forall$ signifie aussi bien «pour tout» que
«quelque soit».

On précise que l'événement est élémentaire lorsqu'il est réduit à un
«point».

\begin{exemple}[événement élémentaire]
  $\{a\}, a\in\Omega$ est un événement élémentaire.

  En pratique, on note plutôt :
  \begin{quote}
    Soit $A$ l'événement $A$ : «la carte tirée dans le jeu de
    32 cartes est un roi de cœur».
  \end{quote}
\end{exemple}

Sans rentrer dans les détails, la réunion des événements élémentaires
recouvre tout l'espace probabilisé.

\subsection{Une axiomatique simplifiée}

On ne prends pas ici la totalité de l'axiomatique de Kolmogorov sur les
probabilités, mais on va citer quelques uns des axiomes, vérités premières
sur lesquelles nous ne reviendrons pas.

Soit $(\Omega, p)$ un espace probabilisé, c'est-à-dire, $p$ est une
mesure de probabilité sur $\Omega$.


\begin{enumerate}
  \item[A1] $p(\emptyset) = 0$
  \item[A2] Si $A$ et $B$ sont deux événements disjoints, $p(A\cup B) =
    p(A) + p(B)$
  \item[A3] Si $A$ est un événement élémentaire, et que tous les
    événements élémentaires ont même probabilité, $p(A) =
    \frac1{\#\Omega}$ où $\#\Omega$ est la «taille» de $\Omega$.
\end{enumerate}

Voici quelques exemples de mise en pratique des ces axiomes.

\begin{exemple}[A3]
  Soit $A$ l'événement $A$ : «tirer un roi de trèfle dans un jeu de 32
  cartes.».

  Il s'agit d'un événement élémentaire, on a donc $p(A) = \frac{1}{32}$.
\end{exemple}

\begin{exemple}[A2]
  Soient $A$ et $B$ deux événements disjoints.

  Supposons que $p(A) = \frac12$ et $p(B) = \frac13$.

  Alors $p(A\cup B ) = \frac56$.

\end{exemple}

Remarque : la disjonction des événements se note de façon ensembliste de
la façon suivante : $A\cap B = \emptyset$. C'est-à-dire que $x\in A
\implies x\notin B$ et réciproquement.

Dans le cas pratique des probabilités, on se sert également souvent de
la notion suivante.

\begin{definition}[événement contraire]
  On appelle événement contraire la négation totale de l'événement $A$
  considéré. On le note $\overline{A}$.
\end{definition}

\begin{exemple}[événement contraire]
  Soit $A$ l'événement $A$ : «tirer un roi de trèfle dans un jeu de 32
  cartes.».

  L'événement contraire $\overline{A}$ est l'événement «ne pas tirer de
  roi de trèfle», c'est à dire tirer n'importe quelle carte \emph{sauf}
  le roi de trèfle.
\end{exemple}

\begin{proposition}[probabilité de l'événement contraire]
  \[p(\overline{A}) = 1 - p(A) \]
\end{proposition}

\begin{corollaire}[probabilité de $\Omega$]
  $p(\Omega) = 1$
\end{corollaire}

\subsection{Formule des probabilités totales et exemples}

On admet la propriété suivante :
\begin{proposition}[Formule des probabilités totales]
  Soient $A$ et $B$ deux parties de $\Omega$, on a \[ p(A\cup B) = p(A)
  + p(B) - p(A\cap B) \]
\end{proposition}

Cette proposition appelle plusieurs remarques :
\begin{itemize}
  \item le terme $- p(A\cap B)$ correspond à la soustraction du «double
    compte» ;
  \item on retrouve A2 en utilisant A1 ;
  \item cette proposition possède deux cas d'utilisation :
    \begin{itemize}
      \item on connaît $p(A\cap B)$ ;
      \item $A$ et $B$ sont indépendant et c'est l'objet de la section
        suivante.
    \end{itemize}
\end{itemize}

\begin{exemple}[Formule des probabilités totales]
\end{exemple}

Pour finir cette partie, il faut évoquer quelques problèmes usuels de
dénombrement et quelques notions et notations associés.

\begin{definition}[factorielle d'un entier]
  On appelle factorielle de $n$ le nombre noté $n! = n\times n-1 \times
  \cdots \times 2\times 1$, avec la convention $0! = 1$
\end{definition}

On peut considérer l'algorithme suivant :

%\begin{algorithmic}[0] % 1 pour avoir des numéros
%  \Procedure{Factorielle}{$n$}
%  \State $r\gets 1$
%  \While{$n\not=0$}
%  \State $r\gets n\times r$
%  \State $n\gets n-1$
%  \EndWhile
%  \State \textbf{afficher} $r$
%  \EndProcedure
%\end{algorithmic}
\todo{réécrire en Python}

On en verra une mise en œuvre en Python.

On peut ainsi calculer le nombre de combinaisons possibles pour le
tirage du loto. On trouve $\frac{49!}{(49 - 7)!} = 49 \times 48 \times
47 \times 46 \times 45 \times 44 \times 43$, en tenant compte de l'ordre
d'apparition des numéros. On parle alors du nombre d'arrangement de 7
parmi 49. Si on ne tient pas compte de cet ordre, on peut aussi définir
le nombre de combinaison de 7 parmi 49. On obtient le définition
suivante.

\begin{definition}[nombre de combinaison]
  Le nombre de combinaison de $p$ parmi $n$ est le nombre de sous
  ensemble à $p$ éléments possible dans un ensemble à $n$ éléments,
  l'ordre ne comptant pas. \[\binom{n}{p} = \frac{n!}{p!(n-p)!} \]
\end{definition}


\section{Conditionnement, indépendance et formule de Bayes}

\subsection{Conditionnement}

La notion de conditionnement recouvre, en probabilités, les événements
dont l'occurrence dépend a priori d'un autre événement. On parle aussi de
probabilités conditionnelles.

\begin{definition}[probabilité conditionnelles]
  On dit que $p(A|B)$ est la probabilité de réalisation de l'événement
  $A$ sachant que l'événement $B$ est réalisé.
\end{definition}

En pratique, on parle dit souvent «probabilité de $A$ sachant $B$.».

On a de façon générale $p(A|B) = \frac{p(A\cap B)}{p(B)}$

Parmi les façons usuelles de présenter ces probabilités conditionnelles,
on a les arbres de probabilité.

\begin{center}
  \begin{tikzpicture}[level distance=25mm,sibling distance=10mm]
    \node {} [grow=right]
    child[sibling distance=20mm] {
      node {$\overline{A}$}
      child[sibling distance=10mm] {
        node {$\overline{B} \longrightarrow p(\overline{A}\cap\overline{B})$}
        edge from parent node[below] {$p_{\overline{A}}(\overline{B})$}
      }
      child[sibling distance=10mm] {
        node {$B \longrightarrow p(\overline{A} \cap B)$}
        edge from parent node[above] {$p_{\overline{A}}(B)$}
      }
      edge from parent node[below] { $p(\overline{A})$ }
    }
    child[sibling distance=20mm] { node {$A$}
      child[sibling distance=10mm] {
        node {$\overline{B} \longrightarrow p(A\cap\overline{B})$}
        edge from parent node[below] { $p_A(\overline{B})$ }
      }
      child[sibling distance=10mm] {
        node {$B \longrightarrow p(A\cap B)$}
        edge from parent node[above] { $p_A(B)$ }
      }
      edge from parent node[above] { $p(A)$ }
    } ;
  \end{tikzpicture}
\end{center}

On peut également utiliser un tableau. Un exercice de probabilité
permettra de mettre en œuvre ces techniques.

\subsection{Indépendance}

Il s'agit de caractériser le fait que des probabilités dépendent l'une
de l'autre.

Dans le cadre du métier de technicien, on admet le théorème suivant dont
nous donnerons les deux cas d'usage.

\begin{theoreme}[indépendance]
  Deux événements $A$ et $B$ sont indépendants si, et seulement si,
  $p(A\cap B) = p(A) \times p(B)$
\end{theoreme}

Ce théorème sert aussi bien dans le sens direct : si $A$ et $B$ sont
deux événements indépendants, alors $p(A\cap B) = p(A) \times p(B)$,
ce qui est en particulier utile pour la formule des probabilités
totales.

Réciproquement, si $p(A\cap B) = p(A) \times p(B)$, alors on
indépendance des événements. En particulier $p(A|B) = p(A)$ lorsque les
événements sont indépendants.

\subsection{Formule de Bayes}

La formule de Bayes et le théorème associé sont particulièrement utile
dans le cadre du calcul des probabilités, en l'ocurrence pour obtenir la
probabilité conditionnelle de $p(A|B)$ à partir de $p(B|A)$.

\begin{theoreme}[de Bayes]
  Soient $A$ et $B$ deux événements, on a \[p(A|B) = \frac{p(B|A)\times
  p(A)}{p(B)} \]
  Une autre formulation, plus complète est la suivante : \[ p(A|B) =
    \frac{p(B|A)\times p(A)}{p(B|A)p(A) +
  p(B|\overline{A})p(\overline{A})} \]
\end{theoreme}

\section{Variable aléatoire et lois discrètes}

\subsection{Variable aléatoire}

Notons d'abord que le mot «variable» est assez mal choisi. En effet, la
définition qui vient va permettre de préciser la notion.

\begin{definition}[variable aléatoire]
  Une variable aléatoire est une \emph{fonction} de $\Omega$, espace
  probabilisé dans un ensemble de nombre.
\end{definition}

Il s'agit ici d'une définition assez large qui servira aussi bine pour
les variables aléatoires discrètes ou continues.

Pour l'instant, on ne s'intéresse qu'aux variables aléatoires discrètes.

\begin{definition}[variable aléatoire discrète]
  On dit qu'une variable aléatoire est discrète lorsqu'elle ne prends
  qu'un nombre fini de valeurs.
\end{definition}

On note une variable aléaotoire sous la forme $(X=k)$, où $k$ est un des
éléments de l'ensemble discret.

Lorsque $k$ décrit l'ensemble des valeurs, la somme des probabilités
vaut 1.



\end{document}

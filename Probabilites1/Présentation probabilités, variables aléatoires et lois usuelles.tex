\documentclass[french]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{lmodern}
\usepackage[sfmathbb]{kpfonts}
%\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\pdfobjcompresslevel 3

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}

\usepackage{babel}
%\theoremstyle{break}
%\theorembodyfont{\normalfont}
%\renewtheorem{exercice}{Exercice}

\usetheme{Frankfurt}

\title{Probabilités 1}
\author{\textsc{Jumel}}
\institute{Jean-Baptiste de la Salle, Saint-Denis}
\date{juin 2015}

\newcommand{\ensemble}[1]{\mathbb{#1}}
\newcommand{\R}{\ensemble{R}}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\mathversion{rm}

\section{Introduction aux probabilités}

\begin{frame}
  \begin{block}{}
    Une probabilité est une \alert{mesure} du hasard prise entre 0 et 1.
  \end{block}
\end{frame}

\subsection{Vocabulaires et notations}

\begin{frame}
  \begin{block}{Vocabulaire}
    \begin{description}
      \item[expérience aléatoire] expérience dont le résultat ne peut
        être connu «simplement» à l'avance ;
      \item[espace de probabilité] l'ensemble des résultats possibles ;
      \item[événement] résultat d'une expérience aléatoire ;
    \end{description}
  \end{block}
\end{frame}

\begin{frame}
  \begin{block}{Axiomes}
    $E$ est un ensemble (collection définie d'objets) et $\emptyset$
    existe défini comme ensemble vide.
  \end{block}
  \begin{block}{Notations ensemblistes}
    \begin{description}
      \item[appartenance] $x\in A$
      \item[inclusion] $B\subset A$ si quand $x\in B$ alors $x\in A$
      \item[égalité] $A=B$ si $A\subset B$ et $B\subset A$
      \item[union] $x\in A\cup B$ si $x\in A$ ou $x\in B$
      \item[intersection] $x\in A\cap B$ si $x\in A$ et $x\in B$
      \item[complémentaire] $x\in\overline{A}$ si $x\notin A$
      \item[disjoint] $A\cap B = \emptyset$
    \end{description}
  \end{block}
\end{frame}

\begin{frame}
  \begin{definition}
    On dit qu'un événement $A$ est réalisé lorsque le résultat d'une
    expérience aléatoire est dans cet ensemble $A$. Avec les notations
    ensemblistes, on a $x\in A$.
  \end{definition}
  \begin{definition}
    On dit qu'un événement est élémentaire s'il n'est réalisé qu'à une
    seule occurrence. Avec les notations ensemblistes, on a
    $A=\lbrace\omega\rbrace$.
  \end{definition}
\end{frame}

\begin{frame}
  \begin{block}{Conventions usuelles}
    $\Omega = \displaystyle{\bigcup_{A\in\text{événements}} A}$
  \end{block}
\end{frame}

\subsection{Définitions et propriétés élementaires}

\begin{frame}
  \begin{definition}
    $P:\Omega\to [0,1]$ est une probabilité si
    \begin{enumerate}
      \item $P(\emptyset) = 0$
      \item $P(A\cup B) = P(A)+P(B)$ si $A$ et $B$ sont disjoints
    \end{enumerate}
  \end{definition}
  \begin{block}{Propriétés élémentaires}
    \begin{itemize}
      \item $P(\overline{A}) = 1-P(A)$
      \item $A\subset B \implies P(A) \leq P(B)$
      \item $P(A\cup B) = P(A) + P(B) - P(A\cap B)$
    \end{itemize}
  \end{block}
\end{frame}

\section{Indépendance et conditionnement}

\subsection{Indépendance}

\begin{frame}
  \begin{definition}
    $A$ et $B$ sont deux événements indépendants si et seulement si
    $P(A\cap B) = P(A)\times P(B)$
  \end{definition}
  \begin{block}{Remarque}
    Indépendants $\neq$ incompatibles = $A\cap B=\emptyset $
  \end{block}
\end{frame}

\subsection{Probabilité conditionnelle}

\begin{frame}
  \begin{definition}
    $P_B(A) = P(A\setminus B)$ est la probabilité de réalisation de $A$
    sachant que $B$ est réalisé.
  \end{definition}
  \begin{block}{Formule de calcul, cas général}
    \[P_B(A) = \dfrac{P(A\cap B)}{P(B)}\]
  \end{block}
  \begin{block}{Formule de calcul, $A$ et $B$ indépendants}
    \[P_B(A) = P(A)\]
  \end{block}
\end{frame}

\subsection{Formule de Bayes}

\begin{frame}
  \begin{block}{Un arbre pondéré}
    \begin{tikzpicture}[level distance=25mm,sibling distance=10mm]
      \node {} [grow=right]
      child[sibling distance=20mm] {
        node {$\overline{A}$}
        child[sibling distance=10mm] {
          node {$\overline{B} \longrightarrow p(\overline{A}\cap\overline{B})$}
          edge from parent node[below] {$p_{\overline{A}}(\overline{B})$}
        }
        child[sibling distance=10mm] {
          node {$B \longrightarrow p(\overline{A} \cap B)$}
          edge from parent node[above] {$p_{\overline{A}}(B)$}
        }
        edge from parent node[below] { $p(\overline{A})$ }
      }
      child[sibling distance=20mm] { node {$A$}
        child[sibling distance=10mm] {
          node {$\overline{B} \longrightarrow p(A\cap\overline{B})$}
          edge from parent node[below] { $p_A(\overline{B})$ }
        }
        child[sibling distance=10mm] {
          node {$B \longrightarrow p(A\cap B)$}
          edge from parent node[above] { $p_A(B)$ }
        }
        edge from parent node[above] { $p(A)$ }
      } ;
    \end{tikzpicture}
  \end{block}
  \begin{block}{«Inverser» une probabilité conditionnelle}
    \[ P_B(A) = \frac{P(B)}{P(A\cap B)} = \frac{P(B)}{P_A(B)P(A) +
    P_{\overline{A}}(B)P(\overline{A})} \]
  \end{block}
\end{frame}

\section{Variables aléatoires}

\subsection{Généralités}

\begin{frame}
  \begin{definition}
    Une variable aléatoire est une fonction de $\Omega$ dans $\R$ :
    $X\colon\Omega\to\R$
  \end{definition}
  \begin{block}{Caractérisation}
    \begin{itemize}
      \item $\mathop{Card}X(\Omega)$ fini $\implies X$ suit une loi
        discrète ;
      \item $\mathop{Card}X(\Omega)$ infini $\implies X$ suit une loi
        continue.
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Quantités caractéristiques d'une variable aléatoire
discrète}

\begin{frame}
  \begin{block}{Espérance}
    \[ E[X] = \sum_{i=1}^n{k_i P(X=k_i)} \]
  \end{block}
\end{frame}

\begin{frame}
  \begin{block}{Variance}
    \[ E[X] = \sum_{i=1}^n{k_i^2 P(X=k_i)} \]
  \end{block}
\end{frame}

\begin{frame}
  \begin{block}{Propriétés}
    \begin{itemize}
      \item $V[X]\geq 0$ ;
      \item $V[X] = 0 \iff k_i = E[X] \forall i$;
      \item $E[aX+b] = aE[X] + b$
      \item $V[aX+b] = a^2V[X]$
    \end{itemize}
  \end{block}
  \begin{theorem}[Koenig]
    \[ V[X] = E[X^2-E[X]^2] \]
  \end{theorem}
\end{frame}

\section{Lois discrètes}

\subsection{Schéma de Bernoulli}

\begin{frame}
  \begin{definition}
    Expérience ne possédant que 2 issues possibles.
  \end{definition}
  \begin{block}{Modélisation}
    Succès $\to 1$, échec $\to 0$ avec la probabilité $p$ pour le
    succès (et $1-p$ pour l'échec)
  \end{block}
\end{frame}

\subsection{Loi binomiale}

\begin{frame}
  \begin{definition}
    \begin{itemize}
      \item répétition d'un même schéma de Bernouilli ;
      \item de façon indépendante.
    \end{itemize}
  \end{definition}
  \begin{block}{$X\leadsto \mathcal{B}(n,p)$}
    \[ P(X=k)=\binom{n}{k}p^k(1-p)^{n-k} \]
  \end{block}
\end{frame}

\begin{frame}
  \begin{block}{}
    \[ X\leadsto\mathcal{B}(n,p) \]
  \end{block}
  \begin{block}{Espérance}
    \[ E[X] = np \]
  \end{block}
  \begin{block}{Variance}
    \[ V[X] = np(1-p) \]
  \end{block}
\end{frame}

\end{document}
